using NUnit.Framework;
using Streaming.Helpers.Azure;

namespace StreamingUnitTests
{
    public class AzureBaseTests
    {
        /*
            <add key="Azure.ClientId" value="c38c7d7d-1958-4601-9589-09f095a3efd5" />
            <add key="Azure.SubscriptionId" value="ff9a2710-ca03-4ae1-b7d7-ff1fc56299dd" />
            <add key="Azure.TenantId" value="c2abc045-7e2f-4f66-93c4-64ceec20b893" />
            <add key="Azure.ClientKey" value="c/m=4JPUCvKvB1lmYYOO9B9-D[[WHJxM" />
            <add key="Azure.BaseURL" value="https://management.azure.com/" />
            <add key="Azure.AuthURL" value="https://login.windows.net/" />
            <add key="Azure.GraphURL" value="https://graph.windows.net/" />
            <add key="Azure.ManagementURL" value="https://management.core.windows.net/" />
        */

        [SetUp]
        public void Setup()
        {
            var opts = new AzureHelperInitializationOptions()
            {
                ClientId = "c38c7d7d-1958-4601-9589-09f095a3efd5",
                SubscriptionId = "ff9a2710-ca03-4ae1-b7d7-ff1fc56299dd",
                TenantId = "c2abc045-7e2f-4f66-93c4-64ceec20b893",
                ClientKey = "c/m=4JPUCvKvB1lmYYOO9B9-D[[WHJxM",
                BaseURL = "https://management.azure.com/",
                AuthURL = "https://login.windows.net/",
                GraphURL = "https://graph.windows.net/",
                ManagementURL = "https://management.core.windows.net/"
            };

            AzureHelper.Configure(opts);
        }
    }
}