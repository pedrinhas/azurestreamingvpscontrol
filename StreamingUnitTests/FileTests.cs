﻿using StreamingAssemblies.Helpers.Azure.FileManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace StreamingUnitTests
{
    public class FileTests : AzureBaseTests
    {
        private readonly string _folderPath = @"D:\git\utad\Streaming\StreamingUnitTests\ConfigFiles";

        private readonly string _existingConfigFileName = "azure_370d2b52_da7b_4889_a594_05ce97da0fa9.properties";
        private readonly string _existingConfigSubscriptionId = "e2f9e6a6-20ea-40b2-9803-22496557418c";
        private readonly string _existingConfigClientId = "3b023993-5d5c-4378-8adf-0b50ad23347b";
        private readonly string _existingConfigClientKey = "SOME_KEY";
        private readonly string _existingConfigTenantId = "ea101e1b-5659-4a5d-9435-e1dbd029eea7";


        public FileTests()
        {
            AzureFileManager.Initialize(_folderPath);
        }

        [Fact]
        public async Task AddNewConfigFile()
        {
            try
            {
                var fileRes = await AzureFileManager.SaveAzureFileAsync(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), $"SOME_KEY_{Guid.NewGuid()}");

                Assert.NotNull(fileRes);
                Assert.True(fileRes.Success);
                Assert.True(File.Exists(fileRes.Value));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Fact]
        public async Task TryAddExistingConfigFile()
        {
            try
            {
                var fileRes = await AzureFileManager.SaveAzureFileAsync(_existingConfigSubscriptionId, _existingConfigTenantId, _existingConfigClientId, _existingConfigClientKey);

                Assert.NotNull(fileRes);
                Assert.True(fileRes.Success);
                Assert.True(File.Exists(fileRes.Value));
                Assert.Equal(Path.Combine(_folderPath, _existingConfigFileName), fileRes.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
