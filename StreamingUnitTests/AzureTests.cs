﻿namespace StreamingUnitTests
{
    using StreamingAssemblies.Helpers.Azure;
    using System;
    using System.Threading.Tasks;
    using Xunit;

    /// <summary>
    /// Defines the <see cref="AzureTests" />
    /// </summary>
    public class AzureTests : AzureBaseTests
    {
        /// <summary>
        /// Defines the _configFilePath
        /// </summary>
        private readonly string _configFilePath = $@"D:\repos utad\Streaming\StreamingUnitTests\azureauth_nsi2.properties";

        private AzureClient _azureClient => AzureClientStore.GetClient(_configFilePath);

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureTests"/> class.
        /// </summary>
        public AzureTests()
        {
            AzureClientStore.AddAzureClient(_configFilePath);
        }

        /// <summary>
        /// The InstanceNotNull
        /// </summary>
        [Fact]
        public void InstanceNotNull()
        {
            try
            {
                Assert.NotNull(_azureClient.AzureInstance);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The StartVmAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        [Fact]
        public async Task StartVmAsync()
        {
            try
            {
                var res = await _azureClient.StartVirtualMachineAsync(_idUtadVideocast2);

                Assert.NotNull(res);
                Assert.True(res.Success);
                Assert.Null(res.Exception);

                var stateRes = await _azureClient.GetVirtualMachineStatusAsync(_idUtadVideocast2);

                Assert.NotNull(stateRes);
                Assert.True(stateRes.Success);
                Assert.NotNull(stateRes.Value);
                Assert.Null(stateRes.Exception);
                Assert.Equal(stateRes.Value, Microsoft.Azure.Management.Compute.Fluent.PowerState.Running);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The StopVmAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        [Fact]
        public async Task StopVmAsync()
        {
            try
            {
                var res = await _azureClient.StopVirtualMachineAsync(_idUtadVideocast2);

                Assert.NotNull(res);
                Assert.True(res.Success);
                Assert.Null(res.Exception);

                var stateRes = await _azureClient.GetVirtualMachineStatusAsync(_idUtadVideocast2);

                Assert.NotNull(stateRes);
                Assert.True(stateRes.Success);
                Assert.NotNull(stateRes.Value);
                Assert.Null(stateRes.Exception);
                Assert.Equal(stateRes.Value, Microsoft.Azure.Management.Compute.Fluent.PowerState.Stopped);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// The StartVmAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        [Fact]
        public async Task StartVmMultiThreadAsync()
        {
            try
            {
                var res = await _azureClient.StartVirtualMachineMultiThreadAsync(_idUtadVideocast2);

                Assert.NotNull(res);
                Assert.True(res.Success);
                Assert.Null(res.Exception);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The StopVmAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        [Fact]
        public async Task StopVmMultiThreadAsync()
        {
            try
            {
                var res = await _azureClient.StopVirtualMachineMultiThreadAsync(_idUtadVideocast2);

                Assert.NotNull(res);
                Assert.True(res.Success);
                Assert.Null(res.Exception);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The GetVmStateAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        [Fact]
        public async Task GetVmStateAsync()
        {
            try
            {
                var res = await _azureClient.GetVirtualMachineStatusAsync(_idUtadVideocast2);

                Assert.NotNull(res);
                Assert.True(res.Success);
                Assert.NotNull(res.Value);
                Assert.Null(res.Exception);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The GetVmAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        [Fact]
        public async Task GetVmAsync()
        {
            try
            {
                var res = await _azureClient.GetVirtualMachineAsync(_idUtadVideocast2);

                Assert.NotNull(res);
                Assert.True(res.Success);
                Assert.NotNull(res.Value);
                Assert.Null(res.Exception);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The GetAllVmsAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        [Fact]
        public async Task GetAllVmsAsync()
        {
            try
            {
                var res = await _azureClient.GetAllVirtualMachinesAsync();

                Assert.NotNull(res);
                Assert.True(res.Success);
                Assert.NotNull(res.Value);
                Assert.Null(res.Exception);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}