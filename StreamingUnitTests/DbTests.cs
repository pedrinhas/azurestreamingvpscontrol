﻿using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Helpers.Azure;
using StreamingAssemblies.Models.Azure.Logs;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace StreamingUnitTests
{
    /// <summary>
    /// Defines the <see cref="DbTests" />.
    /// </summary>
    public class DbTests
    {
        /// <summary>
        /// Defines the _deletedVmId.
        /// </summary>
        public readonly int _deletedVmId = 10;

        /// <summary>
        /// Defines the _notDeletedVmId.
        /// </summary>
        public readonly int _notDeletedVmId = 25;

        /// <summary>
        /// Defines the _azureDb.
        /// </summary>
        public AzureDb _azureDb;
        public LogDb _logDb;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbTests"/> class.
        /// </summary>
        public DbTests()
        {
            _azureDb = new AzureDb("Data Source=.;Initial Catalog=streaming;Integrated Security=True");
            _logDb = new LogDb("Data Source=.;Initial Catalog=streaming;Integrated Security=True");
        }

        /// <summary>
        /// Defines the <see cref="ReadTests" />.
        /// </summary>
        public class ReadTests : DbTests
        {
            /// <summary>
            /// The GetAllActiveVirtualMachines.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetAllActiveVirtualMachines()
            {
                try
                {
                    var allVmsResult = await _azureDb.GetAllActiveVirtualMachinesAsync();

                    Assert.NotNull(allVmsResult);
                    Assert.True(allVmsResult.Success);
                    Assert.NotEmpty(allVmsResult.Value);

                    Assert.Empty(allVmsResult.Value.Where(x => !x.IsActive));
                    Assert.Empty(allVmsResult.Value.Where(x => x.IsDeleted));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The GetAllActiveVirtualMachinesWithDeleted.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetAllActiveVirtualMachinesWithDeleted()
            {
                try
                {
                    var allVmsResult = await _azureDb.GetAllActiveVirtualMachinesAsync(includeDeleted: true);

                    Assert.NotNull(allVmsResult);
                    Assert.True(allVmsResult.Success);
                    Assert.NotEmpty(allVmsResult.Value);

                    Assert.Empty(allVmsResult.Value.Where(x => !x.IsActive));
                    Assert.NotEmpty(allVmsResult.Value.Where(x => x.IsDeleted));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The GetAllInactiveVirtualMachines.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetAllInactiveVirtualMachines()
            {
                try
                {
                    var allVmsResult = await _azureDb.GetAllInactiveVirtualMachinesAsync();

                    Assert.NotNull(allVmsResult);
                    Assert.True(allVmsResult.Success);
                    Assert.NotEmpty(allVmsResult.Value);

                    Assert.Empty(allVmsResult.Value.Where(x => x.IsActive));
                    Assert.Empty(allVmsResult.Value.Where(x => x.IsDeleted));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The GetAllInactiveVirtualMachinesWithDeleted.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetAllInactiveVirtualMachinesWithDeleted()
            {
                try
                {
                    var allVmsResult = await _azureDb.GetAllInactiveVirtualMachinesAsync(includeDeleted: true);

                    Assert.NotNull(allVmsResult);
                    Assert.True(allVmsResult.Success);
                    Assert.NotEmpty(allVmsResult.Value);

                    Assert.Empty(allVmsResult.Value.Where(x => x.IsActive));
                    Assert.NotEmpty(allVmsResult.Value.Where(x => x.IsDeleted));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The GetAllVirtualMachines.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetAllVirtualMachines()
            {
                try
                {
                    var allVmsResult = await _azureDb.GetAllVirtualMachinesAsync();

                    Assert.NotNull(allVmsResult);
                    Assert.True(allVmsResult.Success);
                    Assert.NotEmpty(allVmsResult.Value);

                    Assert.Empty(allVmsResult.Value.Where(x => x.IsDeleted));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The GetAllVirtualMachinesWithDeleted.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetAllVirtualMachinesWithDeleted()
            {
                try
                {
                    var allVmsResult = await _azureDb.GetAllVirtualMachinesAsync(includeDeleted: true);

                    Assert.NotNull(allVmsResult);
                    Assert.True(allVmsResult.Success);
                    Assert.NotEmpty(allVmsResult.Value);

                    Assert.NotEmpty(allVmsResult.Value.Where(x => x.IsDeleted));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The GetVirtualMachine.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetVirtualMachine()
            {
                try
                {
                    var vmResult = await _azureDb.GetVirtualMachineAsync(_notDeletedVmId);

                    Assert.NotNull(vmResult);
                    Assert.True(vmResult.Success);
                    Assert.NotNull(vmResult.Value);

                    Assert.Equal(_notDeletedVmId, vmResult.Value.Id);
                    Assert.False(vmResult.Value.IsDeleted);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The GetVirtualMachineWithDeleted.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task GetVirtualMachineWithDeleted()
            {
                try
                {
                    var vmResult = await _azureDb.GetVirtualMachineAsync(_deletedVmId, includeDeleted: true);

                    Assert.NotNull(vmResult);
                    Assert.True(vmResult.Success);
                    Assert.NotNull(vmResult.Value);

                    Assert.Equal(_deletedVmId, vmResult.Value.Id);
                    Assert.True(vmResult.Value.IsDeleted);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            [Fact]
            public async Task GetAllAzureVmLogs()
            {
                try
                {
                    var logs = await _logDb.AzureLog.Log_LS();

                    Assert.NotNull(logs);
                    Assert.True(logs.Success);
                    Assert.NotEmpty(logs.Value);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Defines the <see cref="WriteTests" />.
        /// </summary>
        public class WriteTests : DbTests
        {
            /// <summary>
            /// Defines the _configFilePath1.
            /// </summary>
            private readonly string _configFilePath1 = $@"D:\git\utad\Streaming\StreamingUnitTests\azureauth.properties";

            /// <summary>
            /// Defines the _configFilePath2.
            /// </summary>
            private readonly string _configFilePath2 = $@"D:\git\utad\Streaming\StreamingUnitTests\azureauth_nsi2.properties";

            /// <summary>
            /// Defines the _user.
            /// </summary>
            private readonly string _user = @"TESTES\cpereira";

            public readonly int _someUserId = 5;

            /// <summary>
            /// Defines the _utadVideocast1Id.
            /// </summary>
            private readonly string _utadVideocast1Id = "/subscriptions/ff9a2710-ca03-4ae1-b7d7-ff1fc56299dd/resourceGroups/NSI/providers/Microsoft.Compute/virtualMachines/utad-videocast";

            /// <summary>
            /// Defines the _utadVideocast2Id.
            /// </summary>
            private readonly string _utadVideocast2Id = "/subscriptions/c6fded35-6797-4be5-9bfd-d5c0ea26e01c/resourceGroups/NSI/providers/Microsoft.Compute/virtualMachines/utad-videocast2";
            /// <summary>
            /// Initializes a new instance of the <see cref="WriteTests"/> class.
            /// </summary>
            public WriteTests()
            {
                var added1 = AzureClientStore.AddAzureClient(_configFilePath1);
                Assert.NotNull(added1);
                Assert.True(added1.Success);
                Assert.NotNull(added1.Value);

                var added2 = AzureClientStore.AddAzureClient(_configFilePath2);
                Assert.NotNull(added2);
                Assert.True(added2.Success);
                Assert.NotNull(added2.Value);
            }

            /// <summary>
            /// The DeleteVm.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task DeleteVm()
            {
                try
                {
                    var setActiveResult = await _azureDb.DeleteVirtualMachineAsync(_notDeletedVmId, _user);

                    Assert.NotNull(setActiveResult);
                    Assert.True(setActiveResult.Success);

                    var vmResult = await _azureDb.GetVirtualMachineAsync(_notDeletedVmId, true);

                    Assert.NotNull(vmResult);
                    Assert.True(vmResult.Success);
                    Assert.NotNull(vmResult.Value);
                    Assert.True(vmResult.Value.IsDeleted);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The InsertVirtualMachine.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task InsertVirtualMachine()
            {
                try
                {
                    var clientAddedResult = AzureClientStore.GetClient(_configFilePath1);

                    Assert.NotNull(clientAddedResult);
                    Assert.True(clientAddedResult.Success);
                    Assert.NotNull(clientAddedResult.Value);

                    var client = clientAddedResult.Value;

                    var allVmsResult = await client.GetAllVirtualMachinesAsync();

                    var vmResult = await client.GetVirtualMachineAsAzureVirtualMachineAsync(_utadVideocast1Id);

                    Assert.NotNull(vmResult);
                    Assert.True(vmResult.Success);
                    Assert.NotNull(vmResult.Value);

                    var vm = vmResult.Value;

                    vm.PluginUrl = "www.tvpirata.com";

                    var vmAddedResult = await _azureDb.AddVirtualMachineAsync(vm, _user);

                    Assert.NotNull(vmAddedResult);
                    Assert.True(vmAddedResult.Success);
                    Assert.NotNull(vmAddedResult.Value);
                    Assert.Equal(vm.VmId, vmAddedResult.Value.VmId);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The RestoreDeletedVm.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task RestoreDeletedVm()
            {
                try
                {
                    var setActiveResult = await _azureDb.RestoreDeletedVirtualMachineAsync(_notDeletedVmId, _user);

                    Assert.NotNull(setActiveResult);
                    Assert.True(setActiveResult.Success);

                    var vmResult = await _azureDb.GetVirtualMachineAsync(_notDeletedVmId);

                    Assert.NotNull(vmResult);
                    Assert.True(vmResult.Success);
                    Assert.NotNull(vmResult.Value);
                    Assert.False(vmResult.Value.IsDeleted);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The SetVmAsActive.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task SetVmAsActive()
            {
                try
                {
                    var setActiveResult = await _azureDb.SetVirtualMachineAsActiveAsync(_notDeletedVmId, _user);

                    Assert.NotNull(setActiveResult);
                    Assert.True(setActiveResult.Success);

                    var vmResult = await _azureDb.GetVirtualMachineAsync(_notDeletedVmId);

                    Assert.NotNull(vmResult);
                    Assert.True(vmResult.Success);
                    Assert.NotNull(vmResult.Value);
                    Assert.True(vmResult.Value.IsActive);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// The SetVmAsInactive.
            /// </summary>
            /// <returns>The <see cref="Task"/>.</returns>
            [Fact]
            public async Task SetVmAsInactive()
            {
                try
                {
                    var setActiveResult = await _azureDb.SetVirtualMachineAsInactiveAsync(_notDeletedVmId, _user);

                    Assert.NotNull(setActiveResult);
                    Assert.True(setActiveResult.Success);

                    var vmResult = await _azureDb.GetVirtualMachineAsync(_notDeletedVmId);

                    Assert.NotNull(vmResult);
                    Assert.True(vmResult.Success);
                    Assert.NotNull(vmResult.Value);
                    Assert.False(vmResult.Value.IsActive);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            [Fact]
            public async Task InsertLog()
            {
                try
                {
                    throw new ArgumentNullException("someArg", "someArgName");
                }
                catch (Exception ex)
                {
                    var logged = await _logDb.Log(ex, Guid.NewGuid());

                    Assert.NotNull(logged);
                    Assert.Null(logged.Exception);
                    Assert.True(logged.Success);
                }
            }

            [Fact]
            public async Task InsertAzureVmLog()
            {
                try
                {
                    var res = await _logDb.AzureLog.Log_WithVM_I(_someUserId, $"UNIT TEST {nameof(InsertAzureVmLog)}", _notDeletedVmId, AzureLogAction.Started);
                    Assert.NotNull(res);
                    Assert.True(res.Success);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
