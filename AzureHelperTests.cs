﻿using NUnit.Framework;
using Streaming.Helpers.Azure;
using System;
using System.Collections.Generic;
using System.Text;

namespace StreamingUnitTests
{
    public class AzureHelperTests : AzureBaseTests
    {
		[Test(Description = "Get IAzure instance")]
		public void GetIAzureInstance()
		{
			try
			{
				var instance = AzureHelper.AzureInstance;

				Assert.NotNull(instance);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}
