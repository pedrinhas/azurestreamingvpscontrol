using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StreamingAssemblies.Models;

namespace Streaming.ViewModels
{
    public class LoginViewModel
    {
        public LoginCredenciais LoginCredenciais { get; set; }
        public LoginCredenciais LoginCredenciaisTemporarias { get; set; }
        public string EmailRecuperarCredenciaisTemporarias { get; set; }

        public LoginViewModel()
        {
            LoginCredenciais = new LoginCredenciais();
            LoginCredenciaisTemporarias = new LoginCredenciais();
            EmailRecuperarCredenciaisTemporarias = "";
        }
    }
}