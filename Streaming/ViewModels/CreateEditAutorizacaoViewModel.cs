using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StreamingAssemblies.Models.Streaming;

namespace Streaming.ViewModels
{
    public class CreateEditAutorizacaoViewModel
    {
        public Autorizacao Autorizacao { get; set; }
        public List<Grupo> GruposList { get; set; }

        public CreateEditAutorizacaoViewModel()
        {
            Autorizacao = new Autorizacao();
            GruposList = new List<Grupo>();
        }
    }
}