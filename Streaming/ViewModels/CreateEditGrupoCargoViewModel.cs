using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Cargos;

namespace Streaming.ViewModels
{
    public class CreateEditGrupoCargoViewModel
    {
        public Grupo Grupo { get; set; }
        public List<TipoCargo> TiposCargosList { get; set; }

        public CreateEditGrupoCargoViewModel()
        {
            Grupo = new Grupo();
            TiposCargosList = new List<TipoCargo>();
        }
    }
}