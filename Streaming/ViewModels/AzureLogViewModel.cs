﻿namespace Streaming.ViewModels
{
    using StreamingAssemblies.Models.Azure.Logs;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="AzureLogViewModel" />.
    /// </summary>
    public class AzureLogViewModel
    {
        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the FullName.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the DateTime.
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the VmName.
        /// </summary>
        public string VmName { get; set; }

        /// <summary>
        /// Gets or sets the Action.
        /// </summary>
        public AzureLogAction Action { get; set; } = AzureLogAction.Unknown;

        /// <summary>
        /// The FromAzureLog.
        /// </summary>
        /// <param name="log">The log<see cref="AzureLog"/>.</param>
        /// <returns>The <see cref="AzureLogViewModel"/>.</returns>
        public static AzureLogViewModel FromAzureLog(AzureLog log)
        {
            try
            {
                if (log == null)
                {
                    return null;
                }

                return new AzureLogViewModel()
                {
                    Action = log.Action,
                    DateTime = log.DateTime,
                    Message = log.Message,
                    FullName = log.FullName,
                    Username = log.Username,
                    VmName = log.VmName
                };
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// The FromAzureLog.
        /// </summary>
        /// <param name="logs">The logs<see cref="IEnumerable{AzureLog}"/>.</param>
        /// <returns>The <see cref="IEnumerable{AzureLogViewModel}"/>.</returns>
        public static IEnumerable<AzureLogViewModel> FromAzureLog(IEnumerable<AzureLog> logs)
        {
            try
            {
                if (logs == null)
                {
                    return null;
                }

                var res = new List<AzureLogViewModel>();

                foreach (var log in logs)
                {
                    res.Add(FromAzureLog(log));
                }

                return res;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
