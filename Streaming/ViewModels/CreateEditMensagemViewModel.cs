using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Mensagens;

namespace Streaming.ViewModels
{
    public class CreateEditMensagemViewModel
    {
        public Mensagem Mensagem { get; set; }
        public string HoraPublicacaoInicio { get; set; }
        public string HoraPublicacaoFim { get; set; }
        public List<Visibilidade> VisibilidadesList { get; set; }
        public List<Grupo> GruposList { get; set; }

        public CreateEditMensagemViewModel()
        {
            Mensagem = new Mensagem();
            HoraPublicacaoInicio = "hh:mm";
            HoraPublicacaoFim = "hh:mm";
            VisibilidadesList = new List<Visibilidade>();
            GruposList = new List<Grupo>();
        }
    }
}