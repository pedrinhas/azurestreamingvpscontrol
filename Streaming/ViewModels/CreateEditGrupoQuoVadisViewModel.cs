using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StreamingAssemblies.Models.Streaming;

namespace Streaming.ViewModels
{
    public class CreateEditGrupoQuoVadisViewModel
    {
        public Grupo Grupo { get; set; }

        public List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo> GruposQuoVadisList { get; set; }
        public string GrupoQuoVadisID { get; set; }

        public CreateEditGrupoQuoVadisViewModel()
        {
            Grupo = new Grupo();

            GruposQuoVadisList = new List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo>();
            GrupoQuoVadisID = "";
        }
    }
}