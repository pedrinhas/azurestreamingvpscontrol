using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Mensagens;

namespace Streaming.ViewModels
{
    public class BaseViewModel
    {
        public List<Mensagem> MensagensList { get; set; }

        public bool IsAutorizacoesEnabled { get; set; }
        public List<Autorizacao> AutorizacoesList { get; set; }

        public BaseViewModel()
        {
            MensagensList = new List<Mensagem>();

            IsAutorizacoesEnabled = false;
            AutorizacoesList = new List<Autorizacao>();
        }

        public bool CheckAutorizacoesListForItem(string itemName, string pagina)
        {
            if (IsAutorizacoesEnabled == false || AutorizacoesList.SingleOrDefault(a => a.Pagina == pagina && a.ItemName == itemName) != default(Autorizacao))
            {
                return true;
            }

            return false;
        }
    }
}