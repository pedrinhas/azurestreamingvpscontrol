using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Net;
using System.Linq;

using System.Configuration;
using System.Security.Cryptography;
using System.Threading;
using System.Globalization;

using CaptchaMvc.HtmlHelpers;

using Streaming.App_Start;
using Streaming.Clients;
using Streaming.Filters;
using Streaming.Helpers;
using Streaming.ViewModels;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.RCU;

namespace Streaming.Controllers
{
    public class SessionController : BaseController
    {
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        private readonly IUTADClient _iUTADClient = new IUTADClient();
        private readonly RCUClient _rcuClient = new RCUClient();

        [StreamingLoggedOff]
        public ActionResult Login()
        {
            return View();
        }

        // Esta é a função principal de autenticação
        // Verifica credenciais, obtém cargos e perfis do login e, com esses dados, verifica as autorizações que o login possui na aplicação
        // Adicionalmente regista o acesso do login na tabela de registo da base de dados do Streaming
        [StreamingUTAD]
        [StreamingLoggedOff]
        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            // Primeiro valida os campos do nome de utilizador e da palavra-passe
            if (string.IsNullOrEmpty(loginViewModel.LoginCredenciais.Username) == true)
            {
                ModelState.AddModelError("LoginCredenciais.Username", "Tem de introduzir um nome de utilizador válido!");
            }

            if(string.IsNullOrEmpty(loginViewModel.LoginCredenciais.Password) == true)
            {
                ModelState.AddModelError("LoginCredenciais.Password", "Tem de introduzir uma palavra-passe válida!");
            }

            if(ModelState.IsValid == true)
            {
                Login login = new Login();

                // Dependendo do valor da flag de simulação de autenticação presente no Web.config, usa diferentes mecanismos para autenticar o login
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Simul"]) == true)
                {
                    // Usa serviços Web de simulação para autenticar o login
                    login = AutenticacaoSimul.AutenticarLoginWSSimul(loginViewModel.LoginCredenciais.Username, loginViewModel.LoginCredenciais.Password);
                }
                else
                {
                    // Usa o serviço Web real de autenticação, que inclui o mecanismo de autenticação por impersonação
                    // Neste mecanismo, um administrador pode autenticar-se como se fosse um utilizador à sua escolha, e ver o que esse utilizador vê no Streaming
                    bool canAutenticarLoginWS = true;

                    if (loginViewModel.LoginCredenciais.Username.Contains(";") == true)
                    {
                        // Assumindo que o login introduzido se encontra na estrutura "administrador;login", separa o login em dois
                        string[] userAdminImpersonacao = loginViewModel.LoginCredenciais.Username.Split(';');

                        string adminImpersonacao = userAdminImpersonacao[0];
                        string userImpersonacao = userAdminImpersonacao[1];

                        if (string.IsNullOrEmpty(adminImpersonacao) == false && string.IsNullOrEmpty(userImpersonacao) == false)
                        {
                            canAutenticarLoginWS = Autenticacao.IsLoginAdmin(adminImpersonacao);
                        }
                        else
                        {
                            canAutenticarLoginWS = false;
                        }
                    }

                    if (canAutenticarLoginWS == true)
                    {
                        login = _iUTADClient.Auth(loginViewModel.LoginCredenciais.Username, loginViewModel.LoginCredenciais.Password);
                    }
                }

                // Se o login tiver sido autenticado com sucesso, procede para a função de carregamento do login
                if (login != null && login.IUPI != null && login.IUPI != new Guid())
                {
                    // -1 - Login não possui permissões
                    // 0 - Ocorreu um erro ao carregar o login
                    // 1 - Login carregado com sucesso
                    // 2 - Login carregado com sucesso (estudante)
                    int success = LoadLogin(login);

                    switch (success)
                    {
                        case -1:
                            TempData["error"] = "Não possui permissões para aceder a esta aplicação!";

                            return View();

                        case 0:
                            TempData["error"] = "Ocorreu um erro ao carregar a informação pessoal!";

                            return View();

                        case 1:
                            var symbols = HttpHelper.GetLanguageSymbols(Request);
                            if (LoadLoginLinguagem(symbols) == true)
                            {
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                TempData["error"] = "Ocorreu um erro ao carregar informação!";

                                return View();
                            }

                        case 2:
                            return RedirectToAction("LoadEstudantePerfil", new { iupi = login.IUPI });

                        default:
                            break;
                    }
                }
                else
                {
                    // Caso contrário, se o login não tiver sido autenticado com sucesso, significa que as credenciais introduzidas estão erradas
                    // Portanto apresenta uma mensagem de erro
                    TempData["error"] = "Credenciais inválidas! Por favor verifique as credenciais introduzidas e tente de novo.";
                }
            }
            else
            {
                // Caso o utilizador não tenha introduzido o nome de utilizador ou a palavra-passe, apresenta uma mensagem de erro
                TempData["error"] = "Tem de preencher todos os campos!";
            }

            return View();
        }

        [StreamingUTADLoginAdmin]
        [StreamingLoggedOff]
        public ActionResult LoginAdmin()
        {
            return View();
        }

        [StreamingUTADLoginAdmin]
        [StreamingLoggedOff]
        [HttpPost]
        public ActionResult LoginAdmin(LoginViewModel loginViewModel)
        {
            // Primeiro valida os campos do nome de utilizador e da palavra-passe
            if (string.IsNullOrEmpty(loginViewModel.LoginCredenciais.Username) == true)
            {
                ModelState.AddModelError("LoginCredenciais.Username", "Tem de introduzir um nome de utilizador válido!");
            }

            if (string.IsNullOrEmpty(loginViewModel.LoginCredenciais.Password) == true)
            {
                ModelState.AddModelError("LoginCredenciais.Password", "Tem de introduzir uma palavra-passe válida!");
            }

            if (ModelState.IsValid == true)
            {
                Login login = new Login();

                // Primeiro verifica se as credenciais introduzidas correspondem a um administrador válido do Streaming
                bool isAdmin = false;

                if (loginViewModel.LoginCredenciais.Username.Contains(";") == true)
                {
                    // Assumindo que o login introduzido se encontra na estrutura "administrador;login", separa o login em dois
                    string[] userAdminImpersonacao = loginViewModel.LoginCredenciais.Username.Split(';');

                    string adminImpersonacao = userAdminImpersonacao[0];
                    string userImpersonacao = userAdminImpersonacao[1];

                    if (string.IsNullOrEmpty(adminImpersonacao) == false && string.IsNullOrEmpty(userImpersonacao) == false)
                    {
                        isAdmin = Autenticacao.IsLoginAdmin(adminImpersonacao);
                    }
                }
                else
                {
                    isAdmin = Autenticacao.IsLoginAdmin(loginViewModel.LoginCredenciais.Username);
                }

                if (isAdmin == true)
                {
                    // Dependendo do valor da flag de simulação de autenticação presente no Web.config, usa diferentes mecanismos para autenticar o login
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Simul"]) == true)
                    {
                        // Usa serviços Web de simulação para autenticar o login
                        login = AutenticacaoSimul.AutenticarLoginWSSimul(loginViewModel.LoginCredenciais.Username, loginViewModel.LoginCredenciais.Password);
                    }
                    else
                    {
                        // Usa o serviço Web real de autenticação, que inclui o mecanismo de autenticação por impersonação
                        // Neste mecanismo, um administrador pode autenticar-se como se fosse um utilizador à sua escolha, e ver o que esse utilizador vê no Streaming
                        login = _iUTADClient.Auth(loginViewModel.LoginCredenciais.Username, loginViewModel.LoginCredenciais.Password);
                    }

                    // Se o login tiver sido autenticado com sucesso, procede para a função de carregamento do login
                    if (login != null && login.IUPI != null && login.IUPI != new Guid())
                    {
                        // -1 - Login não possui permissões
                        // 0 - Ocorreu um erro ao carregar o login
                        // 1 - Login carregado com sucesso
                        // 2 - Login carregado com sucesso (estudante)
                        int success = LoadLogin(login);

                        switch (success)
                        {
                            case -1:
                                TempData["error"] = "Não possui permissões para aceder a esta aplicação!";

                                return View();

                            case 0:
                                TempData["error"] = "Ocorreu um erro ao carregar a informação pessoal!";

                                return View();

                            case 1:
                                var symbols = HttpHelper.GetLanguageSymbols(Request);
                                if (LoadLoginLinguagem(symbols) == true)
                                {
                                    return RedirectToAction("Index", "Home");
                                }
                                else
                                {
                                    TempData["error"] = "Ocorreu um erro ao carregar informação!";

                                    return View();
                                }

                            case 2:
                                return RedirectToAction("LoadEstudantePerfil", new { iupi = login.IUPI });

                            default:
                                break;
                        }
                    }
                }

                // Caso contrário, se o login não tiver sido autenticado com sucesso, significa que as credenciais introduzidas estão erradas
                // Portanto apresenta uma mensagem de erro
                TempData["error"] = "Credenciais inválidas! Por favor verifique as credenciais introduzidas e tente de novo.";
            }
            else
            {
                // Caso o administrador não tenha introduzido o nome de utilizador ou a palavra-passe, apresenta uma mensagem de erro
                TempData["error"] = "Tem de preencher todos os campos!";
            }

            return View();
        }

        private int LoadLogin(Login login)
        {
            // Começa por registar o acesso deste login, guardando o seu ID na sessão
            bool success = Autenticacao.RegistarLoginAcesso(login.Username, login.IUPI, login.Nome, DateTime.Now, true);

            if (success == true)
            {
                // Obtém os cargos e os perfis do login (incluíndo os não ativos), guardando todos esses dados nas respetivas variáveis de sessão
                // Dependendo do valor da flag de simulação de autenticação presente no Web.config, usa diferentes mecanismos para obter esses cargos e perfis
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Simul"]) == true)
                {
                    success = AutenticacaoSimul.LoadLoginCargosAndPerfisSimul(login.Username, login.IUPI);
                }
                else
                {
                    success = Autenticacao.LoadLoginCargosAndPerfis(login.Username, login.IUPI);
                }

                if (success == true)
                {
                    // Só procede com a verificação de permissões apenas se tal mecanismo estiver ativado no Web.config
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]) == true)
                    {
                        // Em adição aos cargos e aos perfis obtidos anteriormente, obtém os grupos do QuoVadis a que o login pertence
                        // Com todos estes dados, verifica se o login possui permissões para aceder à aplicação, verificando as suas autorizações
                        success = Autenticacao.LoadLoginGruposQuoVadis(login.IUPI) && Autenticacao.LoadLoginGruposAndAutorizacoes();

                        if (success == false)
                        {
                            return -1;
                        }
                    }
                }

                if (success == true)
                {
                    // Carrega toda a informação pessoal do login
                    // A partir daqui o login é considerado autenticado pelo Streaming, e "Autenticacao.IsLoggedIn" passa a retornar true
                    if (Autenticacao.LoadLoginInfo(login.Username, login.IUPI) == true)
                    {
                        string usernameSplit = Utilities.GetNumMecFromUsername(login.Username);

                        if (string.IsNullOrEmpty(usernameSplit) == false)
                        {
                            return 2;
                        }
                        else
                        {
                            return 1;
                        }
                    }
                }
            }

            return 0;
        }

        [StreamingUTAD]
        [StreamingLoggedOff]
        public ActionResult LoadEstudantePerfil(Guid iupi)
        {
            // Verifica o número de perfis (matrículas) ativos
            List<EstudantePerfil> estudantePerfis = _rcuClient.GetEstudantePerfis(iupi).Where(ep => ep.IdEstado == "1").ToList();

            if (estudantePerfis != null && estudantePerfis.Count > 0)
            {
                // Se o número de perfis (matrículas) ativos do estudante for apenas um, guarda os dados desse perfil (matrícula) na sessão do browser e termina o processo
                // Caso o estudante possua mais do que um perfil (matrícula), esse mesmo estudante terá de escolher que perfil (matrícula) deseja usar para se autenticar no Streaming
                if (estudantePerfis.Count == 1)
                {
                    Autenticacao.LoadEstudantePerfilInfo(estudantePerfis.First().Nome, estudantePerfis.First().CodCurso, estudantePerfis.First().NomeAcesso);

                    var symbols = HttpHelper.GetLanguageSymbols(Request);
                    if (LoadLoginLinguagem(symbols) == true)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData["error"] = "Ocorreu um erro ao carregar informação!";

                        return RedirectToAction("Login");
                    }
                }
                else
                {
                    List<LoadEstudantePerfilViewModel> loadEstudantePerfilViewModels = new List<LoadEstudantePerfilViewModel>();

                    foreach (EstudantePerfil ep in estudantePerfis)
                    {
                        LoadEstudantePerfilViewModel loadEstudantePerfilViewModel = new LoadEstudantePerfilViewModel();

                        loadEstudantePerfilViewModel.Nome = ep.Nome;
                        loadEstudantePerfilViewModel.CodCurso = ep.CodCurso;
                        loadEstudantePerfilViewModel.Curso = ep.Curso;
                        loadEstudantePerfilViewModel.NomeAcesso = ep.NomeAcesso;

                        loadEstudantePerfilViewModels.Add(loadEstudantePerfilViewModel);
                    }

                    return View(loadEstudantePerfilViewModels);
                }
            }

            TempData["error"] = "Ocorreu um erro ao carregar a informação pessoal!";

            return RedirectToAction("Login");
        }

        [StreamingUTAD]
        [StreamingLoggedOff]
        [HttpPost]
        public ActionResult LoadEstudantePerfil(LoadEstudantePerfilViewModel loadEstudantePerfilViewModel)
        {
            Autenticacao.LoadEstudantePerfilInfo(loadEstudantePerfilViewModel.Nome, loadEstudantePerfilViewModel.CodCurso, loadEstudantePerfilViewModel.NomeAcesso);

            var symbols = HttpHelper.GetLanguageSymbols(Request);
            if (LoadLoginLinguagem(symbols) == true)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = "Ocorreu um erro ao carregar informação!";

                return RedirectToAction("Login");
            }
        }

        [StreamingCredenciaisTemporarias]
        [StreamingLoggedOff]
        [HttpPost]
        public ActionResult LoginCredenciaisTemporarias(FormCollection formCollection)
        {
            // Primeiro valida os campos do nome de utilizador temporário e da palavra-passe
            if (string.IsNullOrEmpty(formCollection["LoginCredenciaisTemporarias.Username"]) == true)
            {
                ModelState.AddModelError("LoginCredenciaisTemporarias.Username", "Tem de introduzir um endereço de e-mail válido!");
            }

            if (string.IsNullOrEmpty(formCollection["LoginCredenciaisTemporarias.Password"]) == true)
            {
                ModelState.AddModelError("LoginCredenciaisTemporarias.Password", "Tem de introduzir uma palavra-passe válida!");
            }

            if (ModelState.IsValid == true)
            {
                CredenciaisTemporarias credenciaisTemporarias = _rcuClient.GetCredenciaisTemporariasPorUsername(formCollection["LoginCredenciaisTemporarias.Username"]);

                // Se conseguiu obter um objeto de credenciais temporárias válido através do nome de utilizador temporário introduzido, tenta assim validar a palavra-passe introduzida
                if (credenciaisTemporarias != null && credenciaisTemporarias.Id > 0 && ValidatePassword(formCollection["LoginCredenciaisTemporarias.Password"], credenciaisTemporarias.Password) == true)
                {
                    // Depois verifica a data de validade das credenciais temporárias
                    // Se já expiraram, para todo o processo e apresenta uma mensagem de erro
                    if (credenciaisTemporarias.DataValidade == new DateTime() || credenciaisTemporarias.DataValidade > DateTime.Now)
                    {
                        // Por fim verifica se as credenciais temporárias pertencem ao domínio da aplicação
                        // Se não pertencem, significa que o utilizador está a tentar entrar nesta aplicação com as credenciais temporárias de outra aplicação
                        // Portanto para todo o processo e apresenta uma mensagem de erro
                        Parametro credenciaisTemporariasDominio = _streamingService.NSI_STP_Parametros_S_ByChave("Auth.CredenciaisTemporarias.Dominio");

                        if(string.IsNullOrEmpty(credenciaisTemporariasDominio.Valor) == false)
                        {
                            if (credenciaisTemporarias.Username.Contains(credenciaisTemporariasDominio.Valor) == true)
                            {
                                bool success = Autenticacao.RegistarLoginAcesso(credenciaisTemporarias.Username, credenciaisTemporarias.IUPI, credenciaisTemporarias.Nome, DateTime.Now, false);

                                if (success == true)
                                {
                                    // Só procede com a verificação de permissões apenas se tal mecanismo estiver ativado no Web.config
                                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]) == true)
                                    {
                                        // Com as credenciais temporárias obtidas anteriormente, cria um objeto perfil e guarda-o sessão do browser
                                        // Com estes dados ir-se-á verificar se o login possui permissões para aceder à aplicação
                                        SessionPerfil credenciaisTemporariasPerfil = new SessionPerfil();

                                        credenciaisTemporariasPerfil.Id = credenciaisTemporarias.Id;
                                        credenciaisTemporariasPerfil.Ano = credenciaisTemporarias.DataCriacao.Year;
                                        credenciaisTemporariasPerfil.Tipo = credenciaisTemporarias.TipoPerfil;

                                        // Se se chegou até aqui, significa que as credenciais temporárias se encontram válidas e activas
                                        credenciaisTemporariasPerfil.Ativo = true;

                                        StreamingSession.Session.Perfis.Add(credenciaisTemporariasPerfil);

                                        success = Autenticacao.LoadLoginGruposAndAutorizacoes();

                                        if (success == false)
                                        {
                                            TempData["error"] = "Não possui permissões para aceder a esta aplicação!";

                                            return RedirectToAction("Login");
                                        }
                                    }

                                    if(success == true)
                                    {
                                        // Carrega toda a informação pessoal das credenciais temporárias
                                        // A partir daqui as credenciais temporárias são consideradas autenticadas pelo Streaming
                                        StreamingSession.Session.NMec = credenciaisTemporarias.NMec;
                                        StreamingSession.Session.Username = credenciaisTemporarias.Username;
                                        StreamingSession.Session.IUPI = credenciaisTemporarias.IUPI;
                                        StreamingSession.Session.Nome = credenciaisTemporarias.Nome;

                                        // Define o idioma do Streaming como sendo o que o utilizador definiu aquando do registo das suas credenciais temporárias
                                        if (LoadLoginLinguagem(credenciaisTemporarias.DefaultLanguage) == true)
                                        {
                                            return RedirectToAction("Index", "Home");
                                        }
                                        else
                                        {
                                            TempData["error"] = "Ocorreu um erro ao carregar informação!";
                                        }
                                    }
                                }
                                else
                                {
                                    TempData["error"] = "Ocorreu um erro ao carregar a informação pessoal!";
                                }
                            }
                            else
                            {
                                TempData["error"] = "As suas credenciais temporárias pertencem a uma aplicação diferente!";
                            }
                        }
                        else
                        {
                            TempData["error"] = "Ocorreu um erro ao carregar a informação pessoal!";
                        }
                    }
                    else
                    {
                        TempData["error"] = "As suas credenciais temporárias expiraram!";
                    }
                }
                else
                {
                    TempData["error"] = "Credenciais inválidas! Por favor verifique as credenciais introduzidas e tente de novo.";
                }
            }
            else
            {
                // Caso o utilizador não tenha introduzido o nome de utilizador temporário ou a palavra-passe, apresenta uma mensagem de erro
                TempData["error"] = "Tem de preencher todos os campos!";
            }

            return RedirectToAction("Login");
        }

        [StreamingCredenciaisTemporarias]
        [StreamingCredenciaisTemporariasRegisto]
        [StreamingLoggedOff]
        public ActionResult RegistarCredenciaisTemporarias()
        {
            return View();
        }

        [StreamingCredenciaisTemporarias]
        [StreamingCredenciaisTemporariasRegisto]
        [StreamingLoggedOff]
        [HttpPost]
        public ActionResult RegistarCredenciaisTemporarias(RegistoCredenciaisTemporarias registoCredenciaisTemporarias)
        {
            if(string.IsNullOrEmpty(registoCredenciaisTemporarias.Nome) == true)
            {
                ModelState.AddModelError("Nome", "Tem de introduzir um nome válido!");
            }

            if(string.IsNullOrEmpty(registoCredenciaisTemporarias.Email) == true)
            {
                ModelState.AddModelError("Email", "Tem de introduzir um endereço de e-mail válido!");
            }
            else
            {
                if(Utilities.CheckEmailString(registoCredenciaisTemporarias.Email) == false)
                {
                    ModelState.AddModelError("Email", "O endereço de e-mail introduzido não é válido!");
                }
            }
            
            if(string.IsNullOrEmpty(registoCredenciaisTemporarias.EmailRepeticao) == true)
            {
                ModelState.AddModelError("EmailRepeticao", "Tem de introduzir de novo o seu endereço de e-mail!");
            }

            if((string.IsNullOrEmpty(registoCredenciaisTemporarias.Email) == false && string.IsNullOrEmpty(registoCredenciaisTemporarias.EmailRepeticao) == false) && registoCredenciaisTemporarias.Email != registoCredenciaisTemporarias.EmailRepeticao)
            {
                ModelState.AddModelError("EmailRepeticao", "Os dois endereços de e-mail não coincidem!");
            }

            if(string.IsNullOrEmpty(registoCredenciaisTemporarias.DefaultLanguage) == true)
            {
                ModelState.AddModelError("DefaultLanguage", "Tem de seleccionar um idioma de apresentação!");
            }

            // Adicionalmente, valida também o captcha
            if (this.IsCaptchaValid("Tem de validar o captcha!") == false)
            {
                ModelState.AddModelError("Captcha", "Tem de validar o captcha!");
            }

            if (ModelState.IsValid == true)
            {
                Parametro credenciaisTemporariasDominio = _streamingService.NSI_STP_Parametros_S_ByChave("Auth.CredenciaisTemporarias.Dominio");
                Parametro credenciaisTemporariasDiasValidade = _streamingService.NSI_STP_Parametros_S_ByChave("Auth.CredenciaisTemporarias.DiasValidade");
                Parametro mailBCC = _streamingService.NSI_STP_Parametros_S_ByChave("Mail.DeveloperAddress");

                if (string.IsNullOrEmpty(credenciaisTemporariasDominio.Valor) == false && string.IsNullOrEmpty(credenciaisTemporariasDiasValidade.Valor) == false)
                {
                    CredenciaisTemporariasInsert credenciaisTemporariasInsert = new CredenciaisTemporariasInsert();

                    credenciaisTemporariasInsert.IUPI = Guid.NewGuid();
                    credenciaisTemporariasInsert.Nome = registoCredenciaisTemporarias.Nome;
                    credenciaisTemporariasInsert.Email = registoCredenciaisTemporarias.Email;
                    credenciaisTemporariasInsert.Dominio = credenciaisTemporariasDominio.Valor;

                    // Gera uma palavra-passe de 8 caracteres aleatórios
                    // Esta palavra-passe descodificada irá ser enviada por e-mail para o novo utilizador temporário
                    string password = PasswordGenerator.GeneratePassword();

                    // Pega na palavra-passe gerada anteriormente e codifica-a
                    // Esta palavra-passe codificada irá ser armazenada na base de dados de credenciais temporárias
                    credenciaisTemporariasInsert.Password = PasswordGenerator.EncodePassword(password);

                    credenciaisTemporariasInsert.SistemaCriacao = ConfigurationManager.AppSettings["Aplicacao.IdentificadorPortalBase"];
                    credenciaisTemporariasInsert.DiasValidade = Convert.ToInt32(credenciaisTemporariasDiasValidade.Valor);
                    credenciaisTemporariasInsert.DefaultLanguage = registoCredenciaisTemporarias.DefaultLanguage;

                    // Regista as novas credenciais temporárias na base de dados, obtendo no fim o novo nome de utilizador
                    string username = _rcuClient.CreateCredenciaisTemporarias(credenciaisTemporariasInsert);

                    // Se o registo anterior for efetuado com sucesso, retorna um nome de utilizador válido
                    // Se esse for o caso, procede
                    if (string.IsNullOrEmpty(username) == false)
                    {
                        // Compõe e envia um e-mail de confirmação de registo, com o nome de utilizador e a palavra-passe criadas anteriormente, para o endereço de e-mail especificado pelo utilizador
                        string registoCredenciaisTemporariasMailSubject = ConfigurationManager.AppSettings["Aplicacao.Titulo"] + " - Registo de Novas Credenciais Temporárias";
                        string registoCredenciaisTemporariasMailBody = "Caro(a) " + credenciaisTemporariasInsert.Nome + "," + Environment.NewLine + Environment.NewLine + "O seu registo foi efetuado com sucesso. Enviamos neste e-mail as credenciais temporárias que deve usar para se autenticar na plataforma " + ConfigurationManager.AppSettings["Aplicacao.Titulo"] + ":" + Environment.NewLine + Environment.NewLine + "Nome de Utilizador: " + username + Environment.NewLine + "Palavra-Passe: " + password + Environment.NewLine + Environment.NewLine + "Informamos também que as suas credenciais temporárias possuem uma validade de " + credenciaisTemporariasInsert.DiasValidade + " dias.";

                        // Se conseguir enviar o e-mail com sucesso, redirecciona o utilizador para a página de autenticação e o processo fica concluído
                        if (Mailer.SendMailFromUTAD(credenciaisTemporariasInsert.Email, mailBCC.Valor, registoCredenciaisTemporariasMailSubject, registoCredenciaisTemporariasMailBody, null) == true)
                        {
                            TempData["success"] = "Registo efetuado com sucesso! Por favor verfique a sua caixa de correio electrónico.";

                            return RedirectToAction("Login");
                        }
                    }
                }

                TempData["error"] = "Ocorreu um erro ao submeter dados! Por favor tente de novo.";
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }          

            return View(registoCredenciaisTemporarias);
        }

        [StreamingCredenciaisTemporarias]
        [StreamingCredenciaisTemporariasRecuperacao]
        [StreamingLoggedOff]
        [HttpPost]
        public ActionResult RecuperarCredenciaisTemporarias(FormCollection formCollection)
        {
            // Primeiro valida o campo do endereço de e-mail/nome de utilizador
            if (string.IsNullOrEmpty(formCollection["EmailRecuperarCredenciaisTemporarias"]) == true)
            {
                ModelState.AddModelError("EmailRecuperarCredenciaisTemporarias", "Tem de introduzir um endereço de e-mail válido!");
            }

            if (ModelState.IsValid == true)
            {
                Parametro credenciaisTemporariasDominio = _streamingService.NSI_STP_Parametros_S_ByChave("Auth.CredenciaisTemporarias.Dominio");
                Parametro mailBCC = _streamingService.NSI_STP_Parametros_S_ByChave("Mail.DeveloperAddress");

                if (string.IsNullOrEmpty(credenciaisTemporariasDominio.Valor) == false)
                {
                    int areCredenciaisTemporariasValidas = 0;

                    // Se o utilizador tiver introduzido o seu endereço de e-mail/nome de utilizador, tenta obter as credenciais temporárias correspondentes
                    // Dependendo do que o utilizador tiver introduzido, tenta obter um objeto de credenciais temporárias válido através de webservices diferentes
                    CredenciaisTemporarias credenciaisTemporarias = _rcuClient.GetCredenciaisTemporariasPorUsername(formCollection["EmailRecuperarCredenciaisTemporarias"]);

                    // Corre as credenciais temporárias obtidas por uma série de verificações
                    if (credenciaisTemporarias != null && credenciaisTemporarias.Id > 0)
                    {
                        // Verifica se as credenciais temporárias pertencem ao domínio da aplicação
                        // Se não pertencem, significa que o utilizador está a tentar recuperar as credenciais temporárias de outra aplicação
                        if (credenciaisTemporarias.Username.Contains(credenciaisTemporariasDominio.Valor) == true)
                        {
                            // Por fim, verifica a data de validade dessas mesmas credenciais
                            if (credenciaisTemporarias.DataValidade == new DateTime() || credenciaisTemporarias.DataValidade > DateTime.Now)
                            {
                                areCredenciaisTemporariasValidas = 1;
                            }
                            else
                            {
                                areCredenciaisTemporariasValidas = 2;
                            }
                        }
                    }
                    else
                    {
                        // Se não conseguiu obter as credenciais temporárias a partir do nome de utilizador, tenta obtê-las a partir do endereço de e-mail
                        // Depois volta a correr as credenciais temporárias obtidas pelas mesmas verificações
                        List<CredenciaisTemporarias> credenciaisTemporariasList = _rcuClient.GetCredenciaisTemporariasPorEmail(formCollection["EmailRecuperarCredenciaisTemporarias"]);

                        if (credenciaisTemporariasList != null && credenciaisTemporariasList.Count > 0)
                        {
                            credenciaisTemporariasList = credenciaisTemporariasList.Where(ct => ct.Username.Contains(credenciaisTemporariasDominio.Valor) == true).ToList();

                            if (credenciaisTemporariasList.Count() > 0)
                            {
                                credenciaisTemporarias = credenciaisTemporariasList.SingleOrDefault(ct => ct.DataValidade == new DateTime() || ct.DataValidade > DateTime.Now);

                                if (credenciaisTemporarias != default(CredenciaisTemporarias))
                                {
                                    areCredenciaisTemporariasValidas = 1;
                                }
                                else
                                {
                                    areCredenciaisTemporariasValidas = 2;
                                }
                            }
                        }
                        else
                        {
                            areCredenciaisTemporariasValidas = -1;
                        }
                    }

                    switch (areCredenciaisTemporariasValidas)
                    {
                        case -1:
                            TempData["error"] = "O endereço de e-mail ou o nome de utilizador introduzido não é válido!";

                            break;

                        case 0:
                            TempData["error"] = "As suas credenciais temporárias pertencem a uma aplicação diferente!";

                            break;

                        case 1:
                            // Gera um novo token de recuperação de credenciais temporárias, com 12 horas de validade, e guarda-o na base de dados
                            CredenciaisTemporariasTokenInsert credenciaisTemporariasTokenInsert = new CredenciaisTemporariasTokenInsert();

                            credenciaisTemporariasTokenInsert.IdCredenciaisTemporarias = credenciaisTemporarias.Id;
                            credenciaisTemporariasTokenInsert.HorasValidade = 12;

                            Guid? token = _rcuClient.CreateCredenciaisTemporariasToken(credenciaisTemporariasTokenInsert);

                            if (token != null && token != new Guid())
                            {
                                // Compõe e envia um e-mail de recuperação de palavra-passe, com o token de recuperação gerado anteriormente, para o endereço de e-mail especificado pelo utilizador
#if DEBUG
                                string recuperacaoCredenciaisTemporariasLink = Request.Url.Scheme + "://" + Request.Url.Authority + Url.Action("RecuperarCredenciaisTemporarias", "Session", new { token = token });
#else
                                string recuperacaoCredenciaisTemporariasLink = Request.Url.Scheme + "://" + Request.Url.Host + Url.Action("RecuperarCredenciaisTemporarias", "Session", new { token = token });
#endif

                                string recuperacaoCredenciaisTemporariasMailSubject = ConfigurationManager.AppSettings["Aplicacao.Titulo"] + " - Recuperação de Credenciais Temporárias";
                                string recuperacaoCredenciaisTemporariasMailBody = "Caro(a) " + credenciaisTemporarias.Nome + "," + Environment.NewLine + Environment.NewLine + "Enviamos neste e-mail um link de recuperação, em que deve clicar para recuperar as suas credenciais temporárias para a plataforma " + ConfigurationManager.AppSettings["Aplicacao.Titulo"] + "." + Environment.NewLine + Environment.NewLine + "O link de recuperação é: " + recuperacaoCredenciaisTemporariasLink;

                                if (Mailer.SendMailFromUTAD(credenciaisTemporarias.Email, mailBCC.Valor, recuperacaoCredenciaisTemporariasMailSubject, recuperacaoCredenciaisTemporariasMailBody, null) == true)
                                {
                                    TempData["success"] = "Foi enviado um e-mail de recuperação. Por favor verfique a sua caixa de correio electrónico.";

                                    return RedirectToAction("Login");
                                }
                            }

                            TempData["error"] = "Ocorreu um erro ao submeter dados! Por favor tente de novo.";

                            break;

                        case 2:
                            TempData["error"] = "As suas credenciais expiraram!";

                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados! Por favor tente de novo.";
                }
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return RedirectToAction("Login");
        }

        [StreamingCredenciaisTemporarias]
        [StreamingCredenciaisTemporariasRecuperacao]
        [StreamingLoggedOff]
        public ActionResult RecuperarCredenciaisTemporarias(Guid token)
        {
            if(token != null & token != new Guid())
            {
                // Tenta obter o token de recuperação de credenciais temporárias anteriormente gerado
                CredenciaisTemporariasToken credenciaisTemporariasToken = _rcuClient.GetCredenciaisTemporariasToken(token);

                if(credenciaisTemporariasToken != null && credenciaisTemporariasToken.Id > 0)
                {
                    // Verifica se o token de recuperação de credenciais temporárias obtido ainda não foi usado
                    if (credenciaisTemporariasToken.EstadoUso == false)
                    {
                        // Verifica inclusive se o token de recuperação de credenciais temporárias ainda se encontra dentro da validade
                        if (credenciaisTemporariasToken.DataExpiracao > DateTime.Now)
                        {
                            // Se o token de recuperação de credenciais temporárias passar por todas as verificações, obtém as credenciais temporárias correspondentes ao token de recuperação
                            CredenciaisTemporarias credenciaisTemporarias = _rcuClient.GetCredenciaisTemporarias(credenciaisTemporariasToken.IdCredenciaisTemporarias);

                            if(credenciaisTemporarias != null && credenciaisTemporarias.Id > 0)
                            {
                                CredenciaisTemporariasPasswordUpdate credenciaisTemporariasPasswordUpdate = new CredenciaisTemporariasPasswordUpdate();

                                credenciaisTemporariasPasswordUpdate.Id = credenciaisTemporarias.Id;

                                // Gera uma nova palavra-passe de 8 caracteres aleatórios
                                // Esta palavra-passe descodificada irá ser enviada pelo e-mail de recuperação para o utilizador temporário
                                string password = PasswordGenerator.GeneratePassword();

                                // Pega na palavra-passe gerada anteriormente e codifica-a
                                // Esta palavra-passe codificada irá ser armazenada na base de dados de credenciais temporárias
                                credenciaisTemporariasPasswordUpdate.Password = PasswordGenerator.EncodePassword(password);

                                // Atualiza as credenciais temporárias presentes na base de dados com a nova palavra-passe gerada e codificada
                                if (_rcuClient.UpdateCredenciaisTemporariasPassword(credenciaisTemporariasPasswordUpdate) > 0)
                                {
                                    if (_rcuClient.UpdateCredenciaisTemporariasTokenEstadoUso(credenciaisTemporariasToken.Id) > 0)
                                    {
                                        // Compõe e envia um e-mail de novas credenciais temporárias, com a palavra-passe criada anteriormente, para o endereço de e-mail especificado pelo utilizador
                                        string recuperacaoCredenciaisTemporariasMailSubject = ConfigurationManager.AppSettings["Aplicacao.Titulo"] + " - Recuperação de Credenciais Temporárias";
                                        string recuperacaoCredenciaisTemporariasMailBody = "Caro(a) " + credenciaisTemporarias.Nome + "," + Environment.NewLine + Environment.NewLine + "Enviamos neste e-mail as novas credenciais temporárias que deve usar para se autenticar na plataforma " + ConfigurationManager.AppSettings["Aplicacao.Titulo"] + ":" + Environment.NewLine + Environment.NewLine + "Nome de Utilizador: " + credenciaisTemporarias.Username + Environment.NewLine + "Palavra-Passe: " + password;

                                        Parametro mailBCC = _streamingService.NSI_STP_Parametros_S_ByChave("Mail.DeveloperAddress");

                                        if (Mailer.SendMailFromUTAD(credenciaisTemporarias.Email, mailBCC.Valor, recuperacaoCredenciaisTemporariasMailSubject, recuperacaoCredenciaisTemporariasMailBody, null) == true)
                                        {
                                            TempData["success"] = "Foi enviado um e-mail com as suas novas credenciais. Por favor verfique a sua caixa de correio electrónico.";

                                            return RedirectToAction("Login");
                                        }
                                    }
                                }
                            }

                            TempData["error"] = "Ocorreu um erro ao submeter dados! Por favor tente de novo.";
                        }
                        else
                        {
                            TempData["error"] = "Este link de recuperação já se encontra expirado! Por favor tente de novo.";
                        }
                    }
                    else
                    {
                        TempData["error"] = "Este link de recuperação já foi usado! Por favor tente de novo.";
                    }
                }
                else
                {
                    TempData["error"] = "Ocorreu um erro ao submeter dados! Por favor tente de novo.";
                }
            }

            return RedirectToAction("Login");
        }

        private bool ValidatePassword(string password, string storedPassword)
        {
            bool success = false;

            try
            {
                // Em primeiro lugar converte a string da palavra-passe guardada na base de dados de volta para um array de bytes
                byte[] storedPasswordBytes = new byte[36];

                storedPasswordBytes = Utilities.DecodeBase64StringToByteArray(storedPassword);

                // A partir do array de bytes obtido, obtém a salt que foi usada para fazer hashing à palavra-passe guardada na base de dados
                byte[] storedPasswordSalt = new byte[16];

                Array.Copy(storedPasswordBytes, 0, storedPasswordSalt, 0, 16);

                // Com a salt obtida, tenta fazer hashing à palavra-passe introduzida pelo utilizador no ecrã de login
                Rfc2898DeriveBytes passwordHashingAlgorithm = new Rfc2898DeriveBytes(password, storedPasswordSalt, 10000);

                byte[] hashedPassword = new byte[20];

                hashedPassword = passwordHashingAlgorithm.GetBytes(20);

                // A partir do array de bytes obtido inicialmente, obtém agora a palavra-passe codificada guardada na base de dados
                byte[] storedHashedPassword = new byte[20];

                Array.Copy(storedPasswordBytes, 16, storedHashedPassword, 0, 20);

                // Compara as duas palavras-passe codificadas
                // Se ambas forem exatamente iguais, significa que o utilizador introduziu a palavra-passe correta no ecrã de login
                // Portanto retorna true
                success = hashedPassword.SequenceEqual(storedHashedPassword);
            }
            catch(Exception e)
            {

            }

            return success;
        }

        [StreamingCredenciaisTemporarias]
        [StreamingLoggedIn]
        public ActionResult EditCredenciaisTemporarias(string username)
        {
            CredenciaisTemporarias credenciaisTemporarias = _rcuClient.GetCredenciaisTemporariasPorUsername(username);

            CredenciaisTemporariasUpdate credenciaisTemporariasUpdate = new CredenciaisTemporariasUpdate();

            credenciaisTemporariasUpdate.Id = credenciaisTemporarias.Id;
            credenciaisTemporariasUpdate.Nome = credenciaisTemporarias.Nome;
            credenciaisTemporariasUpdate.Email = credenciaisTemporarias.Email;
            credenciaisTemporariasUpdate.DefaultLanguage = credenciaisTemporarias.DefaultLanguage;

            return View(credenciaisTemporariasUpdate);
        }

        [StreamingCredenciaisTemporarias]
        [StreamingLoggedIn]
        [HttpPost]
        public ActionResult EditCredenciaisTemporarias(CredenciaisTemporariasUpdate credenciaisTemporariasUpdate)
        {
            if (string.IsNullOrEmpty(credenciaisTemporariasUpdate.Nome) == true)
            {
                ModelState.AddModelError("Nome", "Tem de introduzir um nome válido!");
            }

            if (string.IsNullOrEmpty(credenciaisTemporariasUpdate.Email) == true)
            {
                ModelState.AddModelError("Email", "Tem de introduzir um endereço de e-mail válido!");
            }
            else
            {
                if (Utilities.CheckEmailString(credenciaisTemporariasUpdate.Email) == false)
                {
                    ModelState.AddModelError("Email", "O endereço de e-mail introduzido não é válido!");
                }
            }

            if (string.IsNullOrEmpty(credenciaisTemporariasUpdate.DefaultLanguage) == true)
            {
                ModelState.AddModelError("DefaultLanguage", "Tem de seleccionar um idioma de apresentação!");
            }

            if (ModelState.IsValid == true)
            {
                if(_rcuClient.UpdateCredenciaisTemporarias(credenciaisTemporariasUpdate) == credenciaisTemporariasUpdate.Id)
                {
                    StreamingSession.Session.Nome = credenciaisTemporariasUpdate.Nome;

                    bool success = true;

                    if(credenciaisTemporariasUpdate.DefaultLanguage != StreamingSession.Session.SelectedLinguagem.Symbol)
                    {
                        success = LoadLoginLinguagem(credenciaisTemporariasUpdate.DefaultLanguage);
                    }

                    if(success == true)
                    {
                        TempData["success"] = "Os dados do seu perfil foram atualizados com sucesso!";

                        return RedirectToAction("Index", "Home");
                    }
                }

                TempData["error"] = "Ocorreu um erro ao submeter dados! Por favor tente de novo.";
            }
            else
            {
                TempData["error"] = "Dados não válidos! Por favor verifique todos os campos e tente de novo.";
            }

            return View(credenciaisTemporariasUpdate);
        }

        private bool LoadLoginLinguagem(string symbolLinguagem)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(symbolLinguagem))
                {
                    throw new ArgumentNullException(nameof(symbolLinguagem));
                }

                return LoadLoginLinguagem(new List<string>() { symbolLinguagem });
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private bool LoadLoginLinguagem(IEnumerable<string> symbolsLinguagem)
        {

            bool success = false;

            try
            {
                success = Apresentacao.LoadLinguagem(symbolsLinguagem);
            }
            catch (Exception e)
            {
                success = Apresentacao.LoadLinguagem("pt-PT");
            }

            if (success == true)
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(StreamingSession.Session.SelectedLinguagem.Symbol);
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(StreamingSession.Session.SelectedLinguagem.Symbol);

                success = Apresentacao.LoadApresentacaoData();
            }

            return success;
        }

        public ActionResult Logout()
        {
            if(Autenticacao.IsLoggedIn() == true)
            {
                StreamingSession.ClearSession();
            }

            return Redirect(Url.Content("~/"));
        }        
    }
}