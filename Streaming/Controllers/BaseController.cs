using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using System.Threading;
using System.Web.Routing;
using System.Globalization;

using Streaming.App_Start;
using Streaming.Clients;
using Streaming.ViewModels;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models.QuoVadis.Pessoas;
using Streaming.Helpers;

namespace Streaming.Controllers
{
    public class BaseController : Controller
    {
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        private readonly IUTADClient _iUTADClient = new IUTADClient();
        private readonly MensagensClient _mensagensClient = new MensagensClient();

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            // Se os idiomas suportados ainda não foram obtidos, obtém esses idiomas do QuoVadis
            if (StreamingSession.Session.Linguagens.Count == 0)
            {
                bool success = Apresentacao.LoadSupportedLinguagens();

                if (success == true)
                {
                    try
                    {
                        var symbols = HttpHelper.GetLanguageSymbols(Request, "pt-PT");
                        
                        success = Apresentacao.LoadLinguagem(symbols);
                    }
                    catch (Exception e)
                    {
                        //TODO: log this
                    }

                    if (success == true)
                    {
                        Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(StreamingSession.Session.SelectedLinguagem.Symbol);
                        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(StreamingSession.Session.SelectedLinguagem.Symbol);

                        success = Apresentacao.LoadApresentacaoData();
                    }
                }

                if (success == false)
                {
                    TempData["error"] = "Ocorreu um erro ao carregar informação!";
                }
            }
        }

        // Esta é a função principal que é executada sempre que uma página é carregada no Streaming
        // Obtém os elementos HTML autorizados para apresentar na página, bem como as mensagens mais recentes tanto locais como provenientes do QuoVadis
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            // Só executa o código apenas quando uma view totalmente nova é retornada
            // Sem isto, todo este código é efectuado para qualquer coisa retornada - views, partial views, redirects, etc.
            if (filterContext.Result.GetType() == typeof(ViewResult))
            {
                BaseViewModel layoutModel = new BaseViewModel();

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.Mensagens"]) == true)
                {
                    // Obtém todas as mensagens a apresentar
                    // Começa por obter as mensagens locais da aplicação, que se encontram armazenadas na base de dados do Streaming
                    int mensagensDashboardVisibilidade = 0;

                    if (Autenticacao.IsLoggedIn() == true)
                    {
                        mensagensDashboardVisibilidade = 2;
                    }
                    else
                    {
                        mensagensDashboardVisibilidade = 3;
                    }

                    layoutModel.MensagensList.AddRange(_streamingService.NSI_STP_Mensagens_S_ByVisibilidade(mensagensDashboardVisibilidade));

                    // Depois obtém as mensagens globais, que são definidas a partir do QuoVadis
                    string perfisPublicacao = "''";
                    string tiposCargosPublicacao = "''";
                    string orgaosPublicacao = "''";
                    string usernamePublicacao = "''";

                    if (Autenticacao.IsLoggedIn() == true)
                    {
                        perfisPublicacao = string.Join(";", StreamingSession.Session.Perfis.Where(sp => sp.Ativo == true).Select(p => p.Tipo).Distinct());
                        tiposCargosPublicacao = string.Join(";", StreamingSession.Session.Cargos.Select(p => p.CodigoTipoCargo).Distinct());
                        orgaosPublicacao = string.Join(";", StreamingSession.Session.Cargos.Select(p => p.CodigoOrgao).Distinct());
                        usernamePublicacao = StreamingSession.Session.Username;

                        if (perfisPublicacao == "")
                        {
                            perfisPublicacao = "''";
                        }

                        if (tiposCargosPublicacao == "")
                        {
                            tiposCargosPublicacao = "''";
                        }

                        if (orgaosPublicacao == "")
                        {
                            orgaosPublicacao = "''";
                        }
                    }

                    layoutModel.MensagensList.AddRange(_mensagensClient.GetMensagensByVisibilidades(perfisPublicacao, tiposCargosPublicacao, orgaosPublicacao, usernamePublicacao));
                }

                if (Autenticacao.IsLoggedIn() == true)
                {
                    // Carrega os elementos HTML autorizados para o utilizador autenticado
                    // Primeiro verifica se o mecanismo de autorizações está ativado para o Streaming
                    layoutModel.IsAutorizacoesEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]);

                    if (layoutModel.IsAutorizacoesEnabled == true)
                    {
                        // Da lista de elementos HTML autorizados guardada previamente na sessão do browser, obtém e guarda aqueles que correspondem à página a ser carregada
                        string autorizacoesCurrentURL = RouteData.Values["controller"].ToString() + "/" + RouteData.Values["action"].ToString();

                        layoutModel.AutorizacoesList.AddRange(StreamingSession.Session.Autorizacoes.Where(a => a.Pagina == autorizacoesCurrentURL).ToList());
                    }
                }

                filterContext.Controller.ViewData["LayoutModel"] = layoutModel;
            }
        }

        public ActionResult ChangeLinguagem(string symbol)
        {
            bool success = Apresentacao.LoadLinguagem(symbol);

            if (success == true)
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(StreamingSession.Session.SelectedLinguagem.Symbol);
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(StreamingSession.Session.SelectedLinguagem.Symbol);

                success = Apresentacao.LoadApresentacaoData();
            }

            if (success == false)
            {
                TempData["error"] = "Ocorreu um erro ao carregar informação!";
            }

            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }

        public ActionResult LoadProfilePartial()
        {
            return PartialView("_ProfilePartial");
        }

        public ActionResult SearchPessoas(string searchQuery)
        {
            List<Pessoa> pessoasList = _iUTADClient.SearchPessoas(2, searchQuery);

            return PartialView("_MenuLateralSearchPessoasPartial", pessoasList);
        }
    }
}