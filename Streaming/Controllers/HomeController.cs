using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Streaming.Filters;

namespace Streaming.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [StreamingLoggedIn]
        [StreamingAutorizacoes(GruposNomes = new string[] { "DOCENTES", "NÃO DOCENTE" })]
        public ActionResult TestAutorizacoes()
        {
            // Carrega uma página de teste do mecanismo de autorizações
            // Para visualizar como é que o Streaming esconde ou apresenta elementos visuais consoante as autorizações do utilizador, ver o código desta view
            // Este código e a view correspondente podem ser eliminados sem problemas
            return View();
        }
    }
}