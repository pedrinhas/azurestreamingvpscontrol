﻿namespace Streaming.Controllers
{
    using Microsoft.Rest.Azure;
    using Streaming.App_Start;
    using Streaming.Filters;
    using Streaming.Helpers;
    using Streaming.ViewModels;
    using StreamingAssemblies;
    using StreamingAssemblies.DataAccess;
    using StreamingAssemblies.Helpers.Azure;
    using StreamingAssemblies.Helpers.Azure.FileManagement;
    using StreamingAssemblies.Helpers.Misc;
    using StreamingAssemblies.Models.Azure;
    using StreamingAssemblies.Models.Azure.Logs;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Defines the <see cref="AzureController" />.
    /// </summary>
    [StreamingLoggedIn]
    [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_USER, StreamingAutorizacoesAttribute.VM_ADMIN })]
    public class AzureController : Controller
    {
        /// <summary>
        /// Defines the _defaultReturnTo.
        /// </summary>
        private string _defaultReturnTo = "Index";

        /// <summary>
        /// Defines the _azureService.
        /// </summary>
        private readonly AzureDb _azureService = new AzureDb(ConfigurationHelper.ConnectionStrings.StreamingDb);

        /// <summary>
        /// Defines the _streamingService.
        /// </summary>
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationHelper.ConnectionStrings.StreamingDb);

        /// <summary>
        /// Defines the _logService.
        /// </summary>
        private readonly LogDb _logService = new LogDb(ConfigurationHelper.ConnectionStrings.StreamingDb);

        /// <summary>
        /// Gets the _authenticatedUserIupi.
        /// </summary>
        private string _authenticatedUserIupi => StreamingSession.Session.IUPI.ToString();

        /// <summary>
        /// The CreateVm.
        /// </summary>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public ActionResult CreateVm()
        {
            return View();
        }

        /// <summary>
        /// The CreateVm.
        /// </summary>
        /// <param name="model">The model<see cref="AzureInsertVmModel"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [HttpPost]
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> CreateVm(AzureInsertVmModel model)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(model, nameof(model));
                if (!model.IsValid)
                {
                    throw new ArgumentException("Model state is invalid", nameof(model));
                }

                //config file
                var configFileResult = await AzureFileManager.SaveAzureFileAsync(model.GetSubscriptionIdFromUrl(), model.TenantId, model.ApplicationId, model.ApplicationSecret);

                if (!configFileResult.Success)
                {
                    throw configFileResult.Exception;
                }

                var clientResult = AzureClientStore.AddAzureClient(configFileResult.Value);

                if (!clientResult.Success)
                {
                    throw clientResult.Exception;
                }

                var vmId = model.GetVmIdFromUrl();

                //try to get the VM to test the client
                var azureVmResult = await clientResult.Value.GetVirtualMachineAsync(vmId);

                if (!azureVmResult.Success)
                {
                    AzureFileManager.DeleteAzureFile(configFileResult.Value);
                    throw azureVmResult.Exception;
                }

                var newVm = new AzureVirtualMachine()
                {
                    ConfigFilePath = configFileResult.Value,
                    PluginUrl = model.PluginUrl,
                    VmId = vmId,
                    VmName = azureVmResult.Value.Name,
                    Alias = model.Alias
                };

                var insertedResult = await _azureService.EditVirtualMachineAsync(newVm, _authenticatedUserIupi);

                if (insertedResult.Success)
                {
                    SetSuccessMessage($"A máquina virtual {newVm.DisplayName} foi adicionada com sucesso.");
                }
                else
                {
                    throw insertedResult.Exception;
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao adicionar a máquina virtual. Reveja os dados inseridos, permissões no Azure ou tente mais tarde.");
            }

            return ReturnToReferral(Request);
        }

        /// <summary>
        /// The EditVm.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> EditVm(int id)
        {
            AzureUpdateVmModel model = null;

            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, 0);

                var vmResult = await _azureService.GetVirtualMachineAsync(id);

                if (!vmResult.Success)
                {
                    if (vmResult.Exception != null)
                    {
                        throw vmResult.Exception;
                    }
                    else
                    {
                        throw new ArgumentException($"VM with id {id} not found.");
                    }
                }

                model = await AzureUpdateVmModel.FromAzureVirtualMachineAsync(vmResult.Value);
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao editar a máquina virtual.");

                return ReturnToReferral(Request, "Manage");
            }

            return View(model);
        }

        /// <summary>
        /// The EditVm.
        /// </summary>
        /// <param name="model">The model<see cref="AzureInsertVmModel"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [HttpPost]
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> EditVm(AzureUpdateVmModel model)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(model, nameof(model));
                if (!model.IsValid)
                {
                    throw new ArgumentException("Model state is invalid", nameof(model));
                }

                var vm = await _azureService.GetVirtualMachineAsync(model.Id);

                if (!vm.Success)
                {
                    throw vm.Exception;
                }

                //config file
                var configFileResult = await AzureFileManager.SaveAzureFileAsync(model.GetSubscriptionIdFromUrl(), model.TenantId, model.ApplicationId, model.ApplicationSecret);

                if (!configFileResult.Success)
                {
                    throw configFileResult.Exception;
                }

                var clientResult = AzureClientStore.AddAzureClient(configFileResult.Value);

                if (!clientResult.Success)
                {
                    AzureFileManager.DeleteAzureFile(configFileResult.Value);
                    throw clientResult.Exception;
                }

                var vmId = model.GetVmIdFromUrl();

                //try to get the VM to test the client
                var azureVmResult = await clientResult.Value.GetVirtualMachineAsync(vmId);

                if (!azureVmResult.Success)
                {
                    AzureFileManager.DeleteAzureFile(configFileResult.Value);
                    throw azureVmResult.Exception;
                }

                var newVm = new AzureVirtualMachine()
                {
                    Id = model.Id,
                    ConfigFilePath = configFileResult.Value,
                    VmAzureUrl = model.AzureVmUrl,
                    PluginUrl = model.PluginUrl,
                    VmId = vmId,
                    VmName = azureVmResult.Value.Name,
                    Alias = model.Alias
                };

                var editedResult = await _azureService.EditVirtualMachineAsync(newVm, _authenticatedUserIupi);

                if (editedResult.Success)
                {
                    SetSuccessMessage($"A máquina virtual {newVm.DisplayName} foi editada com sucesso.");
                }
                else
                {
                    throw editedResult.Exception;
                }

                //delete old config file
                AzureFileManager.DeleteAzureFile(vm.Value.ConfigFilePath);
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao editar a máquina virtual. Reveja os dados inseridos, permissões no Azure ou tente mais tarde.");
            }

            return ReturnToReferral(Request);
        }

        /// <summary>
        /// The Stop.
        /// </summary>
        /// <param name="id">The id<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [HttpPost]
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, nameof(id), 0);

                var deleteRes = await _azureService.DeleteVirtualMachineAsync(id, _authenticatedUserIupi);

                if (deleteRes.Success)
                {
                    SetSuccessMessage("Máquina virtual eliminada com sucesso");
                }
                else
                {
                    throw deleteRes.Exception;
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao eliminar a máquina virtual");
            }

            return ReturnToReferral(Request);
        }

        /// <summary>
        /// The Index.
        /// </summary>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        public async Task<ActionResult> Index()
        {
            var model = new List<AzureVirtualMachine>();

            try
            {
                var vms = await _azureService.GetAllActiveVirtualMachinesAsync();

                if (!vms.Success)
                {
                    throw vms.Exception;
                }

                foreach (var vm in vms.Value.OrderBy(x => x.DisplayName).ThenBy(x => x.Id))
                {
                    var azureClientResult = AzureClientStore.GetClient(vm.ConfigFilePath);

                    if (!azureClientResult.Success)
                    {
                        continue;
                    }

                    var azureVmResult = await azureClientResult.Value.GetVirtualMachineAsync(vm.VmId);

                    if (!azureVmResult.Success)
                    {
                        continue;
                    }

                    vm.PowerState = azureVmResult.Value.PowerState;

                    model.Add(vm);
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao carregar a lista de máquinas virtuais");
            }

            return View(model);
        }

        /// <summary>
        /// The Manage.
        /// </summary>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> Manage()
        {
            var model = new List<AzureVirtualMachine>();

            try
            {
                var vms = await _azureService.GetAllVirtualMachinesAsync();

                if (!vms.Success)
                {
                    throw vms.Exception;
                }

                foreach (var vm in vms.Value.OrderBy(x => x.DisplayName).ThenBy(x => x.Id))
                {
                    var azureClientResult = AzureClientStore.GetClient(vm.ConfigFilePath);

                    if (!azureClientResult.Success)
                    {
                        continue;
                    }

                    var azureVmResult = await azureClientResult.Value.GetVirtualMachineAsync(vm.VmId);

                    if (!azureVmResult.Success)
                    {
                        continue;
                    }

                    vm.PowerState = azureVmResult.Value.PowerState;

                    model.Add(vm);
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao carregar a lista de máquinas virtuais");
            }

            return View(model);
        }

        /// <summary>
        /// The LoadDeleteVmPartial.
        /// </summary>
        /// <param name="id">The id<see cref="string"/>.</param>
        /// <param name="name">The name<see cref="string"/>.</param>
        /// <param name="returnTo">The returnTo<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> LoadDeleteVmPartial(int id, string name, string returnTo)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, nameof(id), 0);

                return PartialView("DeleteVmPartial", new AzureStopDeleteVmModel() { Id = id, VmName = name });
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
            }

            return RedirectToAction(_defaultReturnTo);
        }

        /// <summary>
        /// The LoadStopVmPartial.
        /// </summary>
        /// <param name="id">The id<see cref="string"/>.</param>
        /// <param name="name">The name<see cref="string"/>.</param>
        /// <param name="returnTo">The returnTo<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        public async Task<ActionResult> LoadStopVmPartial(int id, string name, string returnTo)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, nameof(id), 0);

                return PartialView("StopVmPartial", new AzureStopDeleteVmModel() { Id = id, VmName = name });
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
            }

            return RedirectToAction(_defaultReturnTo);
        }

        /// <summary>
        /// The Start.
        /// </summary>
        /// <param name="id">The id<see cref="string"/>.</param>
        /// <param name="returnTo">The returnTo<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        public async Task<ActionResult> Start(int id, string returnTo)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, nameof(id), 0);

                var vm = await _azureService.GetVirtualMachineAsync(id);

                if (!vm.Success)
                {
                    throw new ArgumentException($"Invalid value for parameter {nameof(id)}: {id}");
                }

                if (!vm.Value.IsActive || vm.Value.IsDeleted)
                {
                    throw new ArgumentException($"Invalid state for VM with id {id}");
                }

                await _logService.AzureLog.Log_WithVM_I(StreamingSession.Session.IdUser, $"User {StreamingSession.Session.Username} is trying to start virtual machine {vm.Value.VmName}", vm.Value.Id.Value, AzureLogAction.TriedToStart);

                var clientResult = AzureClientStore.GetClient(vm.Value.ConfigFilePath);

                if (!clientResult.Success)
                {
                    throw clientResult.Exception;
                }

                var multiThreadParameter = _streamingService.NSI_STP_Parametros_S_ByChave(AzureParameters.MultiThread);

                StreamingResult res = null;

                if (multiThreadParameter != null && Convert.ToBoolean(multiThreadParameter.Valor))
                {
                    res = await clientResult.Value.StartVirtualMachineMultiThreadAsync(vm.Value.VmId);
                }
                else
                {
                    res = await clientResult.Value.StartVirtualMachineAsync(vm.Value.VmId);
                }

                if (res.Success)
                {
                    SetSuccessMessage("Máquina virtual iniciada com sucesso");

                    await _logService.AzureLog.Log_WithVM_I(StreamingSession.Session.IdUser, $"User {StreamingSession.Session.Username} started virtual machine {vm.Value.VmName}", vm.Value.Id.Value, AzureLogAction.Started);
                }
                else
                {
                    throw res.Exception;
                }
            }
            catch (CloudException ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);

                if (ex.Body.Code == AzureClient.AzureErrorCodes.ReadOnlyDisabledSubscription)
                {
                    SetErrorMessage("Ocorreu um erro ao iniciar a máquina virtual. A subscrição selecionada pode ter chegado ao limite de crédito.");
                }
                else
                {
                    SetErrorMessage("Ocorreu um erro ao iniciar a máquina virtual");
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                await _logService.AzureLog.Log_I(StreamingSession.Session.IdUser, $"Error for user {StreamingSession.Session.Username} trying to start virtual machine with id {id}");
                SetErrorMessage("Ocorreu um erro ao iniciar a máquina virtual");
            }

            return ReturnToReferral(Request);
        }

        /// <summary>
        /// The SetInactive.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> SetInactive(int id)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, nameof(id), 0);

                var res = await _azureService.SetVirtualMachineAsInactiveAsync(id, _authenticatedUserIupi);

                if (res.Success)
                {
                    SetSuccessMessage("Máquina virtual desativada com sucesso");
                }
                else
                {
                    throw res.Exception;
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao desativar a máquina virtual");
            }

            return RedirectToAction("Manage");
        }

        /// <summary>
        /// The SetActive.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> SetActive(int id)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, nameof(id), 0);

                var res = await _azureService.SetVirtualMachineAsActiveAsync(id, _authenticatedUserIupi);

                if (res.Success)
                {
                    SetSuccessMessage("Máquina virtual ativada com sucesso");
                }
                else
                {
                    throw res.Exception;
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao ativar a máquina virtual");
            }

            return RedirectToAction("Manage");
        }

        /// <summary>
        /// The Stop.
        /// </summary>
        /// <param name="id">The id<see cref="string"/>.</param>
        /// <param name="returnTo">The returnTo<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [HttpPost]
        public async Task<ActionResult> Stop(int id, string returnTo)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(id, nameof(id), 0);

                var vm = await _azureService.GetVirtualMachineAsync(id);

                if (!vm.Success)
                {
                    throw new ArgumentException($"Invalid value for parameter {nameof(id)}: {id}");
                }

                if (!vm.Value.IsActive || vm.Value.IsDeleted)
                {
                    throw new ArgumentException($"Invalid state for VM with id {id}");
                }

                await _logService.AzureLog.Log_WithVM_I(StreamingSession.Session.IdUser, $"User {StreamingSession.Session.Username} is trying to stop virtual machine {vm.Value.VmName}", vm.Value.Id.Value, AzureLogAction.TriedToStop);

                var clientResult = AzureClientStore.GetClient(vm.Value.ConfigFilePath);

                if (!clientResult.Success)
                {
                    throw clientResult.Exception;
                }

                var multiThreadParameter = _streamingService.NSI_STP_Parametros_S_ByChave(AzureParameters.MultiThread);
                var alwaysDeallocateParameter = _streamingService.NSI_STP_Parametros_S_ByChave(AzureParameters.AlwaysDeallocate);

                StreamingResult res = null;

                if (alwaysDeallocateParameter != null && Convert.ToBoolean(alwaysDeallocateParameter.Valor))
                {
                    if (multiThreadParameter != null && Convert.ToBoolean(multiThreadParameter.Valor))
                    {
                        res = await clientResult.Value.DeallocateVirtualMachineMultiThreadAsync(vm.Value.VmId);
                    }
                    else
                    {
                        res = await clientResult.Value.DeallocateVirtualMachineAsync(vm.Value.VmId);
                    }
                }
                else
                {
                    if (multiThreadParameter != null && Convert.ToBoolean(multiThreadParameter.Valor))
                    {
                        res = await clientResult.Value.StopVirtualMachineMultiThreadAsync(vm.Value.VmId);
                    }
                    else
                    {
                        res = await clientResult.Value.StopVirtualMachineAsync(vm.Value.VmId);
                    }
                }

                if (res.Success)
                {
                    SetSuccessMessage("Máquina virtual parada com sucesso");

                    await _logService.AzureLog.Log_WithVM_I(StreamingSession.Session.IdUser, $"User {StreamingSession.Session.Username} stopped virtual machine {vm.Value.VmName}", vm.Value.Id.Value, AzureLogAction.Stopped);
                }
                else
                {
                    throw res.Exception;
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                await _logService.AzureLog.Log_I(StreamingSession.Session.IdUser, $"Error for user {StreamingSession.Session.Username} trying to stop virtual machine with id {id}");
                SetErrorMessage("Ocorreu um erro ao parar a máquina virtual");
            }

            return ReturnToReferral(Request);
        }

        /// <summary>
        /// The VmLogs.
        /// </summary>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [StreamingAutorizacoes(GruposNomes = new string[] { StreamingAutorizacoesAttribute.VM_ADMIN })]
        public async Task<ActionResult> VmLogs()
        {
            var model = new List<AzureLogViewModel>();

            try
            {
                var allLogs = await _logService.AzureLog.Log_LS();

                if (allLogs != null && allLogs.Success)
                {
                    model.AddRange(AzureLogViewModel.FromAzureLog(allLogs.Value));

                    model.OrderByDescending(x => x.DateTime);
                }
            }
            catch (Exception ex)
            {
                await _logService.Log(ex, _authenticatedUserIupi);
                SetErrorMessage("Ocorreu um erro ao carregar a lista de logs");
            }

            return View(model);
        }

        /// <summary>
        /// The AddErrorMessage.
        /// </summary>
        /// <param name="message">The message<see cref="string"/>.</param>
        private void SetErrorMessage(string message)
        {
            TempData["error"] = message;
        }

        /// <summary>
        /// The AddSuccessMessage.
        /// </summary>
        /// <param name="message">The message<see cref="string"/>.</param>
        private void SetSuccessMessage(string message)
        {
            TempData["success"] = message;
        }

        /// <summary>
        /// The ReturnToReferral.
        /// </summary>
        /// <param name="request">The request<see cref="HttpRequestBase"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        private ActionResult ReturnToReferral(HttpRequestBase request) => ReturnToReferral(request, _defaultReturnTo);

        /// <summary>
        /// The ReturnToReferral.
        /// </summary>
        /// <param name="request">The request<see cref="HttpRequestBase"/>.</param>
        /// <param name="defaultReturn">The defaultReturn<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        private ActionResult ReturnToReferral(HttpRequestBase request, string defaultReturn)
        {
            var referral = HttpHelper.GetRouteValuesFromUrl(Request.UrlReferrer.ToString());

            if (referral != null && !string.IsNullOrWhiteSpace(referral.Controller) && !string.IsNullOrWhiteSpace(referral.Action))
            {
                return RedirectToAction(referral.Action, referral.Controller);
            }
            else
            {
                return RedirectToAction(defaultReturn);
            }
        }
    }
}
