﻿namespace Streaming.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    /// <summary>
    /// Defines the <see cref="ConfigurationHelper" />.
    /// </summary>
    public static class ConfigurationHelper
    {
        /// <summary>
        /// Defines the <see cref="ConnectionStrings" />.
        /// </summary>
        public static class ConnectionStrings
        {
            /// <summary>
            /// Gets the StreamingDb.
            /// </summary>
            public static string StreamingDb => _getValue("streamingDBString");

            /// <summary>
            /// The _getValue.
            /// </summary>
            /// <param name="connectionStringKey">The connectionStringKey<see cref="string"/>.</param>
            /// <returns>The <see cref="string"/>.</returns>
            private static string _getValue(string connectionStringKey)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(connectionStringKey))
                    {
                        throw new ArgumentNullException(nameof(connectionStringKey));
                    }

                    if (ConfigurationManager.ConnectionStrings[connectionStringKey] == null)
                    {
                        throw new KeyNotFoundException();
                    }

                    return ConfigurationManager.ConnectionStrings[connectionStringKey].ConnectionString;
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return default;
                }
            }
        }

        /// <summary>
        /// Defines the <see cref="AppSettings" />.
        /// </summary>
        public static class AppSettings
        {
            /// <summary>
            /// Defines the <see cref="Azure" />.
            /// </summary>
            public static class Azure
            {
                /// <summary>
                /// Gets the ClientId.
                /// </summary>
                public static string ClientId => _getValue("Azure.ClientId");
                
                /// <summary>
                /// Gets the ConfigFileFolder.
                /// </summary>
                public static string ConfigFileFolder => _getValue("Azure.ConfigFileFolder");

                /// <summary>
                /// Gets the ConfigFileFolderCasa.
                /// </summary>
                public static string ConfigFileFolderCasa => _getValue("Azure.ConfigFileFolderCasa");
            }

            /// <summary>
            /// The _getValue.
            /// </summary>
            /// <typeparam name="T">.</typeparam>
            /// <param name="configKey">The configKey<see cref="string"/>.</param>
            /// <returns>The <see cref="T"/>.</returns>
            private static T _getValue<T>(string configKey)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(configKey))
                    {
                        throw new ArgumentNullException(nameof(configKey));
                    }

                    if (!ConfigurationManager.AppSettings.AllKeys.Contains(configKey))
                    {
                        throw new KeyNotFoundException();
                    }

                    var valueAsString = ConfigurationManager.AppSettings[configKey];
                    var converted = Convert.ChangeType(valueAsString, typeof(T));

                    return (T)converted;
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return default;
                }
            }

            /// <summary>
            /// The _getValue.
            /// </summary>
            /// <param name="configKey">The configKey<see cref="string"/>.</param>
            /// <returns>The <see cref="string"/>.</returns>
            private static string _getValue(string configKey) => _getValue<string>(configKey);
        }
    }
}
