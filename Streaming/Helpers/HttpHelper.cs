﻿namespace Streaming.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Routing;

    /// <summary>
    /// Defines the <see cref="HttpHelper" />
    /// </summary>
    public static class HttpHelper
    {
        /// <summary>
        /// Defines the acceptLanguageServerVarName
        /// </summary>
        private const string acceptLanguageServerVarName = "HTTP_ACCEPT_LANGUAGE";

        /// <summary>
        /// Defines the acceptLanguageHeaderName
        /// </summary>
        private const string acceptLanguageHeaderName = "Accept-Language";

        /// <summary>
        /// The GetLanguageSymbols
        /// </summary>
        /// <param name="req">The req<see cref="HttpRequestBase"/></param>
        /// <param name="fallbackLanguages">The fallbackLanguages<see cref="string[]"/></param>
        /// <returns>The <see cref="IEnumerable{string}"/></returns>
        public static IEnumerable<string> GetLanguageSymbols(HttpRequestBase req, params string[] fallbackLanguages)
        {
            try
            {
                if (req == null)
                {
                    throw new ArgumentNullException(nameof(req));
                }

                if (req.ServerVariables.AllKeys.Any() && req.ServerVariables.AllKeys.Contains(acceptLanguageServerVarName))
                {
                    return GetLanguageSymbols(req.ServerVariables[acceptLanguageServerVarName], fallbackLanguages);
                }
                else
                {
                    if (req.Headers.AllKeys.Any() && req.Headers.AllKeys.Contains(acceptLanguageHeaderName))
                    {
                        return GetLanguageSymbols(req.Headers[acceptLanguageHeaderName], fallbackLanguages);
                    }
                    else
                    {
                        return fallbackLanguages;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: log this
                return fallbackLanguages;
            }
        }

        /// <summary>
        /// The GetLanguageSymbols
        /// </summary>
        /// <param name="langHeader">The langHeader<see cref="string"/></param>
        /// <param name="fallbackLanguages">The fallbackLanguages<see cref="string[]"/></param>
        /// <returns>The <see cref="IEnumerable{string}"/></returns>
        public static IEnumerable<string> GetLanguageSymbols(string langHeader, params string[] fallbackLanguages)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(langHeader))
                {
                    throw new ArgumentNullException(nameof(langHeader));
                }

                var regex = new Regex(".*;");
                var symbolsMatch = regex.Match(langHeader);
                if (symbolsMatch.Success)
                {
                    var symbolsString = symbolsMatch.Value.Trim(new char[] { ';' });
                    var symbols = symbolsString.Split(',');

                    if (fallbackLanguages != null)
                    {
                        return symbols.Concat(fallbackLanguages.Where(x => !symbols.Select(y => y.ToLowerInvariant()).Contains(x.ToLowerInvariant())));
                    }
                    else
                    {
                        return symbols;
                    }
                }
                else
                {
                    return fallbackLanguages;
                }
            }
            catch (Exception ex)
            {
                //TODO: log this
                return fallbackLanguages;
            }
        }

        public static HttpRouteValues GetRouteValuesFromUrl(string fullUrl)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(fullUrl))
                {
                    throw new ArgumentNullException(fullUrl, nameof(fullUrl));
                }

                var questionMarkIndex = fullUrl.IndexOf('?');
                string queryString = null;
                string url = fullUrl;
                if (questionMarkIndex != -1) // There is a QueryString
                {
                    url = fullUrl.Substring(0, questionMarkIndex);
                    queryString = fullUrl.Substring(questionMarkIndex + 1);
                }

                // Arranges
                var request = new HttpRequest(null, url, queryString);
                var response = new HttpResponse(new StringWriter());
                var httpContext = new HttpContext(request, response);

                var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

                // Extract the data    
                var values = routeData.Values;
                var controllerName = values["controller"];
                var actionName = values["action"];
                var areaName = values["area"];

                return new HttpRouteValues()
                {
                    Controller = controllerName?.ToString(),
                    Action = actionName?.ToString(),
                    Area = areaName?.ToString()
                };
            }
            catch
            {
                return null;
            }
        }

        public class HttpRouteValues
        {
            public string Controller { get; set; }
            public string Action { get; set; }
            public string Area { get; set; }
        }
    }
}
