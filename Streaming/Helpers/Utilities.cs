using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Streaming.Helpers
{
    public static class Utilities
    {
        public static string EncodeBase64String(byte[] plainStringBytes)
        {
            try
            {
                string base64String = Convert.ToBase64String(plainStringBytes);

                return base64String;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public static string DecodeBase64String(string base64String)
        {
            try
            {
                byte[] plainStringBytes = Convert.FromBase64String(base64String);

                string plainString = Encoding.UTF8.GetString(plainStringBytes);

                return plainString;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public static byte[] DecodeBase64StringToByteArray(string base64String)
        {
            try
            {
                byte[] plainStringBytes = Convert.FromBase64String(base64String);

                return plainStringBytes;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static bool CheckEmailString(string emailString)
        {
            try
            {
                MailAddress emailAddress = new MailAddress(emailString);

                return emailAddress.Address == emailString;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public static string GetNumMecFromUsername(string username)
        {
            if (username.ToLower().StartsWith("al") == true)
            {
                string numMec = username.Substring(2, username.Length - 2);

                if (int.TryParse(numMec, out int numMecParseResult) == true)
                {
                    return numMec;
                }
            }

            return "";
        }
    }
}