using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Configuration;

using Streaming.App_Start;

using StreamingAssemblies.Models.Streaming;

namespace Streaming.Filters
{
    public class StreamingAutorizacoesAttribute : AuthorizeAttribute
    {
        #region Azure groups

        public const string VM_ADMIN = nameof(VM_ADMIN);
        public const string VM_USER = nameof(VM_USER);

        #endregion

        public string[] GruposNomes { get; set; }

        public StreamingAutorizacoesAttribute()
        {
            GruposNomes = null;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if(Autenticacao.IsAdmin() == true)
            {
                return true;
            }

            if (GruposNomes != null && GruposNomes.Count() >= 0)
            {
                foreach (string grupoNome in GruposNomes)
                {
                    if (StreamingSession.Session.Grupos.SingleOrDefault(g => g.Nome == grupoNome) != default(Grupo))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["error"] = "Tentativa de acesso inválida! Não tem permissões para aceder a esta funcionalidade.";

            filterContext.Result = new RedirectResult("~/Home/Index");
        }
    }
}