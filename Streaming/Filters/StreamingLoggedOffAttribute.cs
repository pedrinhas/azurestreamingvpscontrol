using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

using Streaming.App_Start;

namespace Streaming.Filters
{
    public class StreamingLoggedOffAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return !Autenticacao.IsLoggedIn();
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["error"] = "Já se encontra com sessão iniciada!";

            filterContext.Result = new RedirectResult("~/Home/Index");
        }
    }
}