using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using Streaming.Helpers;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Cargos;

namespace Streaming.Clients
{
    public class CargosClient
    {
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        public List<Cargo> GetPessoaCargos(string username)
        {
            List<Cargo> cargosList = new List<Cargo>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetPessoaCargos");

                if(string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format("{0}{1}", urlWS.Valor, username);

                        cargosList = JsonConvert.DeserializeObject<List<Cargo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return cargosList;
        }

        public List<TipoCargo> GetTiposCargos()
        {
            List<TipoCargo> tiposCargosList = new List<TipoCargo>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Cargos.GetTiposCargos");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        tiposCargosList = JsonConvert.DeserializeObject<List<TipoCargo>>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tiposCargosList;
        }
    }
}