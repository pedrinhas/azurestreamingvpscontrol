using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;
using System.Text;

using Newtonsoft.Json;

using Streaming.Helpers;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.RCU;

namespace Streaming.Clients
{
    public class RCUClient
    {
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        public EstudanteNomeAndIUPI GetEstudanteNomeAndIUPI(string numero)
        {
            EstudanteNomeAndIUPI estudanteNomeAndIUPI = new EstudanteNomeAndIUPI();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetEstudanteNomeAndIUPI");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, numero);

                        estudanteNomeAndIUPI = JsonConvert.DeserializeObject<EstudanteNomeAndIUPI>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return estudanteNomeAndIUPI;
        }

        public List<EstudantePerfil> GetEstudantePerfis(Guid iupi)
        {
            List<EstudantePerfil> estudantePerfisList = new List<EstudantePerfil>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetEstudantePerfis");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, iupi);

                        estudantePerfisList = JsonConvert.DeserializeObject<List<EstudantePerfil>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return estudantePerfisList;
        }

        public EstudanteFoto GetEstudanteFoto(Guid iupi)
        {
            EstudanteFoto estudanteFoto = new EstudanteFoto();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetEstudanteFoto");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, iupi);

                        estudanteFoto = JsonConvert.DeserializeObject<EstudanteFoto>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return estudanteFoto;
        }

        public Guid? GetUtilizadorIUPI(string username)
        {
            Guid iupi = new Guid();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetUtilizadorIUPI");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, username);

                        iupi = JsonConvert.DeserializeObject<Guid>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return iupi;
        }

        public UtilizadorNomeAndIUPI GetUtilizadorNomeAndIUPI(string username)
        {
            UtilizadorNomeAndIUPI utilizadorNomeAndIUPI = new UtilizadorNomeAndIUPI();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetUtilizadorNomeAndIUPI");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, username);

                        utilizadorNomeAndIUPI = JsonConvert.DeserializeObject<UtilizadorNomeAndIUPI>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return utilizadorNomeAndIUPI;
        }

        public List<UtilizadorPerfil> GetUtilizador(Guid iupi)
        {
            List<UtilizadorPerfil> utilizador = new List<UtilizadorPerfil>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetUtilizador");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, iupi);

                        utilizador = JsonConvert.DeserializeObject<List<UtilizadorPerfil>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return utilizador;
        }

        public CredenciaisTemporarias GetCredenciaisTemporarias(int id)
        {
            CredenciaisTemporarias credenciaisTemporarias = new CredenciaisTemporarias();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetCredenciaisTemporarias");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        credenciaisTemporarias = JsonConvert.DeserializeObject<CredenciaisTemporarias>(wcWS.DownloadString(string.Format(urlWS.Valor, id)).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return credenciaisTemporarias;
        }

        public List<CredenciaisTemporarias> GetCredenciaisTemporariasPorEmail(string email)
        {
            List<CredenciaisTemporarias> credenciaisTemporariasList = new List<CredenciaisTemporarias>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetCredenciaisTemporariasPorEmail");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, email);

                        credenciaisTemporariasList = JsonConvert.DeserializeObject<List<CredenciaisTemporarias>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return credenciaisTemporariasList;
        }

        public CredenciaisTemporarias GetCredenciaisTemporariasPorUsername(string username)
        {
            CredenciaisTemporarias credenciaisTemporarias = new CredenciaisTemporarias();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetCredenciaisTemporariasPorUsername");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        credenciaisTemporarias = JsonConvert.DeserializeObject<CredenciaisTemporarias>(wcWS.DownloadString(string.Format(urlWS.Valor, username + "/")).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return credenciaisTemporarias;
        }

        public string CreateCredenciaisTemporarias(CredenciaisTemporariasInsert credenciaisTemporariasInsert)
        {
            string username = "";

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.CreateCredenciaisTemporarias");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        string bodyWS = JsonConvert.SerializeObject(credenciaisTemporariasInsert);

                        username = JsonConvert.DeserializeObject<string>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return username;
        }

        public int? UpdateCredenciaisTemporarias(CredenciaisTemporariasUpdate credenciaisTemporariasUpdate)
        {
            int returnValue = 0;

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.UpdateCredenciaisTemporarias");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        string bodyWS = JsonConvert.SerializeObject(credenciaisTemporariasUpdate);

                        returnValue = JsonConvert.DeserializeObject<int>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public int? UpdateCredenciaisTemporariasPassword(CredenciaisTemporariasPasswordUpdate credenciaisTemporariasPasswordUpdate)
        {
            int returnValue = 0;

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.UpdateCredenciaisTemporariasPassword");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        string bodyWS = JsonConvert.SerializeObject(credenciaisTemporariasPasswordUpdate);

                        returnValue = JsonConvert.DeserializeObject<int>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public CredenciaisTemporariasToken GetCredenciaisTemporariasToken(Guid token)
        {
            CredenciaisTemporariasToken credenciaisTemporariasToken = new CredenciaisTemporariasToken();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetCredenciaisTemporariasToken");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        credenciaisTemporariasToken = JsonConvert.DeserializeObject<CredenciaisTemporariasToken>(wcWS.DownloadString(string.Format(urlWS.Valor, token)).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return credenciaisTemporariasToken;
        }

        public Guid? CreateCredenciaisTemporariasToken(CredenciaisTemporariasTokenInsert credenciaisTemporariasTokenInsert)
        {
            Guid token = new Guid();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.CreateCredenciaisTemporariasToken");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));
                        wcWS.Encoding = Encoding.UTF8;
                        wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                        string bodyWS = JsonConvert.SerializeObject(credenciaisTemporariasTokenInsert);

                        token = JsonConvert.DeserializeObject<Guid>(wcWS.UploadString(urlWS.Valor, bodyWS));
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return token;
        }

        public int? UpdateCredenciaisTemporariasTokenEstadoUso(int id)
        {
            int returnValue = 0;

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.UpdateCredenciaisTemporariasTokenEstadoUso");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        returnValue = JsonConvert.DeserializeObject<int>(wcWS.DownloadString(string.Format(urlWS.Valor, id)).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return returnValue;
        }

        public List<TipoPerfil> GetTiposPerfil(bool incluirApagados = false)
        {
            List<TipoPerfil> tiposPerfilList = new List<TipoPerfil>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetTiposPerfil");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        tiposPerfilList = JsonConvert.DeserializeObject<List<TipoPerfil>>(wcWS.DownloadString(string.Format(urlWS.Valor, incluirApagados)).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return tiposPerfilList;
        }

        public List<GrupoMenu> GetGruposMenu()
        {
            List<GrupoMenu> gruposMenuList = new List<GrupoMenu>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.RCU.GetGruposMenu");

                if(string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        gruposMenuList = JsonConvert.DeserializeObject<List<GrupoMenu>>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }
                }
            }
            catch(Exception e)
            {
                return null;
            }

            return gruposMenuList;
        }
    }
}