using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using Streaming.Helpers;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Grupos;

namespace Streaming.Clients
{
    public class GruposClient
    {
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        public StreamingAssemblies.Models.QuoVadis.Grupos.Grupo GetGrupo(int id)
        {
            StreamingAssemblies.Models.QuoVadis.Grupos.Grupo grupo = new StreamingAssemblies.Models.QuoVadis.Grupos.Grupo();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGrupo");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, id);

                        grupo = JsonConvert.DeserializeObject<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return grupo;
        }

        public List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo> GetGruposByTipo(int grupoTipo)
        {
            List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo> gruposList = new List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGruposByTipo");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, grupoTipo);

                        gruposList = JsonConvert.DeserializeObject<List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return gruposList;
        }

        public List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo> GetGruposByUtilizador(Guid utilizadorIUPI)
        {
            List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo> gruposList = new List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGruposByUtilizador");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, utilizadorIUPI);

                        gruposList = JsonConvert.DeserializeObject<List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return gruposList;
        }

        public List<Utilizador> GetGrupoUtilizadores(string nome)
        {
            List<Utilizador> utilizadoresList = new List<Utilizador>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Grupos.GetGrupoUtilizadores");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, nome);

                        utilizadoresList = JsonConvert.DeserializeObject<List<Utilizador>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return utilizadoresList;
        }
    }
}