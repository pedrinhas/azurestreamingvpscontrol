using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using Streaming.Helpers;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Aplicacoes;

namespace Streaming.Clients
{
    public class AplicacoesClient
    {
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        public List<Linguagem> GetLinguagens()
        {
            List<Linguagem> linguagensList = new List<Linguagem>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Aplicacoes.GetLinguagens");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        linguagensList = JsonConvert.DeserializeObject<List<Linguagem>>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return linguagensList;
        }

        public List<AplicacaoPortalBase> GetAplicacoesPortalBase(string perfisStreaming, string tiposCargosStreaming, string orgaosStreaming, string pessoasStreaming, string symbolLinguagem)
        {
            List<AplicacaoPortalBase> aplicacoesDashboardList = new List<AplicacaoPortalBase>();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Aplicacoes.GetAplicacoesPortalBase");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, perfisStreaming, tiposCargosStreaming, orgaosStreaming, pessoasStreaming, symbolLinguagem);

                        aplicacoesDashboardList = JsonConvert.DeserializeObject<List<AplicacaoPortalBase>>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return aplicacoesDashboardList;
        }

        public AplicacaoPortalBase GetAplicacaoPortalBase(string identificadorStreaming, string symbolLinguagem)
        {
            AplicacaoPortalBase aplicacaoStreaming = new AplicacaoPortalBase();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.Aplicacoes.GetAplicacaoPortalBase");

                if(string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        string url = string.Format(urlWS.Valor, identificadorStreaming, symbolLinguagem);

                        aplicacaoStreaming = JsonConvert.DeserializeObject<AplicacaoPortalBase>(wcWS.DownloadString(url).ToUTF8());
                    }
                }
            }
            catch(Exception e)
            {
                return null;
            }

            return aplicacaoStreaming;
        }
    }
}