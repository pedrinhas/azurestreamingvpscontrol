using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Configuration;

using Newtonsoft.Json;

using Streaming.Helpers;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.PortalBase;

namespace Streaming.Clients
{
    public class PortalBaseClient
    {
        private readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        public Footer GetFooter()
        {
            Footer footer = new Footer();

            try
            {
                Parametro urlWS = _streamingService.NSI_STP_Parametros_S_ByChave("QuoVadis.PortalBase.GetFooter");

                if (string.IsNullOrEmpty(urlWS.Valor) == false)
                {
                    using (WebClient wcWS = new WebClient())
                    {
                        wcWS.Credentials = new NetworkCredential(urlWS.Valor1, Utilities.DecodeBase64String(urlWS.Valor2));

                        footer = JsonConvert.DeserializeObject<Footer>(wcWS.DownloadString(urlWS.Valor).ToUTF8());
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return footer;
        }
    }
}