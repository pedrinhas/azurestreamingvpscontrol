using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StreamingAssemblies.Models;

namespace Streaming.App_Start
{
    public class StreamingSession
    {
        public static Session Session
        {
            get
            {
                Session session = (Session)HttpContext.Current.Session["StreamingSession"];

                if (session == null)
                {
                    session = new Session();

                    HttpContext.Current.Session["StreamingSession"] = session;
                }

                return session;
            }
        }

        public static void ClearSession()
        {
            HttpContext.Current.Session.Clear();
        }
    }
}