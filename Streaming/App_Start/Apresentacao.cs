using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.Web.Mvc;

using Streaming.Clients;

using StreamingAssemblies.Models;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Aplicacoes;
using StreamingAssemblies.Models.QuoVadis.RCU;

namespace Streaming.App_Start
{
    public class Apresentacao
    {
        private static readonly AplicacoesClient _aplicacoesClient = new AplicacoesClient();
        private static readonly PortalBaseClient _portalBaseClient = new PortalBaseClient();
        private static readonly RCUClient _rcuClient = new RCUClient();

        public static bool LoadSupportedLinguagens()
        {
            bool success = false;

            try
            {
                string[] streamingSupportedLinguagens = ConfigurationManager.AppSettings["Aplicacao.SupportedLinguagens"].Split(';');

                StreamingSession.Session.Linguagens = _aplicacoesClient.GetLinguagens().Where(l => streamingSupportedLinguagens.Contains(l.Symbol) == true).ToList();

                success = true;
            }
            catch (Exception e)
            {
                StreamingSession.ClearSession();
            }

            return success;
        }

        public static bool LoadLinguagem(string symbol)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(symbol))
                {
                    throw new ArgumentNullException(nameof(symbol));
                }

                return LoadLinguagem(new List<string>() { symbol });
            }
            catch (Exception e)
            {
                StreamingSession.ClearSession();
            }

            return false;
        }

        public static bool LoadLinguagem(IEnumerable<string> symbols)
        {
            try
            {
                if (symbols == null || !symbols.Any())
                {
                    throw new ArgumentNullException(nameof(symbols));
                }

                var success = false;

                foreach (var symbol in symbols)
                {
                    StreamingSession.Session.SelectedLinguagem = StreamingSession.Session.Linguagens.SingleOrDefault(l => l.Symbol.ToLowerInvariant().StartsWith(symbol.ToLowerInvariant()));

                    success = StreamingSession.Session.SelectedLinguagem != null;

                    if (success)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: log this
                var fallbackLanguages = new List<string>() { "pt-PT" };

                if (symbols.SequenceEqual(fallbackLanguages))
                {
                    StreamingSession.ClearSession();
                }
                else
                {
                    return LoadLinguagem(fallbackLanguages);
                }
            }

            return false;
        }

        public static bool LoadApresentacaoData()
        {
            bool success = false;

            try
            {
                // Obtém os dados da aplicação do próprio Streaming
                StreamingSession.Session.AplicacaoPortalBase = _aplicacoesClient.GetAplicacaoPortalBase(ConfigurationManager.AppSettings["Aplicacao.IdentificadorPortalBase"], StreamingSession.Session.SelectedLinguagem.Symbol);

                // Só é necessário obter estes recursos (aplicações do Streaming, menu lateral, etc.) se o utilizador estiver autenticado
                if (Autenticacao.IsLoggedIn() == true)
                {
                    // Define os items e subitems do menu lateral do Streaming
                    StreamingSession.Session.MenuLateral = BuildMenuLateral();

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.GetAplicacoesFromQuoVadis"]) == true)
                    {
                        List<AplicacaoPortalBase> aplicacoesStreaming = GetAplicacoesPortalBase();

                        if (aplicacoesStreaming.Count > 0)
                        {
                            StreamingSession.Session.AplicacoesPortalBaseMenuLateral = BuildAplicacoesPortalBaseMenuLateral(aplicacoesStreaming);
                        }

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]) == true)
                        {
                            // Se sim, filtra os items e os subitems do menu lateral, para desta forma ficarem apenas aqueles que o utilizador autenticado pode ver
                            CheckMenuLateralAutorizacoes(StreamingSession.Session.MenuLateral);
                        }

                        SetMenuLateralItemIDs(StreamingSession.Session.MenuLateral.Concat(StreamingSession.Session.AplicacoesPortalBaseMenuLateral).ToList(), 0);
                    }
                }

                // Obtém o rodapé para apresentar no Streaming
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Aplicacao.GetFooterFromQuoVadis"]) == true)
                {
                    StreamingSession.Session.Footer = _portalBaseClient.GetFooter();
                }

                success = true;
            }
            catch (Exception e)
            {
                StreamingSession.ClearSession();
            }

            return success;
        }

        private static List<AplicacaoPortalBase> GetAplicacoesPortalBase()
        {
            string perfisStreaming = string.Join(";", StreamingSession.Session.Perfis.Where(sp => sp.Ativo == true).Select(p => p.Tipo).Distinct());
            string tiposCargosStreaming = string.Join(";", StreamingSession.Session.Cargos.Select(p => p.CodigoTipoCargo).Distinct());
            string orgaosStreaming = string.Join(";", StreamingSession.Session.Cargos.Select(p => p.CodigoOrgao).Distinct());

            if (perfisStreaming == "")
            {
                perfisStreaming = "''";
            }

            if (tiposCargosStreaming == "")
            {
                tiposCargosStreaming = "''";
            }

            if (orgaosStreaming == "")
            {
                orgaosStreaming = "''";
            }

            List<AplicacaoPortalBase> aplicacoesStreaming = _aplicacoesClient.GetAplicacoesPortalBase(perfisStreaming, tiposCargosStreaming, orgaosStreaming, StreamingSession.Session.Username, StreamingSession.Session.SelectedLinguagem.Symbol);

            return aplicacoesStreaming;
        }

        private static List<MenuItem> BuildMenuLateral()
        {
            UrlHelper menuLateralURLHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            // Guarda, para cada ícone usado nos itens do menu, o código HTML respectivo a esse ícone
            string angleDoubleRightIconHTML = @"<span class=""fa fa-angle-double-right""></span>";
            string cogsIconHTML = @"<span class=""fa fa-cogs""></span>";
            string serverIconHTML = @"<span class=""fa fa-server""></span>";

            List<MenuItem> menu = new List<MenuItem>();

            List<MenuItem> gruposSubMenuItemChildList = new List<MenuItem>();

            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposSubSubMenuItem", "Grupos", "Grupos", menuLateralURLHelper.Action("Grupos", "Admin"), angleDoubleRightIconHTML));
            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposCargosSubSubMenuItem", "Cargos", "Cargos", menuLateralURLHelper.Action("GruposCargos", "Admin"), angleDoubleRightIconHTML));
            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposPerfisSubSubMenuItem", "Perfis", "Perfis", menuLateralURLHelper.Action("GruposPerfis", "Admin"), angleDoubleRightIconHTML));
            gruposSubMenuItemChildList.Add(new MenuItem(2, "GruposQuoVadisSubSubMenuItem", "Grupos do QuoVadis", "Grupos do QuoVadis", menuLateralURLHelper.Action("GruposQuoVadis", "Admin"), angleDoubleRightIconHTML));

            List<MenuItem> adminMenuItemChildList = new List<MenuItem>();

            adminMenuItemChildList.Add(new MenuItem(1, "AutorizacoesSubMenuItem", "Gerir Autorizações", "Gerir Autorizações", menuLateralURLHelper.Action("Autorizacoes", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "GruposSubMenuItem", "Gerir Grupos", "Gerir Grupos", angleDoubleRightIconHTML, gruposSubMenuItemChildList, false));
            adminMenuItemChildList.Add(new MenuItem(1, "MensagensSubMenuItem", "Gerir Mensagens", "Gerir Mensagens", menuLateralURLHelper.Action("Mensagens", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "ParametrosSubMenuItem", "Gerir Parâmetros", "Gerir Parâmetros", menuLateralURLHelper.Action("Parametros", "Admin"), angleDoubleRightIconHTML));
            adminMenuItemChildList.Add(new MenuItem(1, "UsersSubMenuItem", "Gerir Utilizadores", "Gerir Utilizadores", menuLateralURLHelper.Action("Users", "Admin"), angleDoubleRightIconHTML));

            List<MenuItem> streamingMenuItemChildList = new List<MenuItem>();

            streamingMenuItemChildList.Add(new MenuItem(1, "ListVms", "Listar servidores", "Listar servidores", menuLateralURLHelper.Action("Index", "Azure"), angleDoubleRightIconHTML));
            streamingMenuItemChildList.Add(new MenuItem(1, "ManageVm", "Gerir servidores", "Gerir servidores", menuLateralURLHelper.Action("Manage", "Azure"), angleDoubleRightIconHTML));
            streamingMenuItemChildList.Add(new MenuItem(1, "CreateVm", "Adicionar servidor", "Adicionar servidor", menuLateralURLHelper.Action("CreateVm", "Azure"), angleDoubleRightIconHTML));
            streamingMenuItemChildList.Add(new MenuItem(1, "VmLogs", "Logs dos servidores", "Logs dos servidores", menuLateralURLHelper.Action("VmLogs", "Azure"), angleDoubleRightIconHTML));

            menu.Add(new MenuItem(0, "StrMenuItem", "Streaming", "Streaming", serverIconHTML, streamingMenuItemChildList, true));
            //menu.Add(new MenuItem(0, "TempMenuItem", "Credenciais Temporárias", "Credenciais Temporárias", menuLateralURLHelper.Action("Index", "Home"), angleDoubleRightIconHTML));
            menu.Add(new MenuItem(0, "AdminMenuItem", "Gestor da plataforma", "Gestor da plataforma", cogsIconHTML, adminMenuItemChildList, false));

            return menu;
        }

        private static List<MenuItem> BuildAplicacoesPortalBaseMenuLateral(List<AplicacaoPortalBase> aplicacoesStreaming)
        {
            // Guarda um código HTML genérico respectivo aos ícones das aplicações do Streaming
            string aplicacoesStreamingIconHTML = @"<span class=""portalbase-app-square small"" style=""background-color: {0}""><span>{1}</span></span>";

            List<MenuItem> aplicacoesStreamingMenu = new List<MenuItem>();
            List<MenuItem> aplicacoesStreamingMenuGruposList = new List<MenuItem>();

            // Obtém, do RCU, todos os grupos de menu disponíveis
            List<GrupoMenu> gruposMenu = _rcuClient.GetGruposMenu();

            // Por cada grupo de menu obtido, filtra as aplicações do Streaming que pertencam a esse mesmo grupo de menu
            // Depois, para cada uma dessas aplicações filtradas, cria um item de menu lateral e guarda-o na lista
            foreach (GrupoMenu grupoMenu in gruposMenu)
            {
                List<MenuItem> aplicacoesStreamingMenuGrupoList = new List<MenuItem>();

                List<AplicacaoPortalBase> aplicacoesStreamingGrupoList = aplicacoesStreaming.Where(apb => apb.GruposMenuPortalBase.Contains(grupoMenu.Codigo)).ToList();

                foreach (AplicacaoPortalBase aplicacaoStreaming in aplicacoesStreamingGrupoList)
                {
                    // Com os dados da sigla e da cor, constrói o ícone da aplicação a apresentar no menu lateral
                    string aplicacaoStreamingIconHTML = string.Format(aplicacoesStreamingIconHTML, aplicacaoStreaming.CorPortalBase, aplicacaoStreaming.SiglaPortalBase);

                    aplicacoesStreamingMenuGrupoList.Add(new MenuItem(2, "AplicacoesPortalBase" + aplicacaoStreaming.IdentificadorPortalBase + "SubSubMenuItem", aplicacaoStreaming.Descricao, aplicacaoStreaming.Nome, aplicacaoStreaming.Link, true, aplicacaoStreamingIconHTML));
                }

                if (aplicacoesStreamingMenuGrupoList.Count > 0)
                {
                    string grupoMenuIconHTML = "";

                    switch (grupoMenu.Codigo)
                    {
                        case "E":
                            grupoMenuIconHTML = @"<span class=""fa fa-graduation-cap""></span>";

                            break;

                        case "I":
                            grupoMenuIconHTML = @"<span class=""fa fa-book""></span>";

                            break;

                        case "S":
                            grupoMenuIconHTML = @"<span class=""fa fa-users""></span>";

                            break;

                        default:
                            break;
                    }

                    aplicacoesStreamingMenuGruposList.Add(new MenuItem(1, "AplicacoesPortalBase" + grupoMenu.Codigo + "SubMenuItem", grupoMenu.Grupo, grupoMenu.Grupo, grupoMenuIconHTML, aplicacoesStreamingMenuGrupoList, false));
                }
            }

            if (aplicacoesStreamingMenuGruposList.Count > 0)
            {
                aplicacoesStreamingMenu.Add(new MenuItem(0, "AplicacoesPortalBaseMenuItem", "As Minhas Aplicações", "As Minhas Aplicações", @"<span class=""fa fa-desktop""></span>", aplicacoesStreamingMenuGruposList));
            }

            return aplicacoesStreamingMenu;
        }

        private static void CheckMenuLateralAutorizacoes(List<MenuItem> menuItemList)
        {
            // Instancia uma cópia da lista, para desta forma ser possível enumerar e remover items da lista ao mesmo tempo
            List<MenuItem> oldMenuItemList = new List<MenuItem>(menuItemList);

            foreach (MenuItem menuItem in oldMenuItemList)
            {
                // Se o utilizador autenticado não estiver autorizado a ver um item ou subitem, então esse item ou subitem é removido
                if (StreamingSession.Session.Autorizacoes.SingleOrDefault(a => a.Pagina == "" && a.ItemName == menuItem.Name) == default(Autorizacao))
                {
                    menuItemList.Remove(menuItem);
                }

                if (menuItem.MenuItemChildList.Count > 0)
                {
                    CheckMenuLateralAutorizacoes(menuItem.MenuItemChildList);
                }
            }
        }

        private static int SetMenuLateralItemIDs(List<MenuItem> menuLateral, int startingItemID)
        {
            for (int i = 0; i < menuLateral.Count; i++)
            {
                menuLateral[i].Id = startingItemID;

                startingItemID++;

                if (menuLateral[i].MenuItemChildList.Count > 0)
                {
                    startingItemID = SetMenuLateralItemIDs(menuLateral[i].MenuItemChildList, startingItemID);
                }
            }

            return startingItemID;
        }
    }
}