using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;

using Streaming.Clients;
using Streaming.Helpers;

using StreamingAssemblies.DataAccess;
using StreamingAssemblies.Models;
using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Cargos;
using StreamingAssemblies.Models.QuoVadis.RCU;

namespace Streaming.App_Start
{
    public class Autenticacao
    {
        private static readonly StreamingService _streamingService = new StreamingService(ConfigurationManager.ConnectionStrings["streamingDBString"].ConnectionString);

        private static readonly CargosClient _cargosClient = new CargosClient();
        private static readonly GruposClient _gruposClient = new GruposClient();
        private static readonly RCUClient _rcuClient = new RCUClient();

        public enum TipoGrupoPortalBase
        {
            NULL,
            GRUPO,
            CARGO,
            PERFIL,
            GRUPOQUOVADIS
        }

        public enum TipoGrupoQuoVadis
        {
            GRUPO,
            GRUPOSISTEMA
        }

        public static bool IsLoggedIn()
        {
            return GetLoginStatus();
        }

        public static bool IsAdmin()
        {
            if(GetLoginStatus() == true)
            {
                if (IsInGrupo("ADMIN", TipoGrupoPortalBase.GRUPO) == true)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsEstudante()
        {
            if(GetLoginStatus() == true)
            {
                if (string.IsNullOrEmpty(Utilities.GetNumMecFromUsername(StreamingSession.Session.Username)) == false)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsTemporario()
        {
            if(GetLoginStatus() == true)
            {
                Parametro credenciaisTemporariasDominio = _streamingService.NSI_STP_Parametros_S_ByChave("Auth.CredenciaisTemporarias.Dominio");

                if (string.IsNullOrEmpty(credenciaisTemporariasDominio.Valor) == false)
                {
                    if (StreamingSession.Session.Username.Contains(credenciaisTemporariasDominio.Valor) == true)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool IsInGrupo(string nomeGrupo, TipoGrupoPortalBase tipoGrupo)
        {
            if (GetLoginStatus() == true)
            {
                if (StreamingSession.Session.Grupos.SingleOrDefault(g => g.Nome == nomeGrupo && g.IdTipo == Convert.ToInt32(tipoGrupo)) != default(Grupo))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool GetLoginStatus()
        {
            // Primeiro verifica se a sessão do browser possui dados de um login
            // Se não possuir, procede para a autenticação por Shibboleth
            if (string.IsNullOrEmpty(StreamingSession.Session.Username) == false && (StreamingSession.Session.IUPI != null && StreamingSession.Session.IUPI != new Guid()))
            {
                // Depois verifica se a sessão do browser possui a informação pessoal desse login
                // Isto é necessário para verificar, por exemplo, se um estudante seleccionou um perfil (matrícula) para entrar no Streaming
                // Se esse estudante ainda não seleccionou um perfil (matrícula), então esse estudante ainda não se encontra totalmente autenticado
                if(string.IsNullOrEmpty(StreamingSession.Session.Nome) == false)
                {
                    return true;
                }
            }

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.UTAD.Shibboleth"]) == true)
            {
                // Verifica se se encontra algum login autenticado pelo Shibboleth
                // Se encontrar assim um login, tenta autenticá-lo no Streaming
                if (HttpContext.Current.Session["shib"] != null)
                {
                    string shibbolethUsername = HttpContext.Current.Session["shib"].ToString();

                    if (AutenticarLoginShibboleth(shibbolethUsername) == true)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /*AUTENTICAÇÃO POR SHIBBOLETH*/
        public static bool AutenticarLoginShibboleth(string shibbolethUsername)
        {
            string nome = "";

            Guid? iupi = new Guid();

            string shibbolethUsernameSplit = Utilities.GetNumMecFromUsername(shibbolethUsername);

            if (string.IsNullOrEmpty(shibbolethUsernameSplit) == false)
            {
                EstudanteNomeAndIUPI estudanteNomeAndIUPI = _rcuClient.GetEstudanteNomeAndIUPI(shibbolethUsernameSplit);

                nome = estudanteNomeAndIUPI.Nome;
                iupi = estudanteNomeAndIUPI.IUPI;
            }
            else
            {
                UtilizadorNomeAndIUPI utilizadorNomeAndIUPI = _rcuClient.GetUtilizadorNomeAndIUPI(shibbolethUsername);

                nome = utilizadorNomeAndIUPI.Nome;
                iupi = utilizadorNomeAndIUPI.IUPI;
            }

            if (iupi != null && iupi != new Guid())
            {
                // Regista o acesso deste login, guardando o seu ID na sessão
                bool success = RegistarLoginAcesso(shibbolethUsername, iupi.Value, nome, DateTime.Now, true);

                if (success == true)
                {
                    // Só procede com a verificação de permissões apenas se tal mecanismo estiver ativado no Web.config
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Auth.Autorizacoes"]) == true)
                    {
                        // Obtém, com o IUPI obtido anteriormente, os cargos e os perfis do login, guardando todos esses dados nas respetivas variáveis de sessão
                        // Com esses dados ir-se-á verificar se o login possui permissões para aceder à aplicação
                        success = LoadLoginCargosAndPerfis(shibbolethUsername, iupi.Value) && LoadLoginGruposQuoVadis(iupi.Value) && LoadLoginGruposAndAutorizacoes();
                    }

                    if (success == true)
                    {
                        // Carrega também toda a informação pessoal do login
                        if (LoadLoginInfo(shibbolethUsername, iupi.Value) == true)
                        {
                            // Se tudo correr bem o login fica inteiramente autenticado
                            // Sendo assim, retorna true
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public static bool IsLoginAdmin(string username)
        {
            bool success = false;

            try
            {
                // Obtém o IUPI do suposto administrador e só procede se o mesmo realmente se encontrar no grupo de administração do Streaming
                Guid? adminImpersonacaoIUPI = _rcuClient.GetUtilizadorIUPI(username);

                if (adminImpersonacaoIUPI != null && adminImpersonacaoIUPI != new Guid())
                {
                    // Para verificar se o suposto administrador é mesmo um administrador no Streaming, obtém prematuramente os grupos a que esse utilizador pertence, e verifica se um desses grupos é o grupo de administração
                    if (_streamingService.NSI_STP_Grupo_S_ByIUPI(adminImpersonacaoIUPI.Value).SingleOrDefault(g => g.Nome == "ADMIN") != default(Grupo))
                    {
                        success = true;
                    }
                }
            }
            catch (Exception e)
            {
                StreamingSession.ClearSession();
            }

            return success;
        }

        public static bool RegistarLoginAcesso(string username, Guid iupi, string nome, DateTime loginDate, bool isFromUTAD)
        {
            bool success = false;

            try
            {
                int idUser = _streamingService.NSI_STP_User_IU(username, iupi, nome, loginDate, isFromUTAD);

                if (idUser > 0)
                {
                    StreamingSession.Session.IdUser = idUser;

                    success = true;
                }
            }
            catch (Exception e)
            {
                StreamingSession.ClearSession();
            }

            return success;
        }

        public static bool LoadLoginCargosAndPerfis(string username, Guid iupi)
        {
            bool success = false;

            try
            {
                string usernameSplit = Utilities.GetNumMecFromUsername(username);

                if (string.IsNullOrEmpty(usernameSplit) == false)
                {
                    // Como os estudantes não possuem cargos, inicializa a lista de cargos do estudante como vazia
                    StreamingSession.Session.Cargos = new List<Cargo>();

                    List<EstudantePerfil> estudantePerfis = _rcuClient.GetEstudantePerfis(iupi);

                    foreach(EstudantePerfil estudantePerfil in estudantePerfis)
                    {
                        SessionPerfil sessionPerfil = new SessionPerfil();

                        sessionPerfil.Id = estudantePerfil.IdMatricula;
                        sessionPerfil.Ano = Convert.ToInt32(estudantePerfil.AnoLectivo);
                        sessionPerfil.Tipo = "ALUNOS";
                        sessionPerfil.Categoria = estudantePerfil.Estado;
                        sessionPerfil.CodigoCategoria = estudantePerfil.IdEstado;
                        sessionPerfil.UnidadeOrganica = estudantePerfil.Curso;
                        sessionPerfil.CodigoUnidadeOrganica = estudantePerfil.CodCurso;
                        
                        if(estudantePerfil.IdEstado == "1")
                        {
                            sessionPerfil.Ativo = true;
                        }

                        StreamingSession.Session.Perfis.Add(sessionPerfil);
                    }
                }
                else
                {
                    StreamingSession.Session.Cargos = _cargosClient.GetPessoaCargos(username);

                    List<UtilizadorPerfil> utilizador = _rcuClient.GetUtilizador(iupi);

                    foreach(UtilizadorPerfil utilizadorPerfil in utilizador)
                    {
                        SessionPerfil sessionPerfil = new SessionPerfil();

                        sessionPerfil.Id = utilizadorPerfil.Id;
                        sessionPerfil.Ano = utilizadorPerfil.DataEntrada.Year;
                        sessionPerfil.Tipo = utilizadorPerfil.PerfilTipo;
                        sessionPerfil.Categoria = utilizadorPerfil.Categoria;
                        sessionPerfil.CodigoCategoria = utilizadorPerfil.CodCategoria;
                        sessionPerfil.UnidadeOrganica = utilizadorPerfil.Entidade;
                        sessionPerfil.CodigoUnidadeOrganica = utilizadorPerfil.CodEntidade;
                        sessionPerfil.Ativo = utilizadorPerfil.PerfilAtivo;

                        StreamingSession.Session.Perfis.Add(sessionPerfil);
                    }
                }

                success = true;
            }
            catch(Exception e)
            {
                StreamingSession.ClearSession();
            }

            return success;
        }

        public static bool LoadLoginGruposQuoVadis(Guid iupi)
        {
            bool success = false;

            try
            {
                List<Grupo> userGruposList = new List<Grupo>();

                // Obtém todos os grupos do QuoVadis a que o login pertence
                // Como os grupos do QuoVadis são de dois tipos totalmente distintos, é necessário obter os grupos de cada um desses tipos de maneiras diferentes
                // Primeiro obtemos os grupos de sistema (onde utilizadores são adicionados manualmente) a que o login pertence
                List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo> gruposSistemaList = _gruposClient.GetGruposByUtilizador(iupi);

                foreach (StreamingAssemblies.Models.QuoVadis.Grupos.Grupo grupoSistema in gruposSistemaList)
                {
                    Grupo quoVadisGrupo = _streamingService.NSI_STP_Grupo_S_ByNomeAndTipo(grupoSistema.Nome, Convert.ToInt32(TipoGrupoPortalBase.GRUPOQUOVADIS));

                    if (quoVadisGrupo.Id != 0)
                    {
                        // No entanto, se já existir na lista de grupos um grupo com o mesmo nome, então não adiciona o grupo obtido à lista de grupos
                        if (userGruposList.SingleOrDefault(g => g.Nome == quoVadisGrupo.Nome) == default(Grupo))
                        {
                            userGruposList.Add(quoVadisGrupo);
                        }
                    }
                }

                // Depois obtemos os grupos (onde queries de SQL são usadas para agrupar os seus utilizadores)
                List<StreamingAssemblies.Models.QuoVadis.Grupos.Grupo> gruposList = _gruposClient.GetGruposByTipo(Convert.ToInt32(TipoGrupoQuoVadis.GRUPO));

                foreach (StreamingAssemblies.Models.QuoVadis.Grupos.Grupo grupo in gruposList)
                {
                    // Por cada grupo obtido, obtém os seus respectivos utilizadores
                    List<StreamingAssemblies.Models.QuoVadis.Grupos.Utilizador> utilizadoresGrupoList = _gruposClient.GetGrupoUtilizadores(grupo.Nome);

                    // Através do IUPI, verifica se o login pertence ao grupo
                    if (utilizadoresGrupoList.SingleOrDefault(u => u.Iupi == iupi) != default(StreamingAssemblies.Models.QuoVadis.Grupos.Utilizador))
                    {
                        Grupo quoVadisGrupo = _streamingService.NSI_STP_Grupo_S_ByNomeAndTipo(grupo.Nome, Convert.ToInt32(TipoGrupoPortalBase.GRUPOQUOVADIS));

                        if (quoVadisGrupo.Id != 0)
                        {
                            // No entanto, se já existir na lista de grupos um grupo com o mesmo nome, então não adiciona o grupo obtido à lista de grupos
                            if (userGruposList.SingleOrDefault(g => g.Nome == quoVadisGrupo.Nome) == default(Grupo))
                            {
                                userGruposList.Add(quoVadisGrupo);
                            }
                        }
                    }
                }

                StreamingSession.Session.Grupos.AddRange(userGruposList);

                success = true;
            }
            catch (Exception e)
            {
                StreamingSession.ClearSession();
            }

            return success;
        }

        public static bool LoadLoginGruposAndAutorizacoes()
        {
            bool success = false;

            try
            {
                // Instancia uma lista que irá guardar todos os grupos onde o login pertence e possui permissões
                List<Grupo> userGruposList = new List<Grupo>();

                // Verifica cada perfil que o login possui, e vê se existe um grupo correspondente na base de dados
                // Se existir, adiciona esse grupo obtido à lista de grupos
                foreach (SessionPerfil perfil in StreamingSession.Session.Perfis.Where(sp => sp.Ativo == true))
                {
                    Grupo perfilGrupo = _streamingService.NSI_STP_Grupo_S_ByNomeAndTipo(perfil.Tipo, Convert.ToInt32(TipoGrupoPortalBase.PERFIL));

                    if (perfilGrupo.Id != 0)
                    {
                        // No entanto, se já existir na lista de grupos um grupo com o mesmo nome, então não adiciona o grupo obtido à lista de grupos
                        if(userGruposList.SingleOrDefault(g => g.Nome == perfilGrupo.Nome) == default(Grupo))
                        {
                            userGruposList.Add(perfilGrupo);
                        }
                    }
                }

                // De seguida verifica cada cargo que o login possui, e vê se existe um grupo correspondente na base de dados
                // Se existir, adiciona de igual forma esse grupo obtido à lista de grupos
                foreach (Cargo cargo in StreamingSession.Session.Cargos)
                {
                    Grupo cargoGrupo = _streamingService.NSI_STP_Grupo_S_ByNomeAndTipo(cargo.TipoCargo, Convert.ToInt32(TipoGrupoPortalBase.CARGO));

                    if (cargoGrupo.Id != 0)
                    {
                        // No entanto, se já existir na lista de grupos um grupo com o mesmo nome, então não adiciona o grupo obtido à lista de grupos
                        if (userGruposList.SingleOrDefault(g => g.Nome == cargoGrupo.Nome) == default(Grupo))
                        {
                            userGruposList.Add(cargoGrupo);
                        }
                    }
                }

                // Por fim, através do ID de acesso do login guardado anteriormente, obtém e adiciona todos os grupos (do tipo "G"), definidos na própria base de dados, a que esse login pertence
                userGruposList.AddRange(_streamingService.NSI_STP_Grupo_S_ByUser(StreamingSession.Session.IdUser));

                // Guarda todos os grupos obtidos na sessão do browser
                StreamingSession.Session.Grupos.AddRange(userGruposList);

                // Se o login pertence a pelo menos um grupo, continua
                if (StreamingSession.Session.Grupos.Count > 0)
                {
                    // Por cada grupo a que o login pertence, obtém os elementos HTML autorizados a esse mesmo grupo e guarda-os, sem repetições, na respetiva variável de sessão
                    foreach (Grupo g in StreamingSession.Session.Grupos)
                    {
                        List<Autorizacao> grupoAutorizacoesList = _streamingService.NSI_STP_Autorizacao_S_ByGrupo(g.Id);

                        foreach(Autorizacao grupoAutorizacao in grupoAutorizacoesList)
                        {
                            if(StreamingSession.Session.Autorizacoes.SingleOrDefault(a => a.Id == grupoAutorizacao.Id) == default(Autorizacao))
                            {
                                StreamingSession.Session.Autorizacoes.Add(grupoAutorizacao);
                            }
                        }
                    }

                    success = true;
                }
                else
                {
                    StreamingSession.ClearSession();
                }
            }
            catch(Exception e)
            {
                StreamingSession.ClearSession();
            }

            return success;
        }

        public static bool LoadLoginInfo(string username, Guid iupi)
        {
            // Guarda o username e o IUPI do login na sessão do browser
            StreamingSession.Session.Username = username;
            StreamingSession.Session.IUPI = iupi;

            string usernameSplit = Utilities.GetNumMecFromUsername(username);

            try
            {
                if (string.IsNullOrEmpty(usernameSplit) == true)
                {
                    // De todos os perfis ativos do login, selecciona automaticamente o primeiro 
                    UtilizadorPerfil utilizadorPerfil = _rcuClient.GetUtilizador(iupi).Where(up => up.PerfilAtivo == true).First();

                    StreamingSession.Session.Nome = utilizadorPerfil.Nome;

                    return true;
                }
                else
                {
                    // Obtém a informação da foto do estudante e guarda-a também na sessão do browser
                    // Por agora obtém apenas a foto - os dados do estudante são carregados mais à frente
                    EstudanteFoto estudanteFoto = _rcuClient.GetEstudanteFoto(iupi);

                    if (estudanteFoto.Foto != null && estudanteFoto.Foto.Count() > 0)
                    {
                        string estudanteFotoBase64String = Utilities.EncodeBase64String(estudanteFoto.Foto);

                        StreamingSession.Session.FotoHTMLSrc = string.Format("data:image/{0};base64,{1}", "", estudanteFotoBase64String);
                    }

                    return true;
                }
            }
            catch(Exception e)
            {
                StreamingSession.ClearSession();
            }

            return false;
        }

        public static void LoadEstudantePerfilInfo(string nome, string codigoCurso, string regime)
        {
            StreamingSession.Session.Nome = nome;
            StreamingSession.Session.CodigoCurso = codigoCurso;
            StreamingSession.Session.Regime = regime;
        }
    }
}