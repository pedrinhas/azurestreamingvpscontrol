using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using Streaming.App_Start;
using Streaming.Helpers;
using StreamingAssemblies.Helpers.Azure;
using StreamingAssemblies.Helpers.Azure.FileManagement;

namespace Streaming
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
            Response.Headers.Remove("X-AspNetMvc-Version");
            Response.Headers.Remove("X-Powered-By");
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(string), new EmptyStringModelBinder());
#if DEBUG
            //TODO: teletrabalho
            if (System.IO.Directory.Exists(ConfigurationHelper.AppSettings.Azure.ConfigFileFolder))
            {
                AzureFileManager.Initialize(ConfigurationHelper.AppSettings.Azure.ConfigFileFolder);
            }
            else
            {
                AzureFileManager.Initialize(ConfigurationHelper.AppSettings.Azure.ConfigFileFolderCasa);
            }
#else
                AzureFileManager.Initialize(ConfigurationHelper.AppSettings.Azure.ConfigFileFolder);
#endif
        }
    }
}
