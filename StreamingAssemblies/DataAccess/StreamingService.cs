using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;

using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Mensagens;

namespace StreamingAssemblies.DataAccess
{
    public class StreamingService : BaseDataAccess
    {
        public StreamingService(string streamingDBString) : base(streamingDBString)
        {
        }

        public List<Autorizacao> NSI_STP_Autorizacao_LS()
        {
            List<Autorizacao> autorizacoesList = new List<Autorizacao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Autorizacao autorizacao = new Autorizacao();

                                autorizacao.Id = dbReader.GetInt32(0);
                                autorizacao.ItemName = dbReader.GetString(1);
                                autorizacao.Pagina = dbReader.GetString(2);

                                autorizacoesList.Add(autorizacao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return autorizacoesList;
        }

        public Autorizacao NSI_STP_Autorizacao_S(int id)
        {
            Autorizacao autorizacao = new Autorizacao();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if(dbReader.Read() == true)
                            {
                                autorizacao.Id = id;
                                autorizacao.ItemName = dbReader.GetString(0);
                                autorizacao.Pagina = dbReader.GetString(1);
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {

            }

            return autorizacao;
        }

        public List<Autorizacao> NSI_STP_Autorizacao_S_ByGrupo(int idGrupo)
        {
            List<Autorizacao> autorizacoesList = new List<Autorizacao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_S_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Autorizacao autorizacao = new Autorizacao();

                                autorizacao.Id = dbReader.GetInt32(0);
                                autorizacao.ItemName = dbReader.GetString(1);
                                autorizacao.Pagina = dbReader.GetString(2);

                                autorizacoesList.Add(autorizacao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return autorizacoesList;
        }

        public int NSI_STP_Autorizacao_I(string itemName, string pagina)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_itemName", itemName));
                        dbCommand.Parameters.Add(new SqlParameter("@_pagina", pagina));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Autorizacao_U(int id, string itemName, string pagina)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_itemName", itemName));
                        dbCommand.Parameters.Add(new SqlParameter("@_pagina", pagina));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Autorizacao_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Autorizacao_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<Grupo> NSI_STP_Grupo_LS()
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = dbReader.GetInt32(2);
                                grupo.NomeTipo = dbReader.GetString(3);
                                grupo.Descricao = dbReader.GetString(4);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {
                
            }

            return gruposList;
        }

        public Grupo NSI_STP_Grupo_S(int id)
        {
            Grupo grupo = new Grupo();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if(dbReader.Read() == true)
                            {
                                grupo.Id = id;
                                grupo.Nome = dbReader.GetString(0);
                                grupo.IdTipo = dbReader.GetInt32(1);
                                grupo.Descricao = dbReader.GetString(2);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return grupo;
        }

        public Grupo NSI_STP_Grupo_S_ByNomeAndTipo(string nome, int idTipo)
        {
            Grupo grupo = new Grupo();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByNomeAndTipo";
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = nome;
                                grupo.IdTipo = idTipo;
                                grupo.Descricao = dbReader.GetString(1);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return grupo;
        }

        public List<Grupo> NSI_STP_Grupo_S_ByTipo(int idTipo)
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByTipo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = idTipo;
                                grupo.Descricao = dbReader.GetString(2);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposList;
        }

        public List<Grupo> NSI_STP_Grupo_S_ByUser(int idUser)
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByUser";
                        dbCommand.Parameters.Add(new SqlParameter("@_idUser", idUser));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = 1;
                                grupo.Descricao = dbReader.GetString(2);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposList;
        }

        public List<Grupo> NSI_STP_Grupo_S_ByIUPI(Guid iupi)
        {
            List<Grupo> gruposList = new List<Grupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_S_ByIUPI";
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", iupi));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Grupo grupo = new Grupo();

                                grupo.Id = dbReader.GetInt32(0);
                                grupo.Nome = dbReader.GetString(1);
                                grupo.IdTipo = 1;
                                grupo.Descricao = dbReader.GetString(2);

                                gruposList.Add(grupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposList;
        }

        public int NSI_STP_Grupo_I(string nome, int idTipo, string descricao)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", descricao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Grupo_U(int id, string nome, int idTipo, string descricao)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_idTipo", idTipo));
                        dbCommand.Parameters.Add(new SqlParameter("@_descricao", descricao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Grupo_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Grupo_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<GrupoAutorizacao> NSI_STP_GrupoAutorizacao_S_ByAutorizacao(int idAutorizacao)
        {
            List<GrupoAutorizacao> gruposAutorizacaoList = new List<GrupoAutorizacao>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_S_ByAutorizacao";
                        dbCommand.Parameters.Add(new SqlParameter("@_idAutorizacao", idAutorizacao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                GrupoAutorizacao grupoAutorizacao = new GrupoAutorizacao();

                                grupoAutorizacao.Id = dbReader.GetInt32(0);
                                grupoAutorizacao.IdGrupo = dbReader.GetInt32(1);
                                grupoAutorizacao.IdAutorizacao = idAutorizacao;

                                gruposAutorizacaoList.Add(grupoAutorizacao);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposAutorizacaoList;
        }

        public int NSI_STP_GrupoAutorizacao_I(int idGrupo, int idAutorizacao)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Parameters.Add(new SqlParameter("@_idAutorizacao", idAutorizacao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_GrupoAutorizacao_D_ByGrupo(int idGrupo)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_D_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public int NSI_STP_GrupoAutorizacao_D_ByAutorizacao(int idAutorizacao)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoAutorizacao_D_ByAutorizacao";
                        dbCommand.Parameters.Add(new SqlParameter("@_idAutorizacao", idAutorizacao));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<GrupoMensagem> NSI_STP_GrupoMensagem_S_ByMensagem(int idMensagem)
        {
            List<GrupoMensagem> gruposMensagemList = new List<GrupoMensagem>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoMensagem_S_ByMensagem";
                        dbCommand.Parameters.Add(new SqlParameter("@_idMensagem", idMensagem));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                GrupoMensagem grupoMensagem = new GrupoMensagem();

                                grupoMensagem.Id = dbReader.GetInt32(0);
                                grupoMensagem.IdGrupo = dbReader.GetInt32(1);
                                grupoMensagem.IdMensagem = idMensagem;

                                gruposMensagemList.Add(grupoMensagem);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return gruposMensagemList;
        }

        public int NSI_STP_GrupoMensagem_I(int idGrupo, int idMensagem)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoMensagem_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Parameters.Add(new SqlParameter("@_idMensagem", idMensagem));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_GrupoMensagem_D_ByMensagem(int idMensagem)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_GrupoMensagem_D_ByMensagem";
                        dbCommand.Parameters.Add(new SqlParameter("@_idMensagem", idMensagem));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        public List<Mensagem> NSI_STP_Mensagens_LS()
        {
            List<Mensagem> mensagensList = new List<Mensagem>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                Mensagem mensagem = new Mensagem();

                                mensagem.Id = dbReader.GetInt32(0);
                                mensagem.Titulo = dbReader.GetString(1);
                                mensagem.Texto = dbReader.GetString(2);

                                if (dbReader.IsDBNull(3) == true)
                                {
                                    mensagem.DataPublicacaoInicio = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoInicio = Convert.ToDateTime(dbReader.GetDateTime(3));
                                }

                                if(dbReader.IsDBNull(4) == true)
                                {
                                    mensagem.DataPublicacaoFim = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoFim = Convert.ToDateTime(dbReader.GetDateTime(4));
                                }

                                mensagem.Visibilidade = dbReader.GetInt32(5);

                                mensagensList.Add(mensagem);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return mensagensList;
        }

        public Mensagem NSI_STP_Mensagens_S(int id)
        {
            Mensagem mensagem = new Mensagem();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                mensagem.Id = id;
                                mensagem.Titulo = dbReader.GetString(0);
                                mensagem.Texto = dbReader.GetString(1);

                                if (dbReader.IsDBNull(2) == true)
                                {
                                    mensagem.DataPublicacaoInicio = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoInicio = Convert.ToDateTime(dbReader.GetDateTime(2));
                                }

                                if (dbReader.IsDBNull(3) == true)
                                {
                                    mensagem.DataPublicacaoFim = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoFim = Convert.ToDateTime(dbReader.GetDateTime(3));
                                }

                                mensagem.Visibilidade = dbReader.GetInt32(4);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return mensagem;
        }

        public List<Mensagem> NSI_STP_Mensagens_S_ByVisibilidade(int visibilidade)
        {
            List<Mensagem> mensagensList = new List<Mensagem>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_S_ByVisibilidade";
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidade", visibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Mensagem mensagem = new Mensagem();

                                mensagem.Id = dbReader.GetInt32(0);
                                mensagem.Titulo = dbReader.GetString(1);
                                mensagem.Texto = dbReader.GetString(2);

                                if (dbReader.IsDBNull(3) == true)
                                {
                                    mensagem.DataPublicacaoInicio = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoInicio = Convert.ToDateTime(dbReader.GetDateTime(3));
                                }

                                if (dbReader.IsDBNull(4) == true)
                                {
                                    mensagem.DataPublicacaoFim = null;
                                }
                                else
                                {
                                    mensagem.DataPublicacaoFim = Convert.ToDateTime(dbReader.GetDateTime(4));
                                }

                                mensagem.Visibilidade = visibilidade;

                                mensagensList.Add(mensagem);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return mensagensList;
        }

        public int NSI_STP_Mensagens_I(string titulo, string texto, DateTime? dataPublicacaoInicio, DateTime? dataPublicacaoFim, int visibilidade)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo", titulo));
                        dbCommand.Parameters.Add(new SqlParameter("@_texto", texto));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoInicio", dataPublicacaoInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoFim", dataPublicacaoFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidade", visibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Mensagens_U(int id, string titulo, string texto, DateTime? dataPublicacaoInicio, DateTime? dataPublicacaoFim, int visibilidade)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_titulo", titulo));
                        dbCommand.Parameters.Add(new SqlParameter("@_texto", texto));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoInicio", dataPublicacaoInicio));
                        dbCommand.Parameters.Add(new SqlParameter("@_dataPublicacaoFim", dataPublicacaoFim));
                        dbCommand.Parameters.Add(new SqlParameter("@_visibilidade", visibilidade));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Mensagens_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Mensagens_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return rowCount;
        }

        public List<Parametro> NSI_STP_Parametros_LS()
        {
            List<Parametro> parametrosList = new List<Parametro>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                Parametro parametro = new Parametro();

                                parametro.Id = dbReader.GetInt32(0);
                                parametro.Chave = dbReader.GetString(1);
                                parametro.Valor = dbReader.GetString(2);
                                parametro.Valor1 = dbReader.GetString(3);
                                parametro.Valor2 = dbReader.GetString(4);

                                parametrosList.Add(parametro);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return parametrosList;
        }

        public Parametro NSI_STP_Parametros_S_ByChave(string chave)
        {
            Parametro parametro = new Parametro();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_S_ByChave";
                        dbCommand.Parameters.Add(new SqlParameter("@_chave", chave));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                parametro.Id = dbReader.GetInt32(0);
                                parametro.Chave = chave;
                                parametro.Valor = dbReader.GetString(1);
                                parametro.Valor1 = dbReader.GetString(2);
                                parametro.Valor2 = dbReader.GetString(3);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return parametro;
        }

        public int NSI_STP_Parametros_I(string chave, string valor, string valor1, string valor2)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_chave", chave));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor", valor));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor1", valor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor2", valor2));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Parametros_U(int id, string chave, string valor, string valor1, string valor2)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_U";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Parameters.Add(new SqlParameter("@_chave", chave));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor", valor));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor1", valor1));
                        dbCommand.Parameters.Add(new SqlParameter("@_valor2", valor2));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_Parametros_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_Parametros_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<User> NSI_STP_User_LS()
        {
            List<User> usersList = new List<User>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_LS";
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while(dbReader.Read() == true)
                            {
                                User user = new User();

                                user.Id = dbReader.GetInt32(0);
                                user.Username = dbReader.GetString(1);
                                user.IUPI = dbReader.GetGuid(2);
                                user.Nome = dbReader.GetString(3);
                                user.LastLoginDate = Convert.ToDateTime(GetSafeSqlValue(dbReader, 4));
                                user.IsFromUTAD = dbReader.GetBoolean(5);

                                usersList.Add(user);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return usersList;
        }

        public User NSI_STP_User_S(int id)
        {
            User user = new User();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_S";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            if (dbReader.Read() == true)
                            {
                                user.Id = id;
                                user.Username = dbReader.GetString(0);
                                user.IUPI = dbReader.GetGuid(1);
                                user.Nome = dbReader.GetString(2);
                                user.LastLoginDate = Convert.ToDateTime(GetSafeSqlValue(dbReader, 3));
                                user.IsFromUTAD = dbReader.GetBoolean(4);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return user;
        }

        public int NSI_STP_User_IU(string username, Guid iupi, string nome, DateTime? lastLoginDate, bool isFromUTAD)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_IU";
                        dbCommand.Parameters.Add(new SqlParameter("@_username", username));
                        dbCommand.Parameters.Add(new SqlParameter("@_iupi", iupi));
                        dbCommand.Parameters.Add(new SqlParameter("@_nome", nome));
                        dbCommand.Parameters.Add(new SqlParameter("@_lastLoginDate", lastLoginDate));
                        dbCommand.Parameters.Add(new SqlParameter("@_isFromUTAD", isFromUTAD));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }   
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_User_D(int id)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_User_D";
                        dbCommand.Parameters.Add(new SqlParameter("@_id", id));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public List<UserGrupo> NSI_STP_UserGrupo_S_ByGrupo(int idGrupo)
        {
            List<UserGrupo> usersGrupoList = new List<UserGrupo>();

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_S_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        using (SqlDataReader dbReader = dbCommand.ExecuteReader())
                        {
                            while (dbReader.Read() == true)
                            {
                                UserGrupo userGrupo = new UserGrupo();

                                userGrupo.Id = dbReader.GetInt32(0);
                                userGrupo.IdUser = dbReader.GetInt32(1);
                                userGrupo.IdGrupo = idGrupo;

                                usersGrupoList.Add(userGrupo);
                            }
                        }

                        dbConn.Close();
                    }
                }
            }
            catch (Exception e)
            {

            }

            return usersGrupoList;
        }

        public int NSI_STP_UserGrupo_I(int idUser, int idGrupo)
        {
            int returnValue = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_I";
                        dbCommand.Parameters.Add(new SqlParameter("@_idUser", idUser));
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return returnValue;
        }

        public int NSI_STP_UserGrupo_D_ByUser(int idUser)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_D_ByUser";
                        dbCommand.Parameters.Add(new SqlParameter("@_idUser", idUser));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }

        public int NSI_STP_UserGrupo_D_ByGrupo(int idGrupo)
        {
            int rowCount = 0;

            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "nsi_stp_UserGrupo_D_ByGrupo";
                        dbCommand.Parameters.Add(new SqlParameter("@_idGrupo", idGrupo));
                        dbCommand.Connection = dbConn;

                        dbConn.Open();

                        rowCount = Convert.ToInt32(dbCommand.ExecuteScalar());

                        dbConn.Close();
                    }
                }
            }
            catch(Exception e)
            {

            }

            return rowCount;
        }
    }
}