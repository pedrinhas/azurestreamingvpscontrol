﻿using System;
using System.Data.SqlClient;

namespace StreamingAssemblies.DataAccess
{
    /// <summary>
    /// Defines the <see cref="BaseDataAccess" />.
    /// </summary>
    public class BaseDataAccess
    {
        /// <summary>
        /// Defines the _connectionString.
        /// </summary>
        protected string _connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseDataAccess"/> class.
        /// </summary>
        /// <param name="connectionString">The connectionString<see cref="string"/>.</param>
        public BaseDataAccess(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _connectionString = connectionString;
        }

        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="i">The i<see cref="int"/>.</param>
        /// <returns>The <see cref="object"/>.</returns>
        protected object GetSafeSqlValue(SqlDataReader dbReader, int i)
        {
            return GetSafeSqlValue<object>(dbReader, i);
        }

        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="i">The i<see cref="int"/>.</param>
        /// <returns>The <see cref="T"/>.</returns>
        protected T GetSafeSqlValue<T>(SqlDataReader dbReader, int i)
        {
            if (dbReader == null)
            {
                throw new ArgumentNullException(nameof(dbReader));
            }
            if (i < 0 || i >= dbReader.FieldCount)
            {
                throw new ArgumentException($"Index out of range: {i}", nameof(i));
            }

            if (!dbReader.IsDBNull(i))
            {
                var dbVal = dbReader.GetValue(i);

                if (dbVal is T)
                {
                    return (T)dbVal;
                }
                try
                {
                    return (T)Convert.ChangeType(dbVal, typeof(T));
                }
                catch (InvalidCastException)
                {

                }
            }

            return default;
        }

        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="object"/>.</returns>
        protected object GetSafeSqlValue(SqlDataReader dbReader, string columnName)
        {
            return GetSafeSqlValue<object>(dbReader, columnName);
        }

        /// <summary>
        /// The GetSafeSqlValue.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="dbReader">The dbReader<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="T"/>.</returns>
        protected T GetSafeSqlValue<T>(SqlDataReader dbReader, string columnName)
        {
            if (dbReader == null)
            {
                throw new ArgumentNullException(nameof(dbReader));
            }
            if (string.IsNullOrWhiteSpace(columnName))
            {
                throw new ArgumentNullException(nameof(columnName));
            }

            if (!HasColumn(dbReader, columnName))
            {
                return default;
                //throw new ArgumentException($"Invalid column name: {columnName}");
            }

            return GetSafeSqlValue<T>(dbReader, dbReader.GetOrdinal(columnName));
        }

        /// <summary>
        /// The HasColumn.
        /// </summary>
        /// <param name="dr">The dr<see cref="SqlDataReader"/>.</param>
        /// <param name="columnName">The columnName<see cref="string"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        protected static bool HasColumn(SqlDataReader dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
