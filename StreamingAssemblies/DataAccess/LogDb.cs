﻿using Microsoft.Azure.Management.AppService.Fluent.Models;
using StreamingAssemblies.Helpers.Misc;
using StreamingAssemblies.Models.Azure;
using StreamingAssemblies.Models.Azure.Logs;
using StreamingAssemblies.Models.Logs;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingAssemblies.DataAccess
{
    public class LogDb : BaseDataAccess
    {
        public LogDb(string connectionString) : base(connectionString)
        {
            AzureLog = new AzureLogDb(connectionString);
        }

        public async Task<StreamingResult> Log(StreamingLog log)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(log, nameof(log));

                return await internal_Log(log.Type, log.Message, log.StackTrace, log.IUPI);
            }
            catch (Exception ex)
            {
                return new StreamingResult(ex);
            }
        }

        public async Task<StreamingResult> Log(StreamingResult result, Guid iupi)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(result, nameof(result));

                return await Log(new StreamingLog(result, iupi));
            }
            catch (Exception ex)
            {
                return new StreamingResult(ex);
            }
        }

        public async Task<StreamingResult> Log(StreamingResult result, string iupi)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(result, nameof(result));
                ParameterExceptionHelper.ThrowIfArgumentIsNull(iupi, nameof(iupi));

                return await Log(new StreamingLog(result, iupi));
            }
            catch (Exception ex)
            {
                return new StreamingResult(ex);
            }
        }

        public async Task<StreamingResult> Log(Exception exception, Guid iupi)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(exception, nameof(exception));

                return await Log(new StreamingLog(exception, iupi));
            }
            catch (Exception ex)
            {
                return new StreamingResult(ex);
            }
        }

        public async Task<StreamingResult> Log(Exception exception, string iupi)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(exception, nameof(exception));
                ParameterExceptionHelper.ThrowIfArgumentIsNull(iupi, nameof(iupi));

                return await Log(new StreamingLog(exception, iupi));
            }
            catch (Exception ex)
            {
                return new StreamingResult(ex);
            }
        }

        private async Task<StreamingResult> internal_Log(string type, string message, string stackTrace, Guid iupi)
        {
            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Log_I";
                    dbCommand.Connection = dbConn;

                    dbCommand.Parameters.AddWithValue("@_type", type);
                    dbCommand.Parameters.AddWithValue("@_message", message);
                    dbCommand.Parameters.AddWithValue("@_stackTrace", stackTrace);
                    dbCommand.Parameters.AddWithValue("@_iupiUtilizador", iupi != Guid.Empty ? iupi.ToString() : null);

                    await dbConn.OpenAsync();

                    await dbCommand.ExecuteScalarAsync();
                }

                return new StreamingResult(true);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingResult(ex);
            }
        }

        public AzureLogDb AzureLog { get; }

        public class AzureLogDb : BaseDataAccess
        {
            private const string TRIED_TO_START = nameof(TRIED_TO_START);
            private const string STARTED = nameof(STARTED);
            private const string TRIED_TO_STOP = nameof(TRIED_TO_STOP);
            private const string STOPPED = nameof(STOPPED);
            private const string ERROR = nameof(ERROR);
            private const string UNKNOWN = nameof(UNKNOWN);

            public AzureLogDb(string connectionString) : base(connectionString)
            {
                
            }

            public async Task<StreamingResult> Log_I(AzureLog azureLog)
            {
                try
                {
                    ParameterExceptionHelper.ThrowIfArgumentIsNull(azureLog, nameof(azureLog));

                    return await Log_I(azureLog.IdUser, azureLog.Message);
                }
                catch (Exception ex)
                {
                    return new StreamingResult(ex);
                }
            }

            public async Task<StreamingResult> Log_I(int idUser, string message)
            {
                try
                {
                    ParameterExceptionHelper.ThrowIfArgumentIsNull(message, nameof(message));

                    return await internal_Log_I(idUser, message);
                }
                catch (Exception ex)
                {
                    return new StreamingResult(ex);
                }
            }

            public async Task<StreamingResult> Log_WithVM_I(AzureLog azureLog)
            {
                try
                {
                    ParameterExceptionHelper.ThrowIfArgumentIsNull(azureLog, nameof(azureLog));

                    return await Log_WithVM_I(azureLog.IdUser, azureLog.Message, azureLog.IdVirtualMachine, azureLog.Action);
                }
                catch (Exception ex)
                {
                    return new StreamingResult(ex);
                }
            }

            public async Task<StreamingResult> Log_WithVM_I(int idUser, string message, int idVm, AzureLogAction action)
            {
                try
                {
                    ParameterExceptionHelper.ThrowIfArgumentIsNull(message, nameof(message));

                    return await internal_Log_WithVM_I(idUser, message, idVm, action);
                }
                catch (Exception ex)
                {
                    return new StreamingResult(ex);
                }
            }

            public async Task<StreamingValueResult<IEnumerable<AzureLog>>> Log_LS()
            {
                try
                {
                    return await internal_Log_LS();
                }
                catch (Exception ex)
                {
                    return new StreamingValueResult<IEnumerable<AzureLog>>(ex);
                }
            }

            public async Task<StreamingValueResult<IEnumerable<AzureLog>>> Log_LS_ByVM(int idVirtualMachine)
            {
                try
                {
                    return await internal_Log_LS_ByVM(idVirtualMachine);
                }
                catch (Exception ex)
                {
                    return new StreamingValueResult<IEnumerable<AzureLog>>(ex);
                }
            }

            public async Task<StreamingValueResult<IEnumerable<AzureLog>>> Log_LS_ByDate(DateTime? minDate, DateTime? maxDate)
            {
                try
                {
                    return await internal_Log_LS_ByDate(minDate, maxDate);
                }
                catch (Exception ex)
                {
                    return new StreamingValueResult<IEnumerable<AzureLog>>(ex);
                }
            }

            private async Task<StreamingResult> internal_Log_I(int idUser, string message)
            {
                try
                {
                    using (SqlConnection dbConn = new SqlConnection(_connectionString))
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "streaming_stp_Log_I";
                        dbCommand.Connection = dbConn;

                        dbCommand.Parameters.AddWithValue("@idUser", idUser);
                        dbCommand.Parameters.AddWithValue("@message", message);

                        await dbConn.OpenAsync();

                        await dbCommand.ExecuteScalarAsync();
                    }

                    return new StreamingResult(true);
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return new StreamingResult(ex);
                }
            }

            private async Task<StreamingResult> internal_Log_WithVM_I(int idUser, string message, int idVm, AzureLogAction action)
            {
                try
                {
                    using (SqlConnection dbConn = new SqlConnection(_connectionString))
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "streaming_stp_Log_WithVM_I";
                        dbCommand.Connection = dbConn;

                        dbCommand.Parameters.AddWithValue("@idUser", idUser);
                        dbCommand.Parameters.AddWithValue("@message", message);
                        dbCommand.Parameters.AddWithValue("@idVirtualMachine", idVm);
                        dbCommand.Parameters.AddWithValue("@action", AzureLogActionToString(action));

                        await dbConn.OpenAsync();

                        await dbCommand.ExecuteScalarAsync();
                    }

                    return new StreamingResult(true);
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return new StreamingResult(ex);
                }
            }

            private async Task<StreamingValueResult<IEnumerable<AzureLog>>> internal_Log_LS()
            {
                try
                {
                    using (SqlConnection dbConn = new SqlConnection(_connectionString))
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "streaming_stp_Log_LS";
                        dbCommand.Connection = dbConn;

                        await dbConn.OpenAsync();

                        var reader = await dbCommand.ExecuteReaderAsync();

                        var res = new List<AzureLog>();

                        while (await reader.ReadAsync())
                        {
                            res.Add(ReadAzureLog(reader));
                        }

                        return new StreamingValueResult<IEnumerable<AzureLog>>(res);
                    }
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return new StreamingValueResult<IEnumerable<AzureLog>>(ex);
                }
            }

            private async Task<StreamingValueResult<IEnumerable<AzureLog>>> internal_Log_LS_ByDate(DateTime? minDate, DateTime? maxDate)
            {
                try
                {
                    using (SqlConnection dbConn = new SqlConnection(_connectionString))
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "streaming_stp_Log_LS_ByDate";
                        dbCommand.Connection = dbConn;

                        dbCommand.Parameters.AddWithValue("@minDate", minDate);
                        dbCommand.Parameters.AddWithValue("@maxDate", maxDate);

                        await dbConn.OpenAsync();

                        var reader = await dbCommand.ExecuteReaderAsync();

                        var res = new List<AzureLog>();

                        while (await reader.ReadAsync())
                        {
                            res.Add(ReadAzureLog(reader));
                        }

                        return new StreamingValueResult<IEnumerable<AzureLog>>(res);
                    }
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return new StreamingValueResult<IEnumerable<AzureLog>>(ex);
                }
            }

            private async Task<StreamingValueResult<IEnumerable<AzureLog>>> internal_Log_LS_ByVM(int idVirtualMachine)
            {
                try
                {
                    using (SqlConnection dbConn = new SqlConnection(_connectionString))
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        dbCommand.CommandText = "streaming_stp_Log_LS_ByVM";
                        dbCommand.Connection = dbConn;

                        dbCommand.Parameters.AddWithValue("@idVirtualMachine", idVirtualMachine);

                        await dbConn.OpenAsync();

                        var reader = await dbCommand.ExecuteReaderAsync();

                        var res = new List<AzureLog>();

                        while (await reader.ReadAsync())
                        {
                            res.Add(ReadAzureLog(reader));
                        }

                        return new StreamingValueResult<IEnumerable<AzureLog>>(res);
                    }
                }
                catch (Exception ex)
                {
                    //TODO: log this
                    return new StreamingValueResult<IEnumerable<AzureLog>>(ex);
                }
            }

            private AzureLog ReadAzureLog(SqlDataReader reader)
            {
                var res = new AzureLog()
                {
                    Id = GetSafeSqlValue<int>(reader, "Id"),
                    IdUser = GetSafeSqlValue<int>(reader, "IdUser"),
                    DateTime = GetSafeSqlValue<DateTime>(reader, "Date"),
                    Message = GetSafeSqlValue<string>(reader, "Message"),
                    IdVirtualMachine = GetSafeSqlValue<int>(reader, "IdVirtualMachine"),
                    VmName = GetSafeSqlValue<string>(reader, "VmName"),
                    Action = ParseAzureLogAction(GetSafeSqlValue<string>(reader, "Action")),

                    IUPI = GetSafeSqlValue<Guid>(reader, "IUPI"),
                    Username = GetSafeSqlValue<string>(reader, "Username"),
                    FullName = GetSafeSqlValue<string>(reader, "FullName")
                };

                return res;
            }

            private AzureLogAction ParseAzureLogAction(string str)
            {
                switch (str?.ToUpperInvariant())
                {
                    case TRIED_TO_START:
                        return AzureLogAction.TriedToStart;
                    case TRIED_TO_STOP:
                        return AzureLogAction.TriedToStop;
                    case STARTED:
                        return AzureLogAction.Started;
                    case STOPPED:
                        return AzureLogAction.Stopped;
                    case ERROR:
                        return AzureLogAction.Error;
                    case UNKNOWN:
                        return AzureLogAction.Unknown;
                }

                return AzureLogAction.Unknown;
            }

            private string AzureLogActionToString(AzureLogAction ala)
            {
                switch (ala)
                {
                    case AzureLogAction.TriedToStart:
                        return TRIED_TO_START;
                    case AzureLogAction.TriedToStop:
                        return TRIED_TO_STOP;
                    case AzureLogAction.Started:
                        return STARTED;
                    case AzureLogAction.Stopped:
                        return STOPPED;
                    case AzureLogAction.Error:
                        return ERROR;
                    case AzureLogAction.Unknown:
                        return UNKNOWN;
                }

                return "";
            }
        }
    }
}
