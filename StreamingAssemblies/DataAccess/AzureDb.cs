﻿using StreamingAssemblies.Helpers.Azure;
using StreamingAssemblies.Models.Azure;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace StreamingAssemblies.DataAccess
{
    /// <summary>
    /// Defines the <see cref="AzureDb" />.
    /// </summary>
    public class AzureDb : BaseDataAccess
    {
        /// <summary>
        /// Defines the __min_index.
        /// </summary>
        private const int __min_index = 1;

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureDb"/> class.
        /// </summary>
        /// <param name="azureConnectionString">The azureConnectionString<see cref="string"/>.</param>
        public AzureDb(string azureConnectionString) : base(azureConnectionString)
        {
        }

        /// <summary>
        /// The AddVirtualMachineAsync.
        /// </summary>
        /// <param name="vm">The vm<see cref="AzureVirtualMachine"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{AzureVirtualMachine}}"/>.</returns>
        public async Task<StreamingValueResult<AzureVirtualMachine>> AddVirtualMachineAsync(AzureVirtualMachine vm, string userPerformingAction)
        {
            try
            {
                if (vm == null)
                {
                    throw new ArgumentNullException(nameof(vm));
                }
                if (string.IsNullOrWhiteSpace(userPerformingAction))
                {
                    throw new ArgumentNullException(nameof(userPerformingAction));
                }

                var toEdit = vm.Clone();

                toEdit.Id = null;

                return await internal_nsi_stp_Azure_VirtualMachines_IU(vm, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureVirtualMachine>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> DeleteVirtualMachineAsync(int id, string userPerformingAction)
        {
            try
            {
                if (id < __min_index)
                {
                    throw new ArgumentException($"Parameter {nameof(id)} out of range. Value: {id} | Min: {__min_index}", nameof(id));
                }
                if (string.IsNullOrWhiteSpace(userPerformingAction))
                {
                    throw new ArgumentNullException(nameof(userPerformingAction));
                }

                return await internal_nsi_stp_Azure_VirtualMachines_D(id, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="vm">The vm<see cref="AzureVirtualMachine"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> DeleteVirtualMachineAsync(AzureVirtualMachine vm, string userPerformingAction)
        {
            try
            {
                if (vm == null)
                {
                    throw new ArgumentNullException(nameof(vm));
                }
                if (!vm.Id.HasValue)
                {
                    throw new ArgumentNullException(nameof(AzureVirtualMachine.Id));
                }

                return await DeleteVirtualMachineAsync(vm.Id.Value, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// Edits a virtual machine. If the <see cref="AzureVirtualMachine.Id"/> is null, inserts a new entry.
        /// </summary>
        /// <param name="vm">The vm<see cref="AzureVirtualMachine"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{AzureVirtualMachine}}"/>.</returns>
        public async Task<StreamingValueResult<AzureVirtualMachine>> EditVirtualMachineAsync(AzureVirtualMachine vm, string userPerformingAction)
        {
            try
            {
                if (vm == null)
                {
                    throw new ArgumentNullException(nameof(vm));
                }
                if (string.IsNullOrWhiteSpace(userPerformingAction))
                {
                    throw new ArgumentNullException(nameof(userPerformingAction));
                }

                return await internal_nsi_stp_Azure_VirtualMachines_IU(vm, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureVirtualMachine>(ex);
            }
        }

        /// <summary>
        /// The GetAllVirtualMachines.
        /// </summary>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="StreamingValueResult{IEnumerable{AzureVirtualMachine}}"/>.</returns>
        public async Task<StreamingValueResult<IEnumerable<AzureVirtualMachine>>> GetAllActiveVirtualMachinesAsync(bool? includeDeleted = null)
        {
            try
            {
                return await internal_nsi_stp_Azure_VirtualMachines_LS_Active(includeDeleted);
            }
            catch (Exception ex)
            {
                //TODO: log this
                //shouldn't really enter here, but this way it's already done if anything changes either in the internal method or on the parameters this one accepts
                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(ex);
            }
        }

        /// <summary>
        /// The GetAllVirtualMachines.
        /// </summary>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="StreamingValueResult{IEnumerable{AzureVirtualMachine}}"/>.</returns>
        public async Task<StreamingValueResult<IEnumerable<AzureVirtualMachine>>> GetAllInactiveVirtualMachinesAsync(bool? includeDeleted = null)
        {
            try
            {
                return await internal_nsi_stp_Azure_VirtualMachines_LS_Inactive(includeDeleted);
            }
            catch (Exception ex)
            {
                //TODO: log this
                //shouldn't really enter here, but this way it's already done if anything changes either in the internal method or on the parameters this one accepts
                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(ex);
            }
        }

        /// <summary>
        /// The GetAllVirtualMachines.
        /// </summary>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="StreamingValueResult{IEnumerable{AzureVirtualMachine}}"/>.</returns>
        public async Task<StreamingValueResult<IEnumerable<AzureVirtualMachine>>> GetAllVirtualMachinesAsync(bool? includeDeleted = null)
        {
            try
            {
                return await internal_nsi_stp_Azure_VirtualMachines_LS(includeDeleted);
            }
            catch (Exception ex)
            {
                //TODO: log this
                //shouldn't really enter here, but this way it's already done if anything changes either in the internal method or on the parameters this one accepts
                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(ex);
            }
        }

        /// <summary>
        /// The GetVirtualMachine.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="StreamingValueResult{AzureVirtualMachine}"/>.</returns>
        public async Task<StreamingValueResult<AzureVirtualMachine>> GetVirtualMachineAsync(int id, bool? includeDeleted = null)
        {
            try
            {
                if (id < __min_index)
                {
                    throw new ArgumentException($"Parameter {nameof(id)} out of range. Value: {id} | Min: {__min_index}", nameof(id));
                }

                return await internal_nsi_stp_Azure_VirtualMachines_S(id, includeDeleted);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureVirtualMachine>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> RestoreDeletedVirtualMachineAsync(int id, string userPerformingAction)
        {
            try
            {
                if (id < __min_index)
                {
                    throw new ArgumentException($"Parameter {nameof(id)} out of range. Value: {id} | Min: {__min_index}", nameof(id));
                }
                if (string.IsNullOrWhiteSpace(userPerformingAction))
                {
                    throw new ArgumentNullException(nameof(userPerformingAction));
                }

                return await internal_nsi_stp_Azure_VirtualMachines_R(id, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="vm">The vm<see cref="AzureVirtualMachine"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> RestoreDeletedVirtualMachineAsync(AzureVirtualMachine vm, string userPerformingAction)
        {
            try
            {
                if (vm == null)
                {
                    throw new ArgumentNullException(nameof(vm));
                }
                if (!vm.Id.HasValue)
                {
                    throw new ArgumentNullException(nameof(AzureVirtualMachine.Id));
                }

                return await RestoreDeletedVirtualMachineAsync(vm.Id.Value, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> SetVirtualMachineAsActiveAsync(int id, string userPerformingAction)
        {
            try
            {
                if (id < __min_index)
                {
                    throw new ArgumentException($"Parameter {nameof(id)} out of range. Value: {id} | Min: {__min_index}", nameof(id));
                }
                if (string.IsNullOrWhiteSpace(userPerformingAction))
                {
                    throw new ArgumentNullException(nameof(userPerformingAction));
                }

                return await internal_nsi_stp_Azure_VirtualMachines_SetActive(id, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="vm">The vm<see cref="AzureVirtualMachine"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> SetVirtualMachineAsActiveAsync(AzureVirtualMachine vm, string userPerformingAction)
        {
            try
            {
                if (vm == null)
                {
                    throw new ArgumentNullException(nameof(vm));
                }
                if (!vm.Id.HasValue)
                {
                    throw new ArgumentNullException(nameof(AzureVirtualMachine.Id));
                }

                return await SetVirtualMachineAsActiveAsync(vm.Id.Value, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> SetVirtualMachineAsInactiveAsync(int id, string userPerformingAction)
        {
            try
            {
                if (id < __min_index)
                {
                    throw new ArgumentException($"Parameter {nameof(id)} out of range. Value: {id} | Min: {__min_index}", nameof(id));
                }
                if (string.IsNullOrWhiteSpace(userPerformingAction))
                {
                    throw new ArgumentNullException(nameof(userPerformingAction));
                }

                return await internal_nsi_stp_Azure_VirtualMachines_SetInactive(id, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The DeleteVirtualMachineAsync.
        /// </summary>
        /// <param name="vm">The vm<see cref="AzureVirtualMachine"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        public async Task<StreamingValueResult<bool>> SetVirtualMachineAsInactiveAsync(AzureVirtualMachine vm, string userPerformingAction)
        {
            try
            {
                if (vm == null)
                {
                    throw new ArgumentNullException(nameof(vm));
                }
                if (!vm.Id.HasValue)
                {
                    throw new ArgumentNullException(nameof(AzureVirtualMachine.Id));
                }

                return await SetVirtualMachineAsInactiveAsync(vm.Id.Value, userPerformingAction);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_D.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        private async Task<StreamingValueResult<bool>> internal_nsi_stp_Azure_VirtualMachines_D(int id, string userPerformingAction)
        {
            try
            {
                bool res = false;

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_D";
                    dbCommand.Connection = dbConn;

                    dbCommand.Parameters.AddWithValue("@id", id);
                    dbCommand.Parameters.AddWithValue("@userPerformingAction", userPerformingAction);

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            res = GetSafeSqlValue<bool>(dbReader, "Result");
                        }
                    }
                }

                return new StreamingValueResult<bool>(res);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// Calls stored procedure internal_nsi_stp_Azure_VirtualMachines_IU without verifying parameters.
        /// </summary>
        /// <param name="vm">The vm<see cref="AzureVirtualMachine"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{AzureVirtualMachine}}"/>.</returns>
        private async Task<StreamingValueResult<AzureVirtualMachine>> internal_nsi_stp_Azure_VirtualMachines_IU(AzureVirtualMachine vm, string userPerformingAction)
        {
            try
            {
                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_IU";
                    dbCommand.Connection = dbConn;

                    if (vm.Id.HasValue)
                    {
                        dbCommand.Parameters.AddWithValue("@id", vm.Id.Value);
                    }
                    dbCommand.Parameters.AddWithValue("@vmId", vm.VmId);
                    dbCommand.Parameters.AddWithValue("@vmName", vm.VmName);
                    dbCommand.Parameters.AddWithValue("@alias", vm.Alias);
                    dbCommand.Parameters.AddWithValue("@vmAzureUrl", vm.VmAzureUrl);
                    dbCommand.Parameters.AddWithValue("@configFilePath", vm.ConfigFilePath);
                    dbCommand.Parameters.AddWithValue("@pluginUrl", vm.PluginUrl);
                    dbCommand.Parameters.AddWithValue("@userPerformingAction", userPerformingAction);

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            vm = new AzureVirtualMachine();

                            vm.Id = GetSafeSqlValue<int>(dbReader, "Id");
                            vm.VmId = GetSafeSqlValue<string>(dbReader, "VmId");
                            vm.VmName = GetSafeSqlValue<string>(dbReader, "VmName");
                            vm.Alias = GetSafeSqlValue<string>(dbReader, "Alias");
                            vm.VmAzureUrl = GetSafeSqlValue<string>(dbReader, "VmAzureUrl");
                            vm.ConfigFilePath = GetSafeSqlValue<string>(dbReader, "ConfigFilePath");
                            vm.PluginUrl = GetSafeSqlValue<string>(dbReader, "PluginUrl");
                            vm.IsActive = GetSafeSqlValue<bool>(dbReader, "IsActive");
                            vm.IsDeleted = GetSafeSqlValue<bool>(dbReader, "IsDeleted");
                            vm.CreatedDate = GetSafeSqlValue<DateTime>(dbReader, "CreatedDate");
                            vm.CreatedBy = GetSafeSqlValue<string>(dbReader, "CreatedBy");
                            vm.ModifiedDate = GetSafeSqlValue<DateTime?>(dbReader, "ModifiedDate");
                            vm.ModifiedBy = GetSafeSqlValue<string>(dbReader, "ModifiedBy");

                            return new StreamingValueResult<AzureVirtualMachine>(vm);
                        }
                    }
                }

                //didn't find the entry, return not success
                return new StreamingValueResult<AzureVirtualMachine>(false, vm);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureVirtualMachine>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_LS.
        /// </summary>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{IEnumerable{AzureVirtualMachine}}}"/>.</returns>
        private async Task<StreamingValueResult<IEnumerable<AzureVirtualMachine>>> internal_nsi_stp_Azure_VirtualMachines_LS(bool? includeDeleted = null)
        {
            try
            {
                List<AzureVirtualMachine> vmList = new List<AzureVirtualMachine>();

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_LS";
                    dbCommand.Connection = dbConn;

                    if (includeDeleted.HasValue)
                    {
                        dbCommand.Parameters.AddWithValue("@includeDeleted", includeDeleted.Value);
                    }

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            AzureVirtualMachine vm = new AzureVirtualMachine();

                            vm.Id = GetSafeSqlValue<int>(dbReader, "Id");
                            vm.VmId = GetSafeSqlValue<string>(dbReader, "VmId");
                            vm.VmName = GetSafeSqlValue<string>(dbReader, "VmName");
                            vm.Alias = GetSafeSqlValue<string>(dbReader, "Alias");
                            vm.VmAzureUrl = GetSafeSqlValue<string>(dbReader, "VmAzureUrl");
                            vm.ConfigFilePath = GetSafeSqlValue<string>(dbReader, "ConfigFilePath");
                            vm.PluginUrl = GetSafeSqlValue<string>(dbReader, "PluginUrl");
                            vm.IsActive = GetSafeSqlValue<bool>(dbReader, "IsActive");
                            vm.IsDeleted = GetSafeSqlValue<bool>(dbReader, "IsDeleted");
                            vm.CreatedDate = GetSafeSqlValue<DateTime>(dbReader, "CreatedDate");
                            vm.CreatedBy = GetSafeSqlValue<string>(dbReader, "CreatedBy");
                            vm.ModifiedDate = GetSafeSqlValue<DateTime?>(dbReader, "ModifiedDate");
                            vm.ModifiedBy = GetSafeSqlValue<string>(dbReader, "ModifiedBy");

                            vmList.Add(vm);
                        }
                    }
                }

                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(vmList);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_LS_Active.
        /// </summary>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{IEnumerable{AzureVirtualMachine}}}"/>.</returns>
        private async Task<StreamingValueResult<IEnumerable<AzureVirtualMachine>>> internal_nsi_stp_Azure_VirtualMachines_LS_Active(bool? includeDeleted = null)
        {
            try
            {
                List<AzureVirtualMachine> vmList = new List<AzureVirtualMachine>();

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_LS_Active";
                    dbCommand.Connection = dbConn;

                    if (includeDeleted.HasValue)
                    {
                        dbCommand.Parameters.AddWithValue("@includeDeleted", includeDeleted.Value);
                    }

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            AzureVirtualMachine vm = new AzureVirtualMachine();

                            vm.Id = GetSafeSqlValue<int>(dbReader, "Id");
                            vm.VmId = GetSafeSqlValue<string>(dbReader, "VmId");
                            vm.VmName = GetSafeSqlValue<string>(dbReader, "VmName");
                            vm.Alias = GetSafeSqlValue<string>(dbReader, "Alias");
                            vm.VmAzureUrl = GetSafeSqlValue<string>(dbReader, "VmAzureUrl");
                            vm.ConfigFilePath = GetSafeSqlValue<string>(dbReader, "ConfigFilePath");
                            vm.PluginUrl = GetSafeSqlValue<string>(dbReader, "PluginUrl");
                            vm.IsActive = GetSafeSqlValue<bool>(dbReader, "IsActive");
                            vm.IsDeleted = GetSafeSqlValue<bool>(dbReader, "IsDeleted");
                            vm.CreatedDate = GetSafeSqlValue<DateTime>(dbReader, "CreatedDate");
                            vm.CreatedBy = GetSafeSqlValue<string>(dbReader, "CreatedBy");
                            vm.ModifiedDate = GetSafeSqlValue<DateTime?>(dbReader, "ModifiedDate");
                            vm.ModifiedBy = GetSafeSqlValue<string>(dbReader, "ModifiedBy");

                            vmList.Add(vm);
                        }
                    }
                }

                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(vmList);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_LS_Inactive.
        /// </summary>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{IEnumerable{AzureVirtualMachine}}}"/>.</returns>
        private async Task<StreamingValueResult<IEnumerable<AzureVirtualMachine>>> internal_nsi_stp_Azure_VirtualMachines_LS_Inactive(bool? includeDeleted = null)
        {
            try
            {
                List<AzureVirtualMachine> vmList = new List<AzureVirtualMachine>();

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_LS_Inactive";
                    dbCommand.Connection = dbConn;

                    if (includeDeleted.HasValue)
                    {
                        dbCommand.Parameters.AddWithValue("@includeDeleted", includeDeleted.Value);
                    }

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            AzureVirtualMachine vm = new AzureVirtualMachine();

                            vm.Id = GetSafeSqlValue<int>(dbReader, "Id");
                            vm.VmId = GetSafeSqlValue<string>(dbReader, "VmId");
                            vm.VmName = GetSafeSqlValue<string>(dbReader, "VmName");
                            vm.Alias = GetSafeSqlValue<string>(dbReader, "Alias");
                            vm.VmAzureUrl = GetSafeSqlValue<string>(dbReader, "VmAzureUrl");
                            vm.ConfigFilePath = GetSafeSqlValue<string>(dbReader, "ConfigFilePath");
                            vm.PluginUrl = GetSafeSqlValue<string>(dbReader, "PluginUrl");
                            vm.IsActive = GetSafeSqlValue<bool>(dbReader, "IsActive");
                            vm.IsDeleted = GetSafeSqlValue<bool>(dbReader, "IsDeleted");
                            vm.CreatedDate = GetSafeSqlValue<DateTime>(dbReader, "CreatedDate");
                            vm.CreatedBy = GetSafeSqlValue<string>(dbReader, "CreatedBy");
                            vm.ModifiedDate = GetSafeSqlValue<DateTime?>(dbReader, "ModifiedDate");
                            vm.ModifiedBy = GetSafeSqlValue<string>(dbReader, "ModifiedBy");

                            vmList.Add(vm);
                        }
                    }
                }

                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(vmList);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<IEnumerable<AzureVirtualMachine>>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_R.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        private async Task<StreamingValueResult<bool>> internal_nsi_stp_Azure_VirtualMachines_R(int id, string userPerformingAction)
        {
            try
            {
                bool res = false;

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_R";
                    dbCommand.Connection = dbConn;

                    dbCommand.Parameters.AddWithValue("@id", id);
                    dbCommand.Parameters.AddWithValue("@userPerformingAction", userPerformingAction);

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            res = GetSafeSqlValue<bool>(dbReader, "Result");
                        }
                    }
                }

                return new StreamingValueResult<bool>(res);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_S.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="includeDeleted">The includeDeleted<see cref="bool?"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{AzureVirtualMachine}}"/>.</returns>
        private async Task<StreamingValueResult<AzureVirtualMachine>> internal_nsi_stp_Azure_VirtualMachines_S(int id, bool? includeDeleted = null)
        {
            try
            {
                AzureVirtualMachine vm = null;

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_S";
                    dbCommand.Connection = dbConn;

                    dbCommand.Parameters.AddWithValue("@id", id);
                    if (includeDeleted.HasValue)
                    {
                        dbCommand.Parameters.AddWithValue("@includeDeleted", includeDeleted.Value);
                    }

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            vm = new AzureVirtualMachine();

                            vm.Id = GetSafeSqlValue<int>(dbReader, "Id");
                            vm.VmId = GetSafeSqlValue<string>(dbReader, "VmId");
                            vm.VmName = GetSafeSqlValue<string>(dbReader, "VmName");
                            vm.Alias = GetSafeSqlValue<string>(dbReader, "Alias");
                            vm.VmAzureUrl = GetSafeSqlValue<string>(dbReader, "VmAzureUrl");
                            vm.ConfigFilePath = GetSafeSqlValue<string>(dbReader, "ConfigFilePath");
                            vm.PluginUrl = GetSafeSqlValue<string>(dbReader, "PluginUrl");
                            vm.IsActive = GetSafeSqlValue<bool>(dbReader, "IsActive");
                            vm.IsDeleted = GetSafeSqlValue<bool>(dbReader, "IsDeleted");
                            vm.CreatedDate = GetSafeSqlValue<DateTime>(dbReader, "CreatedDate");
                            vm.CreatedBy = GetSafeSqlValue<string>(dbReader, "CreatedBy");
                            vm.ModifiedDate = GetSafeSqlValue<DateTime?>(dbReader, "ModifiedDate");
                            vm.ModifiedBy = GetSafeSqlValue<string>(dbReader, "ModifiedBy");

                            return new StreamingValueResult<AzureVirtualMachine>(vm);
                        }
                    }
                }

                //didn't find the entry, return not success
                return new StreamingValueResult<AzureVirtualMachine>(false, vm);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureVirtualMachine>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_SetActive.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        private async Task<StreamingValueResult<bool>> internal_nsi_stp_Azure_VirtualMachines_SetActive(int id, string userPerformingAction)
        {
            try
            {
                bool res = false;

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_SetActive";
                    dbCommand.Connection = dbConn;

                    dbCommand.Parameters.AddWithValue("@id", id);
                    dbCommand.Parameters.AddWithValue("@userPerformingAction", userPerformingAction);

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            res = GetSafeSqlValue<bool>(dbReader, "Result");
                        }
                    }
                }

                return new StreamingValueResult<bool>(res);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }

        /// <summary>
        /// The internal_nsi_stp_Azure_VirtualMachines_SetInactive.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="userPerformingAction">The userPerformingAction<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{bool}}"/>.</returns>
        private async Task<StreamingValueResult<bool>> internal_nsi_stp_Azure_VirtualMachines_SetInactive(int id, string userPerformingAction)
        {
            try
            {
                bool res = false;

                using (SqlConnection dbConn = new SqlConnection(_connectionString))
                using (SqlCommand dbCommand = new SqlCommand())
                {
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.CommandText = "nsi_stp_Azure_VirtualMachines_SetInactive";
                    dbCommand.Connection = dbConn;

                    dbCommand.Parameters.AddWithValue("@id", id);
                    dbCommand.Parameters.AddWithValue("@userPerformingAction", userPerformingAction);

                    await dbConn.OpenAsync();

                    using (SqlDataReader dbReader = await dbCommand.ExecuteReaderAsync())
                    {
                        while (await dbReader.ReadAsync())
                        {
                            res = GetSafeSqlValue<bool>(dbReader, "Result");
                        }
                    }
                }

                return new StreamingValueResult<bool>(res);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<bool>(ex);
            }
        }
    }
}
