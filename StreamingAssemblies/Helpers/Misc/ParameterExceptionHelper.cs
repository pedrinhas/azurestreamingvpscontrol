﻿using System;

namespace StreamingAssemblies.Helpers.Misc
{
    /// <summary>
    /// Defines the <see cref="ParameterExceptionHelper" />.
    /// </summary>
    public class ParameterExceptionHelper
    {
        /// <summary>
        /// The ThrowIfArgumentIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="string"/>.</param>
        /// <param name="argName">The argName<see cref="string"/>.</param>
        public static void ThrowIfArgumentIsNull(string value, string argName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("Value cannot be null, empty or whitespace", argName);
            }
        }

        /// <summary>
        /// The ThrowIfArgumentIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="object"/>.</param>
        /// <param name="argName">The argName<see cref="string"/>.</param>
        public static void ThrowIfArgumentIsNull(object value, string argName)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Value cannot be null", argName);
            }
        }

        /// <summary>
        /// The ThrowIfArgumentIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="object"/>.</param>
        /// <param name="argName">The argName<see cref="string"/>.</param>
        /// <param name="min">The min<see cref="int?"/>.</param>
        /// <param name="max">The max<see cref="int?"/>.</param>
        public static void ThrowIfArgumentIsNull(int? value, string argName, int? min = null, int? max = null)
        {
            if (!value.HasValue || min.HasValue && value < min || max.HasValue && max < value)
            {
                throw new ArgumentNullException($"Value outside of the range. {(min.HasValue ? $"Min: {min.Value}" : "")}{(min.HasValue && max.HasValue ? " | " : "")}{(max.HasValue ? $"Max: {max.Value}" : "")} | Value: {value}", argName);
            }
        }

        /// <summary>
        /// The ThrowIfArgumentIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="string"/>.</param>
        public static void ThrowIfArgumentIsNull(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("Value cannot be null, empty or whitespace");
            }
        }

        /// <summary>
        /// The ThrowIfArgumentIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="object"/>.</param>
        public static void ThrowIfArgumentIsNull(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Value cannot be null");
            }
        }

        /// <summary>
        /// The ThrowIfArgumentIsNull.
        /// </summary>
        /// <param name="value">The value<see cref="object"/>.</param>
        /// <param name="min">The min<see cref="int?"/>.</param>
        /// <param name="max">The max<see cref="int?"/>.</param>
        public static void ThrowIfArgumentIsNull(int? value, int? min = null, int? max = null)
        {
            if (!value.HasValue || min.HasValue && value < min || max.HasValue && max < value)
            {
                throw new ArgumentNullException($"Value outside of the range. {(min.HasValue ? $"Min: {min.Value}" : "")}{(min.HasValue && max.HasValue ? " | " : "")}{(max.HasValue ? $"Max: {max.Value}" : "")} | Value: {value}");
            }
        }
    }
}
