﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamingAssemblies.Helpers.Azure.Exceptions
{
    public class AzureResourceNotFoundException : Exception
    {
        public AzureResourceNotFoundException(string id) : this(null, null, null)
        {
        }

        public AzureResourceNotFoundException(string id, string message) : base(message)
        {

        }

        public AzureResourceNotFoundException(string id, string message, Exception ex) : base(message, ex)
        {
            Id = id;
        }

        public string Id { get; }
    }
}