﻿using StreamingAssemblies.Helpers.Misc;
using System;
using System.Collections.Generic;

namespace StreamingAssemblies.Helpers.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureClientStore" />.
    /// </summary>
    public static class AzureClientStore
    {
        /// <summary>
        /// Defines the _clients.
        /// </summary>
        private readonly static IDictionary<string, AzureClient> _clients = new Dictionary<string, AzureClient>();

        /// <summary>
        /// The AddAzureClient.
        /// </summary>
        /// <param name="configFilePath">The configFilePath<see cref="string"/>.</param>
        /// <param name="overwrite">The overwrite<see cref="bool"/>.</param>
        /// <returns>The <see cref="StreamingResult"/>.</returns>
        public static StreamingValueResult<AzureClient> AddAzureClient(string configFilePath)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(configFilePath, nameof(configFilePath));

                var newClient = new AzureClient(configFilePath);

                return AddAzureClient(newClient);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureClient>(ex);
            }
        }

        /// <summary>
        /// The AddAzureClient.
        /// </summary>
        /// <param name="client">The client<see cref="AzureClient"/>.</param>
        /// <param name="overwrite">The overwrite<see cref="bool"/>.</param>
        /// <returns>The <see cref="StreamingResult"/>.</returns>
        public static StreamingValueResult<AzureClient> AddAzureClient(AzureClient client)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(client.ConfigFilePath, nameof(AzureClient.ConfigFilePath));
                ParameterExceptionHelper.ThrowIfArgumentIsNull(client, nameof(client));

                if (_clients.ContainsKey(client.ConfigFilePath))
                {
                    return new StreamingValueResult<AzureClient>(_clients[client.ConfigFilePath]);
                }

                _clients.Add(client.ConfigFilePath, client);

                return new StreamingValueResult<AzureClient>(client);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureClient>(ex);
            }
        }

        /// <summary>
        /// The RemoveAzureClient.
        /// </summary>
        /// <param name="configFilePath">The id<see cref="string"/>.</param>
        /// <returns>The <see cref="StreamingResult"/>.</returns>
        public static StreamingResult RemoveAzureClient(string configFilePath)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(configFilePath, nameof(configFilePath));

                _clients.Remove(configFilePath);

                return new StreamingResult(true);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingResult(ex);
            }
        }

        /// <summary>
        /// The GetClient.
        /// </summary>
        /// <param name="configFilePath">The id<see cref="string"/>.</param>
        /// <returns>The <see cref="StreamingValueResult{AzureClient}"/>.</returns>
        public static StreamingValueResult<AzureClient> GetClient(string configFilePath)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(configFilePath, nameof(configFilePath));

                if (!_clients.ContainsKey(configFilePath))
                {
                    return AddAzureClient(configFilePath);
                }

                return new StreamingValueResult<AzureClient>(_clients[configFilePath]);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureClient>(ex);
            }
        }
    }
}
