﻿using Microsoft.Azure.Management.Compute.Fluent;
using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using StreamingAssemblies.Helpers.Azure.Exceptions;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AzureFluent = Microsoft.Azure.Management.Fluent.Azure;

namespace StreamingAssemblies.Helpers.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureHelper" />
    /// </summary>
    public static class AzureHelper
    {
        public static string GetVmIdFromUrl(string vmUrl)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(vmUrl))
                {
                    return null;
                }

                var regex = new Regex("\\/subscriptions.*?(?:(?!/overview).)*");

                var match = regex.Match(vmUrl);

                if (match.Success)
                {
                    return match.Value;
                }
                else
                {
                    throw new ArgumentException($"Invalid value for {nameof(vmUrl)}: {vmUrl}");
                }
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }

        public static string GetSubscriptionIdFromUrl(string vmUrl)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(vmUrl))
                {
                    return null;
                }

                var regex = new Regex("\\/subscriptions\\/(.*?)\\/");

                var match = regex.Match(vmUrl);

                if (match.Success && match.Groups.Count == 2 && match.Groups[1].Success)
                {
                    return match.Groups[1].Value;
                }
                else
                {
                    throw new ArgumentException($"Invalid value for {nameof(vmUrl)}: {vmUrl}");
                }
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }
    }
}
