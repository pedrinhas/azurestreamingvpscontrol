﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingAssemblies.Helpers.Azure
{
    public abstract class AzureParameters
    {
        private const string _prefix = "Azure.";

        public static string MultiThread => $"{_prefix}{nameof(MultiThread)}";
        public static string AlwaysDeallocate => $"{_prefix}{nameof(AlwaysDeallocate)}";
    }
}
