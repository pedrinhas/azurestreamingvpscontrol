﻿namespace StreamingAssemblies.Helpers.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureHelperConfigurationOptions" />
    /// </summary>
    public class AzureHelperConfigurationOptions
    {
        /// <summary>
        /// Gets or sets the ClientId
        /// Gets the ClientId
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Gets or sets the SubscriptionId
        /// Gets the SubscriptionId
        /// </summary>
        public string SubscriptionId { get; set; }

        /// <summary>
        /// Gets or sets the TenantId
        /// Gets the TenantId
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Gets or sets the ClientKey
        /// Gets the ClientKey
        /// </summary>
        public string ClientKey { get; set; }

        /// <summary>
        /// Gets or sets the BaseURL
        /// Gets the BaseURL
        /// </summary>
        public string BaseURL { get; set; }

        /// <summary>
        /// Gets or sets the AuthURL
        /// Gets the AuthURL
        /// </summary>
        public string AuthURL { get; set; }

        /// <summary>
        /// Gets or sets the GraphURL
        /// Gets the GraphURL
        /// </summary>
        public string GraphURL { get; set; }

        /// <summary>
        /// Gets or sets the ManagementURL
        /// Gets the ManagementURL
        /// </summary>
        public string ManagementURL { get; set; }
    }
}
