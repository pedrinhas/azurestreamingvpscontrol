﻿using StreamingAssemblies.Helpers.Misc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace StreamingAssemblies.Helpers.Azure.FileManagement
{
    /// <summary>
    /// Defines the <see cref="AzureFileManager" />.
    /// </summary>
    public static class AzureFileManager
    {
        /// <summary>
        /// Defines the _folderPath.
        /// </summary>
        private static string _folderPath = "";

        /// <summary>
        /// Defines the _fileExtension.
        /// </summary>
        private static string _fileExtension = ".properties";

        /// <summary>
        /// Gets a value indicating whether Initialized.
        /// </summary>
        public static bool Initialized => !string.IsNullOrWhiteSpace(_folderPath);

        /// <summary>
        /// The Initialize.
        /// </summary>
        /// <param name="folderPath">The folderPath<see cref="string"/>.</param>
        public static void Initialize(string folderPath)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(folderPath, nameof(folderPath));

                _folderPath = folderPath;
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }

        public static async Task<StreamingValueResult<string>> SaveAzureFileAsync(AzureFile azureFile)
        {
            try
            {
                ThrowIfNotInitialized();
                ParameterExceptionHelper.ThrowIfArgumentIsNull(azureFile, nameof(azureFile));

                return await SaveAzureFileAsync(azureFile.SubscriptionId, azureFile.TenantId, azureFile.ApplicationId, azureFile.ApplicationSecret);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<string>(ex);
            }
        }

        /// <summary>
        /// The SaveAzureFileAsync.
        /// </summary>
        /// <param name="subscriptionId">The subscriptionId<see cref="string"/>.</param>
        /// <param name="directoryId">The directoryId<see cref="string"/>.</param>
        /// <param name="applicationId">The applicationId<see cref="string"/>.</param>
        /// <param name="applicationSecret">The applicationSecret<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{string}}"/>.</returns>
        public static async Task<StreamingValueResult<string>> SaveAzureFileAsync(string subscriptionId, string directoryId, string applicationId, string applicationSecret)
        {
            try
            {
                ThrowIfNotInitialized();
                ParameterExceptionHelper.ThrowIfArgumentIsNull(subscriptionId, nameof(subscriptionId));
                ParameterExceptionHelper.ThrowIfArgumentIsNull(directoryId, nameof(directoryId));
                ParameterExceptionHelper.ThrowIfArgumentIsNull(applicationId, nameof(applicationId));
                ParameterExceptionHelper.ThrowIfArgumentIsNull(applicationSecret, nameof(applicationSecret));

                return await SaveAzureFileInternalAsync(subscriptionId, directoryId, applicationId, applicationSecret);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<string>(ex);
            }
        }

        public static async Task<StreamingValueResult<AzureFile>> LoadAzureFileAsync(string filePath)
        {
            try
            {
                ThrowIfNotInitialized();
                ParameterExceptionHelper.ThrowIfArgumentIsNull(filePath, nameof(filePath));

                if (!File.Exists(filePath))
                {
                    throw new FileNotFoundException($"The file at {filePath} does not exists");
                }

                using (var file = new FileStream(filePath, FileMode.Open))
                using (var reader = new StreamReader(file))
                {
                    var fileContent = await reader.ReadToEndAsync();

                    var res = AzureFile.FromFileContent(fileContent);

                    return new StreamingValueResult<AzureFile>(res);
                }
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureFile>(ex);
            }
        }

        /// <summary>
        /// The DeleteAzureFileAsync.
        /// </summary>
        /// <param name="filePath">The filePath<see cref="string"/>.</param>
        /// <returns>The <see cref="StreamingResult"/>.</returns>
        public static StreamingResult DeleteAzureFile(string filePath)
        {
            try
            {
                ThrowIfNotInitialized();
                ParameterExceptionHelper.ThrowIfArgumentIsNull(filePath, nameof(filePath));

                return DeleteAzureFileInternal(filePath);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<string>(ex);
            }
        }

        /// <summary>
        /// The SaveAzureFileInternalAsync.
        /// </summary>
        /// <param name="subscriptionId">The subscriptionId<see cref="string"/>.</param>
        /// <param name="directoryId">The directoryId<see cref="string"/>.</param>
        /// <param name="applicationId">The applicationId<see cref="string"/>.</param>
        /// <param name="applicationSecret">The applicationSecret<see cref="string"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{string}}"/>.</returns>
        private static async Task<StreamingValueResult<string>> SaveAzureFileInternalAsync(string subscriptionId, string directoryId, string applicationId, string applicationSecret)
        {
            try
            {
                if (!Directory.Exists(_folderPath))
                {
                    Directory.CreateDirectory(_folderPath);
                }

                var path = "";

                subscriptionId = subscriptionId.Trim();
                directoryId = directoryId.Trim();
                applicationId = applicationId.Trim();
                applicationSecret = applicationSecret.Trim();

                var azureFile = new AzureFile()
                {
                    SubscriptionId = subscriptionId,
                    TenantId = directoryId,
                    ApplicationId = applicationId,
                    ApplicationSecret = applicationSecret
                };

                //guaranteed new file name
                while (File.Exists(path = Path.Combine(_folderPath, $"azure_{Guid.NewGuid().ToString().Replace('-', '_')}{_fileExtension}"))) ;

                using (var fileStream = new FileStream(path, FileMode.CreateNew))
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    await streamWriter.WriteAsync(azureFile.Serialize());
                }

                return new StreamingValueResult<string>(path);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<string>(ex);
            }
        }

        /// <summary>
        /// The DeleteAzureFileInternal.
        /// </summary>
        /// <param name="filePath">The filePath<see cref="string"/>.</param>
        /// <returns>The <see cref="StreamingResult"/>.</returns>
        private static StreamingResult DeleteAzureFileInternal(string filePath)
        {
            try
            {
                var trueFilePath = filePath;
                if (File.Exists(trueFilePath))
                {
                    File.Delete(trueFilePath);
                }
                else
                {
                    trueFilePath = Path.Combine(_folderPath, filePath);
                    if (File.Exists(trueFilePath))
                    {
                        File.Delete(trueFilePath);
                    }
                }

                return new StreamingResult(!File.Exists(trueFilePath));
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<string>(ex);
            }
        }

        /// <summary>
        /// The ThrowIfNotInitialized.
        /// </summary>
        private static void ThrowIfNotInitialized()
        {
            if (!Initialized)
            {
                throw new InvalidOperationException($"The {nameof(AzureFileManager)} helper was not initialized.");
            }
        }
    }
}
