﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace StreamingAssemblies.Helpers.Azure.FileManagement
{
    /// <summary>
    /// Defines the <see cref="AzureFile" />.
    /// </summary>
    public class AzureFile
    {
        /// <summary>
        /// Defines the SubscriptionIdLabel.
        /// </summary>
        public const string SubscriptionIdLabel = "subscription";

        /// <summary>
        /// Defines the ApplicationIdLabel.
        /// </summary>
        public const string ApplicationIdLabel = "client";

        /// <summary>
        /// Defines the ApplicationSecretLabel.
        /// </summary>
        public const string ApplicationSecretLabel = "key";

        /// <summary>
        /// Defines the TenantIdLabel.
        /// </summary>
        public const string TenantIdLabel = "tenant";

        /// <summary>
        /// Gets or sets the SubscriptionId.
        /// </summary>
        public string SubscriptionId { get; set; }

        /// <summary>
        /// Gets or sets the ApplicationId.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the ApplicationSecret.
        /// </summary>
        public string ApplicationSecret { get; set; }

        /// <summary>
        /// Gets or sets the TenantId.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// The Serialize.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        public string Serialize()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(SubscriptionId))
                {
                    throw new ArgumentNullException(nameof(SubscriptionId));
                }
                if (string.IsNullOrWhiteSpace(ApplicationId))
                {
                    throw new ArgumentNullException(nameof(ApplicationId));
                }
                if (string.IsNullOrWhiteSpace(ApplicationSecret))
                {
                    throw new ArgumentNullException(nameof(ApplicationSecret));
                }
                if (string.IsNullOrWhiteSpace(TenantId))
                {
                    throw new ArgumentNullException(nameof(TenantId));
                }

                var fileContent = new StringBuilder();

                fileContent.AppendLine($"{SubscriptionIdLabel}={SubscriptionId.Trim()}");
                fileContent.AppendLine($"{ApplicationIdLabel}={ApplicationId.Trim()}");
                fileContent.AppendLine($"{ApplicationSecretLabel}={ApplicationSecret.Trim()}");
                fileContent.AppendLine($"{TenantIdLabel}={TenantId.Trim()}");
                fileContent.AppendLine("managementURI=https://management.core.windows.net/");
                fileContent.AppendLine("baseURL=https://management.azure.com/");
                fileContent.AppendLine("authURL=https://login.windows.net/");
                fileContent.AppendLine("graphURL=https://graph.windows.net/");

                return fileContent.ToString();
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        public void DeserializeLine(string line)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    return;
                }

                var subscriptionRegexMatch = _regexForParameter(SubscriptionIdLabel).Match(line);
                if (subscriptionRegexMatch.Success && subscriptionRegexMatch.Groups.Count > 0)
                {
                    this.SubscriptionId = subscriptionRegexMatch.Groups[1].Value;
                    return;
                }

                var applicationIdRegexMatch = _regexForParameter(ApplicationIdLabel).Match(line);
                if (applicationIdRegexMatch.Success && applicationIdRegexMatch.Groups.Count > 0)
                {
                    this.ApplicationId = applicationIdRegexMatch.Groups[1].Value;
                    return;
                }

                var applicationSecretRegexMatch = _regexForParameter(ApplicationSecretLabel).Match(line);
                if (applicationSecretRegexMatch.Success && applicationSecretRegexMatch.Groups.Count > 0)
                {
                    this.ApplicationSecret = applicationSecretRegexMatch.Groups[1].Value;
                    return;
                }

                var tenantRegexMatch = _regexForParameter(TenantIdLabel).Match(line);
                if (tenantRegexMatch.Success && tenantRegexMatch.Groups.Count > 0)
                {
                    this.TenantId = tenantRegexMatch.Groups[1].Value;
                    return;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Deserialize(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return;
                }

                using (var sr = new StringReader(value))
                {
                    var line = "";

                    while ((line = sr.ReadLine()) != null)
                    {
                        DeserializeLine(line);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// The Deserialize.
        /// </summary>
        /// <param name="fileContent">The fileContent<see cref="string"/>.</param>
        /// <returns>The <see cref="AzureFile"/>.</returns>
        public static AzureFile FromFileContent(string fileContent)
        {
            try
            {
                var res = new AzureFile();

                res.Deserialize(fileContent);

                return res;
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }

        private Regex _regexForParameter(string parameter)
        {
            return new Regex($"^{parameter}=(.*)$", RegexOptions.Multiline);
        }
    }
}
