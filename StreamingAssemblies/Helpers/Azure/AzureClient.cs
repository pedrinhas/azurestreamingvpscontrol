﻿using Microsoft.Azure.Management.Compute.Fluent;
using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using StreamingAssemblies.Helpers.Azure.Exceptions;
using StreamingAssemblies.Models.Azure;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AzureFluent = Microsoft.Azure.Management.Fluent.Azure;

namespace StreamingAssemblies.Helpers.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureClient" />.
    /// </summary>
    public class AzureClient
    {
        /// <summary>
        /// Defines the _configFilePath.
        /// </summary>
        private string _configFilePath = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureClient"/> class.
        /// </summary>
        /// <param name="configFilePath">The configFilePath<see cref="string"/>.</param>
        public AzureClient(string configFilePath)
        {
            if (string.IsNullOrWhiteSpace(configFilePath))
            {
                throw new ArgumentNullException(nameof(configFilePath));
            }

            _configFilePath = configFilePath;

            _init(configFilePath);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureClient"/> class.
        /// </summary>
        /// <param name="options">The options<see cref="AzureHelperConfigurationOptions"/>.</param>
        [Obsolete("Don't use, currently not working", true)]
        public AzureClient(AzureHelperConfigurationOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            _init(options);
        }

        /// <summary>
        /// Gets a value indicating whether _initd.
        /// </summary>
        public bool _initd => AzureInstance != null;

        /// <summary>
        /// Gets the AzureInstance.
        /// </summary>
        public IAzure AzureInstance { get; private set; }

        /// <summary>
        /// Gets the ConfigFilePath.
        /// </summary>
        public string ConfigFilePath { get => _configFilePath; }

        /// <summary>
        /// The _initFromConfigFileEnvironmentVariable.
        /// </summary>
        /// <param name="envVarName">The envVarName<see cref="string"/>.</param>
        public void _initFromConfigFileEnvironmentVariable(string envVarName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(envVarName))
                {
                    throw new ArgumentNullException(nameof(envVarName));
                }

                var filePath = Environment.GetEnvironmentVariable(envVarName);

                if (string.IsNullOrWhiteSpace(filePath))
                {
                    throw new ArgumentException($"The environment variable {envVarName} is empty or does not exist", nameof(envVarName));
                }

                _init(filePath);
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }

        /// <summary>
        /// The GetAllVirtualMachinesAsync.
        /// </summary>
        /// <param name="cancellationToken">.</param>
        /// <returns>.</returns>
        public async Task<StreamingValueResult<IEnumerable<IVirtualMachine>>> GetAllVirtualMachinesAsync(CancellationToken cancellationToken = default)
        {
            try
            {
                var res = new List<IVirtualMachine>();

                var vmsPaged = await AzureInstance.VirtualMachines.ListAsync(true, cancellationToken);

                if (vmsPaged == null)
                {
                    throw new AzureResourceNotFoundException("", $"Não foram encontradas máquinas virtuais");
                }

                var enumerator = vmsPaged.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    res.Add(enumerator.Current);
                }

                return new StreamingValueResult<IEnumerable<IVirtualMachine>>(res);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<IEnumerable<IVirtualMachine>>(ex);
            }
        }

        /// <summary>
        /// The GetVirtualMachineAsAzureVirtualMachineAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{AzureVirtualMachine}}"/>.</returns>
        public async Task<StreamingValueResult<AzureVirtualMachine>> GetVirtualMachineAsAzureVirtualMachineAsync(string vmId, CancellationToken cancellationToken = default)
        {

            try
            {
                if (string.IsNullOrWhiteSpace(vmId))
                {
                    throw new ArgumentNullException(nameof(vmId));
                }

                var vm = await _getVirtualMachineInternalAsync(vmId, cancellationToken);

                if (!vm.Success)
                {
                    return new StreamingValueResult<AzureVirtualMachine>(vm.Exception);
                }

                var res = new AzureVirtualMachine(vm.Value);

                res.ConfigFilePath = ConfigFilePath;

                return new StreamingValueResult<AzureVirtualMachine>(res);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<AzureVirtualMachine>(ex);
            }
        }

        /// <summary>
        /// The GetVirtualMachineAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellation<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{IVirtualMachine}}"/>.</returns>
        public async Task<StreamingValueResult<IVirtualMachine>> GetVirtualMachineAsync(string vmId, CancellationToken cancellationToken = default)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(vmId))
                {
                    throw new ArgumentNullException(nameof(vmId));
                }

                return await _getVirtualMachineInternalAsync(vmId, cancellationToken);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<IVirtualMachine>(ex);
            }
        }
        /// <summary>
        /// The GetVirtualMachineStatusAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{PowerState}}"/>.</returns>
        public async Task<StreamingValueResult<PowerState>> GetVirtualMachineStatusAsync(string vmId, CancellationToken cancellationToken = default)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(vmId))
                {
                    throw new ArgumentNullException(nameof(vmId));
                }

                var vmResult = await _getVirtualMachineInternalAsync(vmId, cancellationToken);

                if (!vmResult.Success)
                {
                    return new StreamingValueResult<PowerState>(vmResult.Exception);
                }

                return new StreamingValueResult<PowerState>(vmResult.Value.PowerState);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<PowerState>(ex);
            }
        }

        /// <summary>
        /// The StartVirtualMachineAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        public async Task<StreamingResult> StartVirtualMachineAsync(string vmId, CancellationToken cancellationToken = default)
        {
            return await internal_StartVirtualMachineAsync(vmId, false, cancellationToken);
        }

        /// <summary>
        /// The StartVirtualMachineMultiThreadAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        public async Task<StreamingResult> StartVirtualMachineMultiThreadAsync(string vmId, CancellationToken cancellationToken = default)
        {
            return await internal_StartVirtualMachineAsync(vmId, true, cancellationToken);
        }

        /// <summary>
        /// The StopVirtualMachineAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        public async Task<StreamingResult> StopVirtualMachineAsync(string vmId, CancellationToken cancellationToken = default)
        {
            return await internal_StopVirtualMachineAsync(vmId, false, cancellationToken);
        }

        /// <summary>
        /// The StopVirtualMachineMultiThreadAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        public async Task<StreamingResult> StopVirtualMachineMultiThreadAsync(string vmId, CancellationToken cancellationToken = default)
        {
            return await internal_StopVirtualMachineAsync(vmId, true, cancellationToken);
        }

        /// <summary>
        /// The DeallocateVirtualMachineAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        public async Task<StreamingResult> DeallocateVirtualMachineAsync(string vmId, CancellationToken cancellationToken = default)
        {
            return await internal_DeallocateVirtualMachineAsync(vmId, false, cancellationToken);
        }

        /// <summary>
        /// The DeallocateVirtualMachineMultiThreadAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        public async Task<StreamingResult> DeallocateVirtualMachineMultiThreadAsync(string vmId, CancellationToken cancellationToken = default)
        {
            return await internal_DeallocateVirtualMachineAsync(vmId, true, cancellationToken);
        }

        /// <summary>
        /// The _getVirtualMachineInternalAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{StreamingValueResult{IVirtualMachine}}"/>.</returns>
        private async Task<StreamingValueResult<IVirtualMachine>> _getVirtualMachineInternalAsync(string vmId, CancellationToken cancellationToken = default)
        {
            try
            {
                var vm = await AzureInstance.VirtualMachines.GetByIdAsync(vmId, cancellationToken);

                if (vm == null)
                {
                    throw new AzureResourceNotFoundException(vmId, $"A máquina virtual com o id {vmId} não foi encontrada");
                }

                return new StreamingValueResult<IVirtualMachine>(vm);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingValueResult<IVirtualMachine>(ex);
            }
        }

        /// <summary>
        /// Configure the helper. By default it reads the options from the configuration file, but they can be passed manually.
        /// </summary>
        /// <param name="options">The options<see cref="AzureHelperConfigurationOptions"/>.</param>
        [Obsolete("Don't use, currently not working", true)]
        private void _init(AzureHelperConfigurationOptions options)
        {
            try
            {
                if (options == null)
                {
                    throw new ArgumentNullException(nameof(options));
                }

                var env = new AzureEnvironment()
                {
                    AuthenticationEndpoint = options.AuthURL,
                    GraphEndpoint = options.GraphURL,
                    ManagementEndpoint = options.ManagementURL,
                    ResourceManagerEndpoint = options.BaseURL
                };

                var credentials = SdkContext.AzureCredentialsFactory
                    .FromServicePrincipal(
                    clientId: options.ClientId,
                    clientSecret: options.ClientKey,
                    tenantId: options.TenantId,
                    environment: env
                    );

                AzureInstance = AzureFluent
                    .Configure()
                    .WithLogLevel(Microsoft.Azure.Management.ResourceManager.Fluent.Core.HttpLoggingDelegatingHandler.Level.Basic)
                    .Authenticate(credentials)
                    .WithDefaultSubscription();
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }

        /// <summary>
        /// The _init.
        /// </summary>
        /// <param name="configFilePath">The configFilePath<see cref="string"/>.</param>
        private void _init(string configFilePath)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(configFilePath))
                {
                    throw new ArgumentNullException(nameof(configFilePath));
                }
                var credentials = SdkContext.AzureCredentialsFactory.FromFile(configFilePath);

                AzureInstance = AzureFluent
                    .Configure()
                    .WithLogLevel(Microsoft.Azure.Management.ResourceManager.Fluent.Core.HttpLoggingDelegatingHandler.Level.Basic)
                    .Authenticate(credentials)
                    .WithDefaultSubscription();
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }

        /// <summary>
        /// The StartVirtualMachineInternalAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="multiThread">The multiThread<see cref="bool"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        private async Task<StreamingResult> internal_StartVirtualMachineAsync(string vmId, bool multiThread = false, CancellationToken cancellationToken = default)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(vmId))
                {
                    throw new ArgumentNullException(nameof(vmId));
                }

                var vmResult = await _getVirtualMachineInternalAsync(vmId, cancellationToken);

                if (!vmResult.Success)
                {
                    return new StreamingResult(vmResult.Exception);
                }

                Exception exception = null;

                var resetEvent = new AutoResetEvent(multiThread);

                var t = new Thread(async () =>
                {
                    try
                    {
                        await vmResult.Value.StartAsync(cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }

                    resetEvent.Set();
                });

                t.Start();

                resetEvent.WaitOne();

                if (exception != null)
                {
                    throw exception;
                }

                return new StreamingResult(true);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingResult(ex);
            }
        }

        /// <summary>
        /// The StopVirtualMachineInternalAsync.
        /// </summary>
        /// <param name="vmId">The vmId<see cref="string"/>.</param>
        /// <param name="multiThread">The multiThread<see cref="bool"/>.</param>
        /// <param name="cancellationToken">The cancellationToken<see cref="CancellationToken"/>.</param>
        /// <returns>The <see cref="Task{AzureResult}"/>.</returns>
        private async Task<StreamingResult> internal_StopVirtualMachineAsync(string vmId, bool multiThread = false, CancellationToken cancellationToken = default)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(vmId))
                {
                    throw new ArgumentNullException(nameof(vmId));
                }

                var vmResult = await _getVirtualMachineInternalAsync(vmId, cancellationToken);

                if (!vmResult.Success)
                {
                    return new StreamingResult(vmResult.Exception);
                }

                Exception exception = null;

                var resetEvent = new AutoResetEvent(multiThread);

                var t = new Thread(async () =>
                {
                    try
                    {
                        await vmResult.Value.PowerOffAsync(cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }

                    resetEvent.Set();
                });

                t.Start();

                resetEvent.WaitOne();

                if (exception != null)
                {
                    throw exception;
                }

                return new StreamingResult(true);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingResult(ex);
            }
        }

        private async Task<StreamingResult> internal_DeallocateVirtualMachineAsync(string vmId, bool multiThread = false, CancellationToken cancellationToken = default)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(vmId))
                {
                    throw new ArgumentNullException(nameof(vmId));
                }

                var vmResult = await _getVirtualMachineInternalAsync(vmId, cancellationToken);

                if (!vmResult.Success)
                {
                    return new StreamingResult(vmResult.Exception);
                }

                Exception exception = null;

                var resetEvent = new AutoResetEvent(multiThread);

                var t = new Thread(async () =>
                {
                    try
                    {
                        await vmResult.Value.DeallocateAsync(cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }

                    resetEvent.Set();
                });

                t.Start();

                resetEvent.WaitOne();

                if (exception != null)
                {
                    throw exception;
                }

                return new StreamingResult(true);
            }
            catch (Exception ex)
            {
                //TODO: log this
                return new StreamingResult(ex);
            }
        }

        public abstract class AzureErrorCodes
        {
            public const string ReadOnlyDisabledSubscription = nameof(ReadOnlyDisabledSubscription);
        }
    }
}
