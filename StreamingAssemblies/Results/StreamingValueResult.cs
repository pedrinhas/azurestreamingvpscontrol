﻿namespace StreamingAssemblies
{
    using System;

    /// <summary>
    /// Defines the <see cref="StreamingValueResult{T}" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StreamingValueResult<T> : StreamingResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StreamingValueResult{T}"/> class.
        /// </summary>
        /// <param name="value">The value<see cref="T"/></param>
        public StreamingValueResult(T value) : this(value != null, value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamingValueResult{T}"/> class.
        /// </summary>
        /// <param name="success">The success<see cref="bool"/></param>
        /// <param name="value">The value<see cref="T"/></param>
        public StreamingValueResult(bool success, T value) : base(success)
        {
            Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamingValueResult{T}"/> class.
        /// </summary>
        /// <param name="ex">The ex<see cref="Exception"/></param>
        public StreamingValueResult(Exception ex) : base(ex)
        {
        }

        /// <summary>
        /// Gets the Value
        /// </summary>
        public T Value { get; }

        public static implicit operator T(StreamingValueResult<T> avr) => avr.Value;
        public static explicit operator StreamingValueResult<T>(T someObj) => new StreamingValueResult<T>(someObj);
    }
}
