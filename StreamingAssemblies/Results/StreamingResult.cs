﻿namespace StreamingAssemblies
{
    using System;

    /// <summary>
    /// Defines the <see cref="StreamingResult" />
    /// </summary>
    public class StreamingResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StreamingResult"/> class.
        /// </summary>
        /// <param name="success">The success<see cref="bool"/></param>
        public StreamingResult(bool success)
        {
            Success = success;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamingResult"/> class.
        /// </summary>
        /// <param name="ex">The ex<see cref="Exception"/></param>
        public StreamingResult(Exception ex) : this(false)
        {
            Exception = ex;
        }

        /// <summary>
        /// Gets a value indicating whether Success
        /// Gets or sets a value indicating whether Success
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Gets the Exception
        /// Gets or sets the Exception
        /// </summary>
        public Exception Exception { get; private set; }

        public static implicit operator bool(StreamingResult ar) => ar.Success;
        public static explicit operator StreamingResult(bool b) => new StreamingResult(b);
    }
}
