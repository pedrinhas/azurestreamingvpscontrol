using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingAssemblies.Models.QuoVadis.Aplicacoes
{
    public class AplicacaoPortalBase
    {
        public string IdentificadorPortalBase { get; set; }
        public string SiglaPortalBase { get; set; }
        public string CorPortalBase { get; set; }
        public string GruposMenuPortalBase { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Link { get; set; }

        public AplicacaoPortalBase()
        {
            IdentificadorPortalBase = "";
            SiglaPortalBase = "";
            CorPortalBase = "";
            GruposMenuPortalBase = "";
            Nome = "";
            Descricao = "";
            Link = "";
        }
    }
}
