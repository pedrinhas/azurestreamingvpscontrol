using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingAssemblies.Models.QuoVadis.RCU
{
    public class EstudanteNomeAndIUPI
    {
        public string Nome { get; set; }
        public Guid IUPI { get; set; }

        public EstudanteNomeAndIUPI()
        {
            Nome = "";
            IUPI = new Guid();
        }
    }
}
