﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingAssemblies.Models.Logs
{
    public class StreamingLog
    {
        public StreamingLog(StreamingResult result, string iupi) : this(ToSafeException(result), iupi)
        {
        }

        public StreamingLog(StreamingResult result, Guid iupi) : this(ToSafeException(result), iupi)
        {

        }

        public StreamingLog(Exception exception, string iupi) : this(exception, ToSafeGuid(iupi))
        {
        }

        public StreamingLog(Exception exception, Guid iupi)
        {
            this.Exception = exception;
            this.IUPI = iupi;
        }

        private static Exception ToSafeException(StreamingResult result)
        {
            return result?.Exception;
        }

        private static Guid ToSafeGuid(string guid)
        {
            var res = Guid.Empty;

            try
            {
                if (string.IsNullOrWhiteSpace(guid))
                {
                    throw new ArgumentNullException(nameof(guid));
                }

                if (!Guid.TryParse(guid, out res))
                {
                    //consistency, this if and throw are kinda unnecessary
                    throw new ArgumentException("Invalid input", nameof(guid));
                }
            }
            catch (Exception)
            {
            }

            return res;
        }

        public Exception Exception { get; set; }

        public Guid IUPI { get; set; }

        public string Type => Exception?.GetType().FullName;
        public string StackTrace => Exception?.StackTrace;
        public string Message => Exception?.Message;
    }
}
