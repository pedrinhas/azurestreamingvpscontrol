using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StreamingAssemblies.Models.Streaming;
using StreamingAssemblies.Models.QuoVadis.Aplicacoes;
using StreamingAssemblies.Models.QuoVadis.Cargos;
using StreamingAssemblies.Models.QuoVadis.PortalBase;

namespace StreamingAssemblies.Models
{
    public class Session
    {
        public int IdUser { get; set; }
        public string NMec { get; set; }
        public Guid IUPI { get; set; }
        public string Username { get; set; }
        public string Nome { get; set; }
        public string FotoHTMLSrc { get; set; }

        public string CodigoCurso { get; set; }
        public string Regime { get; set; }

        public List<Cargo> Cargos { get; set; }
        public List<SessionPerfil> Perfis { get; set; }

        public List<Grupo> Grupos { get; set; }
        public List<Autorizacao> Autorizacoes { get; set; }

        public List<Linguagem> Linguagens { get; set; }
        public Linguagem SelectedLinguagem { get; set; }

        public AplicacaoPortalBase AplicacaoPortalBase { get; set; }

        public List<MenuItem> MenuLateral { get; set; }
        public List<MenuItem> AplicacoesPortalBaseMenuLateral { get; set; }

        public Footer Footer { get; set; }

        public Session()
        {
            IdUser = 0;
            NMec = "";
            IUPI = new Guid();
            Username = "";
            Nome = "";
            FotoHTMLSrc = "";

            CodigoCurso = "";
            Regime = "";

            Cargos = new List<Cargo>();
            Perfis = new List<SessionPerfil>();

            Grupos = new List<Grupo>();
            Autorizacoes = new List<Autorizacao>();

            Linguagens = new List<Linguagem>();
            SelectedLinguagem = new Linguagem();

            AplicacaoPortalBase = new AplicacaoPortalBase();

            MenuLateral = new List<MenuItem>();
            AplicacoesPortalBaseMenuLateral = new List<MenuItem>();

            Footer = new Footer();
        }
    }
}
