﻿using StreamingAssemblies.Helpers.Azure;
using System;
using System.Text.RegularExpressions;

namespace StreamingAssemblies.Models.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureInsertVmModel" />.
    /// </summary>
    public class AzureInsertVmModel : AzureInsertOrCreateVmModel
    {
    }
}
