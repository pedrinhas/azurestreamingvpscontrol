﻿namespace StreamingAssemblies.Models.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureStopDeleteVmModel" />.
    /// </summary>
    public class AzureStopDeleteVmModel
    {
        /// <summary>
        /// Gets or sets the VmName.
        /// </summary>
        public string VmName { get; set; }

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        public int Id { get; set; }
    }
}
