﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingAssemblies.Models.Azure.Logs
{
    public enum AzureLogAction
    {
        TriedToStart,
        TriedToStop,
        Started,
        Stopped,
        Error,
        Unknown
    }
}
