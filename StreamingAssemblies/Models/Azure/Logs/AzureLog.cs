﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingAssemblies.Models.Azure.Logs
{
    public class AzureLog
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public DateTime DateTime { get; set; }
        public string Message { get; set; }
        public int IdVirtualMachine { get; set; }
        public string VmName { get; set; }
        public AzureLogAction Action { get; set; } = AzureLogAction.Unknown;

        #region from joins

        public string Username { get; set; }
        public string FullName { get; set; }
        public Guid IUPI { get; set; }

        #endregion
    }
}
