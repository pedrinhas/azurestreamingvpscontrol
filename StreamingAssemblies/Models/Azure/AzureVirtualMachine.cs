﻿namespace StreamingAssemblies.Models.Azure
{
    using Microsoft.Azure.Management.Compute.Fluent;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="AzureVirtualMachine" />.
    /// </summary>
    public class AzureVirtualMachine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AzureVirtualMachine"/> class.
        /// </summary>
        public AzureVirtualMachine()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureVirtualMachine"/> class.
        /// </summary>
        /// <param name="vm">The vm<see cref="IVirtualMachine"/>.</param>
        public AzureVirtualMachine(IVirtualMachine vm)
        {
            if (vm != null)
            {
                VmId = vm.Id;
                VmName = vm.Name;
                PowerState = vm.PowerState;
            }
        }

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets the VmId.
        /// </summary>
        public string VmId { get; set; }

        /// <summary>
        /// Gets the EscapedId.
        /// </summary>
        public string DisplayName => string.IsNullOrWhiteSpace(Alias) ? VmName : Alias;

        /// <summary>
        /// Gets or sets the VmName.
        /// </summary>
        public string VmName { get; set; }

        public string Alias { get; set; }

        public string VmAzureUrl { get; set; }

        /// <summary>
        /// Gets or sets the ConfigFilePath.
        /// </summary>
        public string ConfigFilePath { get; set; }

        /// <summary>
        /// Gets or sets the PowerState.
        /// </summary>
        public PowerState PowerState { get; set; }

        /// <summary>
        /// Gets the PowerStateString.
        /// </summary>
        public string PowerStateString
        {
            get
            {
                if (PowerState == null)
                {
                    return "Unknown";
                }

                try
                {
                    var res = PowerState.Value.Split('/')[1];

                    return char.ToUpper(res[0]) + res.Substring(1);
                }
                catch (Exception ex)
                {
                    return "Unknown";
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsDeleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsActive.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy.
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy.
        /// </summary>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the PluginUrl.
        /// </summary>
        public string PluginUrl { get; set; }

        /// <summary>
        /// Gets a value indicating whether CanStart.
        /// </summary>
        public bool CanStart => PowerState == PowerState.Stopped || PowerState == PowerState.Deallocated;

        /// <summary>
        /// Gets a value indicating whether CanStop.
        /// </summary>
        public bool CanStop => PowerState == PowerState.Running || PowerState == PowerState.Starting;

        /// <summary>
        /// The FromIVirtualMachine.
        /// </summary>
        /// <param name="vm">The vm<see cref="IVirtualMachine"/>.</param>
        /// <returns>The <see cref="AzureVirtualMachine"/>.</returns>
        public static AzureVirtualMachine FromIVirtualMachine(IVirtualMachine vm)
        {
            if (vm == null)
            {
                throw new ArgumentNullException(nameof(vm));
            }

            return new AzureVirtualMachine(vm);
        }

        /// <summary>
        /// The FromIVirtualMachineCollection.
        /// </summary>
        /// <param name="vms">The vms<see cref="IEnumerable{IVirtualMachine}"/>.</param>
        /// <returns>The <see cref="IEnumerable{AzureVirtualMachine}"/>.</returns>
        public static IEnumerable<AzureVirtualMachine> FromIVirtualMachineCollection(IEnumerable<IVirtualMachine> vms)
        {
            if (vms == null)
            {
                throw new ArgumentNullException(nameof(vms));
            }

            foreach (var vm in vms)
            {
                yield return AzureVirtualMachine.FromIVirtualMachine(vm);
            }
        }

        /// <summary>
        /// The Clone.
        /// </summary>
        /// <returns>The <see cref="AzureVirtualMachine"/>.</returns>
        public AzureVirtualMachine Clone()
        {
            return new AzureVirtualMachine()
            {
                Alias = this.Alias,
                ConfigFilePath = this.ConfigFilePath,
                CreatedBy = this.CreatedBy,
                CreatedDate = this.CreatedDate,
                Id = this.Id,
                IsActive = this.IsActive,
                IsDeleted = this.IsDeleted,
                ModifiedBy = this.ModifiedBy,
                ModifiedDate = this.ModifiedDate,
                PluginUrl = this.PluginUrl,
                PowerState = this.PowerState,
                VmId = this.VmId,
                VmName = this.VmName
            };
        }

        /// <summary>
        /// The Clone.
        /// </summary>
        /// <param name="source">The source<see cref="AzureVirtualMachine"/>.</param>
        /// <returns>The <see cref="AzureVirtualMachine"/>.</returns>
        public static AzureVirtualMachine Clone(AzureVirtualMachine source) => source?.Clone();
    }
}
