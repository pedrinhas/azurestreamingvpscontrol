﻿using StreamingAssemblies.Helpers.Azure;
using StreamingAssemblies.Helpers.Azure.FileManagement;
using StreamingAssemblies.Helpers.Misc;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StreamingAssemblies.Models.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureUpdateVmModel" />.
    /// </summary>
    public class AzureUpdateVmModel : AzureInsertOrCreateVmModel
    {
        public int Id { get; set; }

        public static async Task<StreamingValueResult<AzureUpdateVmModel>> FromAzureVirtualMachineAsync(AzureVirtualMachine azureVm)
        {
            try
            {
                ParameterExceptionHelper.ThrowIfArgumentIsNull(azureVm);

                var res = new AzureUpdateVmModel()
                {
                    Id = azureVm.Id.Value,
                    Alias = azureVm.Alias,
                    AzureVmUrl = azureVm.VmAzureUrl,
                    PluginUrl = azureVm.PluginUrl
                };

                var configFileResult = await AzureFileManager.LoadAzureFileAsync(azureVm.ConfigFilePath);

                if (configFileResult.Success)
                {
                    res.TenantId = configFileResult.Value.TenantId;
                    res.ApplicationId = configFileResult.Value.ApplicationId;
                    res.ApplicationSecret = configFileResult.Value.ApplicationSecret;
                }
                else
                {
                    return new StreamingValueResult<AzureUpdateVmModel>(configFileResult.Exception);
                }

                return new StreamingValueResult<AzureUpdateVmModel>(res);
            }
            catch (Exception)
            {
                //TODO: log this
                throw;
            }
        }
    }
}
