﻿using StreamingAssemblies.Helpers.Azure;
using System;
using System.Text.RegularExpressions;

namespace StreamingAssemblies.Models.Azure
{
    /// <summary>
    /// Defines the <see cref="AzureInsertVmModel" />.
    /// </summary>
    public abstract class AzureInsertOrCreateVmModel
    {
        /// <summary>
        /// Gets or sets the AzureVmUrl. Used to get the id of the virtual machine with regex...
        /// </summary>
        public string AzureVmUrl { get; set; }
        public string Alias { get; set; }

        /// <summary>
        /// Gets or sets the TenantId.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Gets or sets the ApplicationId, or also called ClientId....
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the application secret, or also called application key, client secret or client key...
        /// </summary>
        public string ApplicationSecret { get; set; }

        /// <summary>
        /// Gets or sets the PluginUrl.
        /// </summary>
        public string PluginUrl { get; set; }

        /// <summary>
        /// The GetVmIdFromUrl.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        public string GetVmIdFromUrl()
        {
            return AzureHelper.GetVmIdFromUrl(AzureVmUrl);
        }

        public string GetSubscriptionIdFromUrl()
        {
            return AzureHelper.GetSubscriptionIdFromUrl(AzureVmUrl);
        }

        /// <summary>
        /// Gets a value indicating whether IsValid.
        /// </summary>
        public bool IsValid => !string.IsNullOrWhiteSpace(AzureVmUrl) &&
                               !string.IsNullOrWhiteSpace(TenantId) &&
                               !string.IsNullOrWhiteSpace(ApplicationId) &&
                               !string.IsNullOrWhiteSpace(ApplicationSecret) &&
                               !string.IsNullOrWhiteSpace(PluginUrl);
    }
}
