using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamingAssemblies.Models.Streaming
{
    public class Autorizacao
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public string Pagina { get; set; }

        public Autorizacao()
        {
            Id = 0;
            ItemName = "";
            Pagina = "";
        }
    }
}