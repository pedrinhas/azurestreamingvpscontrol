using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamingAssemblies.Models.Streaming
{
    public class Grupo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int IdTipo { get; set; }
        public string NomeTipo { get; set; }
        public string Descricao { get; set; }

        public bool Checked { get; set; }

        public Grupo()
        {
            Id = 0;
            Nome = "";
            IdTipo = 0;
            NomeTipo = "";
            Descricao = "";

            Checked = false;
        }
    }
}