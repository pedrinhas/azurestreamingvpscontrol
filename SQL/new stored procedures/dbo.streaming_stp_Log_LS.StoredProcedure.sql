USE [streaming]
GO
/****** Object:  StoredProcedure [dbo].[streaming_stp_Log_LS]    Script Date: 15/06/2020 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--[dbo].[streaming_stp_Log_LS]
CREATE PROCEDURE [dbo].[streaming_stp_Log_LS]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT l.[Id], l.[IdUser], usr.Username, usr.Nome as FullName, usr.IUPI, l.[Date], l.[Message], l.[IdVirtualMachine], vm.VmName, l.[Action]
	FROM [dbo].[streaming_t_Log] l
	LEFT JOIN [dbo].[nsi_t_Azure_VirtualMachines] vm on vm.Id = l.IdVirtualMachine
	LEFT JOIN [dbo].[nsi_t_User] usr on usr.id = l.IdUser
	order by l.Date desc
	END
GO
