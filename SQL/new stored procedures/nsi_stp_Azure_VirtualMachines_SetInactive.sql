USE [streaming]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_SetInactive]
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_SetInactive]
	@id int,
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
	SET IsActive = 0, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
	WHERE Id = @id

	select (case when IsActive = 0 then 1 else 0 end) as Result from [dbo].[nsi_t_Azure_VirtualMachines] where Id = @id

	--exec [dbo].[nsi_stp_Azure_VirtualMachines_SetInactive] 7, 'cpereira'