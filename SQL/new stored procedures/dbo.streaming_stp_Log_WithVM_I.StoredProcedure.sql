USE [streaming]
GO
/****** Object:  StoredProcedure [dbo].[streaming_stp_Log_WithVM_I]    Script Date: 15/06/2020 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[streaming_stp_Log_WithVM_I]
	@idUser int,
	@message nvarchar(max),
	@idVirtualMachine int,
	@action nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    insert into [streaming_t_Log] ([IdUser], [Date], [Message], [IdVirtualMachine], [Action])
	values (@idUser, GETDATE(), @message, @idVirtualMachine, @action)
END
GO
