USE [streaming]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_D]
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_D]
	@id int,
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
	SET IsDeleted = 1, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
	WHERE Id = @id

	select IsDeleted as Result from [dbo].[nsi_t_Azure_VirtualMachines] where Id = @id
	
	--[dbo].[nsi_stp_Azure_VirtualMachines_D] 11, 'cpereira'