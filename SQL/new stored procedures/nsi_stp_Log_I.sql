USE [streaming]
GO

/****** Object:  StoredProcedure [dbo].[nsi_stp_Log_I]    Script Date: 03/04/2020 12:09:53 ******/
DROP PROCEDURE [dbo].[nsi_stp_Log_I]
GO

/****** Object:  StoredProcedure [dbo].[nsi_stp_Log_I]    Script Date: 03/04/2020 12:09:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[nsi_stp_Log_I] @_type NVARCHAR(MAX), @_message NVARCHAR(MAX), @_stackTrace NVARCHAR(MAX), @_iupiUtilizador UNIQUEIDENTIFIER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_Log (date, type, message, stackTrace, iupiUtilizador)
	VALUES (GETDATE(), @_type, @_message, @_stackTrace, @_iupiUtilizador)

	SELECT [id], [date], [type], [message], [stackTrace], [iupiUtilizador]
	FROM nsi_t_Log
	WHERE id = SCOPE_IDENTITY()
GO

