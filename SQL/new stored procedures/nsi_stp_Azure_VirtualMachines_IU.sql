USE [streaming]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_IU]
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_IU]
	@id int = null,
	@vmId nvarchar(max),
	@vmName nvarchar(max),
	@alias nvarchar(max),
	@vmAzureUrl nvarchar(max),
	@configFilePath nvarchar(max),
	@pluginUrl nvarchar(max),
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @id IS NOT NULL
	BEGIN
		UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
		SET VmId = @vmId, VmName = @vmName, Alias = @alias, ConfigFilePath = @configFilePath, VmAzureUrl = @vmAzureUrl, PluginUrl = @pluginUrl, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
		WHERE Id = @id
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[nsi_t_Azure_VirtualMachines] (VmId, VmName, Alias, ConfigFilePath, VmAzureUrl, PluginUrl, IsActive, IsDeleted, CreatedDate, CreatedBy)
		VALUES (@vmId, @vmName, @alias, @configFilePath, @vmAzureUrl, @pluginUrl, 0, 0, GETDATE(), @userPerformingAction)

		set @id = SCOPE_IDENTITY()
	END

	SELECT Id, VmId, VmName, Alias, ConfigFilePath, PluginUrl, VmAzureUrl, IsActive, IsDeleted, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy
	FROM [dbo].[nsi_t_Azure_VirtualMachines]
	WHERE Id = @id

GO

--exec [dbo].[nsi_stp_Azure_VirtualMachines_IU] NULL, 'some vm id 1', 'some vm name 2', 'some config path', 'some plugin url', 1, 0, 'cpereira'