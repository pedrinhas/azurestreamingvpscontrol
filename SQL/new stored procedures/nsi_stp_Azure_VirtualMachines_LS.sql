USE [streaming]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_LS]
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_LS]
	@includeDeleted bit = 0
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Id, VmId, VmName, Alias, VmAzureUrl, ConfigFilePath, PluginUrl, IsActive, IsDeleted, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy
	FROM [dbo].[nsi_t_Azure_VirtualMachines]
	where IsDeleted = 0 OR IsDeleted = @includeDeleted
