USE [streaming]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_R]
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_R]
	@id int,
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
	SET IsDeleted = 0, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
	WHERE Id = @id

	select (case when IsDeleted = 0 then 1 else 0 end) as Result from [dbo].[nsi_t_Azure_VirtualMachines] where Id = @id

	--[dbo].[nsi_stp_Azure_VirtualMachines_R] 7, 'cpereira'