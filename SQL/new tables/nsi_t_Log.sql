USE [streaming]
GO

/****** Object:  Table [dbo].[nsi_t_Log]    Script Date: 03/04/2020 12:09:12 ******/
DROP TABLE [dbo].[nsi_t_Log]
GO

/****** Object:  Table [dbo].[nsi_t_Log]    Script Date: 03/04/2020 12:09:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[nsi_t_Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[type] [nvarchar](max) NOT NULL,
	[message] [nvarchar](max) NOT NULL,
	[stackTrace] [nvarchar](max) NOT NULL,
	[iupiUtilizador] [uniqueidentifier] NULL,
 CONSTRAINT [PK__nsi_t_Lo__3213E83F298D1279] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

