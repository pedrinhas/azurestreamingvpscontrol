USE [streaming]
GO
/****** Object:  Table [dbo].[streaming_t_Log]    Script Date: 15/06/2020 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[streaming_t_Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[IdVirtualMachine] [int] NULL,
	[Action] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_streaming_t_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[streaming_t_Log]  WITH CHECK ADD  CONSTRAINT [FK_streaming_t_Log_nsi_t_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[nsi_t_User] ([id])
GO
ALTER TABLE [dbo].[streaming_t_Log] CHECK CONSTRAINT [FK_streaming_t_Log_nsi_t_User]
GO
