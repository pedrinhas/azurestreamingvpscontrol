USE [master]
GO
/****** Object:  Database [streaming]    Script Date: 15/06/2020 14:51:51 ******/
CREATE DATABASE [streaming]
GO

EXEC sys.sp_db_vardecimal_storage_format N'streaming', N'ON'
GO
ALTER DATABASE [streaming] SET QUERY_STORE = OFF
GO
USE [streaming]
GO
/****** Object:  User [INTRA\sis-inf]    Script Date: 15/06/2020 14:51:51 ******/
CREATE USER [INTRA\sis-inf] FOR LOGIN [INTRA\sis-inf] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [db_executer]    Script Date: 15/06/2020 14:51:51 ******/
CREATE ROLE [db_executer]
GO
ALTER ROLE [db_executer] ADD MEMBER [INTRA\sis-inf]
GO
ALTER ROLE [db_datareader] ADD MEMBER [INTRA\sis-inf]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [INTRA\sis-inf]
GO
/****** Object:  Table [dbo].[nsi_t_Autorizacao]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_Autorizacao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[itemName] [nvarchar](100) NOT NULL,
	[pagina] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK__nsi_t_Au__3213E83FBE49960C] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_Azure_VirtualMachines]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_Azure_VirtualMachines](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VmId] [nvarchar](max) NOT NULL,
	[VmName] [nvarchar](max) NOT NULL,
	[Alias] [nvarchar](max) NULL,
	[VmAzureUrl] [nvarchar](max) NOT NULL,
	[ConfigFilePath] [nvarchar](max) NOT NULL,
	[PluginUrl] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](max) NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_nsi_t_Azure_VirtualMachines] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_Grupo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_Grupo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](100) NOT NULL,
	[idTipo] [int] NOT NULL,
	[descricao] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK__nsi_t_Gr__3213E83F0443D1A6] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_GrupoAutorizacao]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_GrupoAutorizacao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idGrupo] [int] NOT NULL,
	[idAutorizacao] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_GrupoMensagem]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_GrupoMensagem](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idGrupo] [int] NOT NULL,
	[idMensagem] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_Log]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[type] [nvarchar](max) NOT NULL,
	[message] [nvarchar](max) NOT NULL,
	[stackTrace] [nvarchar](max) NOT NULL,
	[iupiUtilizador] [uniqueidentifier] NULL,
 CONSTRAINT [PK__nsi_t_Lo__3213E83F298D1279] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_Mensagens]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_Mensagens](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](255) NOT NULL,
	[texto] [nvarchar](max) NOT NULL,
	[dataPublicacaoInicio] [datetime] NULL,
	[dataPublicacaoFim] [datetime] NULL,
	[visibilidade] [int] NOT NULL,
 CONSTRAINT [PK__nsi_t_Me__3213E83F54B918A8] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_Parametros]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_Parametros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[chave] [nvarchar](100) NOT NULL,
	[valor] [nvarchar](100) NOT NULL,
	[valor1] [nvarchar](50) NOT NULL,
	[valor2] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK__nsi_t_Pa__3213E83F5512C773] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_Tipo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_Tipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](20) NOT NULL,
	[codigo] [nvarchar](5) NOT NULL,
 CONSTRAINT [PK__nsi_t_Ti__3213E83F710C6472] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_User]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[iupi] [uniqueidentifier] NOT NULL,
	[nome] [nvarchar](255) NOT NULL,
	[lastLoginDate] [datetime] NULL,
	[isFromUTAD] [bit] NOT NULL,
 CONSTRAINT [PK__nsi_t_Us__3213E83FCF7A316C] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nsi_t_UserGrupo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nsi_t_UserGrupo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUser] [int] NOT NULL,
	[idGrupo] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[streaming_t_Log]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[streaming_t_Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[IdVirtualMachine] [int] NULL,
	[Action] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_streaming_t_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[nsi_t_Autorizacao] ON 

INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (9, N'AdminMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (12, N'AutorizacoesSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (13, N'GruposSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (14, N'ParametrosSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (15, N'UsersSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (16, N'GruposSubSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (17, N'GruposCargosSubSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (18, N'GruposPerfisSubSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (20, N'MensagensSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (29, N'EstMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (30, N'FuncMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (31, N'DocMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (32, N'FuncDocMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (33, N'TempMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (34, N'TestAutorizacoesSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (37, N'GruposQuoVadisSubSubMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (39, N'StrMenuItem', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (40, N'ListVms', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (41, N'CreateVm', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (42, N'ManageVm', N'')
INSERT [dbo].[nsi_t_Autorizacao] ([id], [itemName], [pagina]) VALUES (43, N'VmLogs', N'')
SET IDENTITY_INSERT [dbo].[nsi_t_Autorizacao] OFF
SET IDENTITY_INSERT [dbo].[nsi_t_Grupo] ON 

INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (3, N'DOCENTES', 3, N'Este grupo contém todos os utilizadores com Perfil de DOCENTES')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (5, N'NÃO DOCENTE', 3, N'Este grupo contém todos os utilizadores com Perfil de NÃO DOCENTE')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (6, N'ADMIN', 1, N'Este grupo contém todos os utilizadores que têm privilégios de Administrador na aplicação')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (19, N'INVESTIGADORES', 3, N'Este grupo contém todos os utilizadores com Perfil de INVESTIGADORES')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (20, N'OUTROS', 3, N'Este grupo contém todos os utilizadores com Perfil de OUTROS')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (21, N'ALUNOS', 3, N'Este grupo contém todos os utilizadores com Perfil de ALUNOS')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (22, N'TEMPORARIOS', 3, N'Este grupo contém todos os utilizadores com Perfil de TEMPORARIOS')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (27, N'VM_ADMIN', 1, N'Administrar máquinas virtuais')
INSERT [dbo].[nsi_t_Grupo] ([id], [nome], [idTipo], [descricao]) VALUES (28, N'VM_USER', 1, N'Ligar e desligar máquinas virtuais')
SET IDENTITY_INSERT [dbo].[nsi_t_Grupo] OFF
SET IDENTITY_INSERT [dbo].[nsi_t_GrupoAutorizacao] ON 

INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (53, 6, 13)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (54, 6, 14)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (55, 6, 15)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (62, 6, 16)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (63, 6, 17)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (78, 6, 18)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (137, 6, 9)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (141, 6, 12)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (146, 6, 20)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (151, 6, 29)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (152, 21, 29)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (157, 6, 31)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (158, 3, 31)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (159, 6, 32)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (160, 3, 32)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (161, 19, 32)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (162, 5, 32)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (163, 20, 32)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (164, 6, 33)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (165, 22, 33)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (166, 6, 34)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (167, 3, 34)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (168, 19, 34)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (169, 5, 34)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (170, 20, 34)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (183, 6, 37)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (191, 6, 30)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (192, 27, 30)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (199, 6, 39)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (200, 27, 39)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (201, 28, 39)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (202, 6, 40)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (203, 27, 40)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (204, 28, 40)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (205, 6, 41)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (206, 27, 41)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (207, 6, 42)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (208, 27, 42)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (209, 6, 43)
INSERT [dbo].[nsi_t_GrupoAutorizacao] ([id], [idGrupo], [idAutorizacao]) VALUES (210, 27, 43)
SET IDENTITY_INSERT [dbo].[nsi_t_GrupoAutorizacao] OFF
SET IDENTITY_INSERT [dbo].[nsi_t_Mensagens] ON 

INSERT [dbo].[nsi_t_Mensagens] ([id], [titulo], [texto], [dataPublicacaoInicio], [dataPublicacaoFim], [visibilidade]) VALUES (4, N'Mensagem de Boas-Vindas!', N'Bem-vindo à aplicação!', NULL, NULL, 3)
INSERT [dbo].[nsi_t_Mensagens] ([id], [titulo], [texto], [dataPublicacaoInicio], [dataPublicacaoFim], [visibilidade]) VALUES (5, N'Mensagem de Teste Para Utilizadores Autenticados', N'Texto simples de teste.<br />
<span class="text-primary"><b>Texto HTML de teste.</b></span>', NULL, NULL, 2)
SET IDENTITY_INSERT [dbo].[nsi_t_Mensagens] OFF
SET IDENTITY_INSERT [dbo].[nsi_t_Parametros] ON 

INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (6, N'API.UTAD.AutenticarEstudanteSimul', N'https://api.utad.pt/ms/IUTADAuthAlunoSimul/UTADAuthAlunos.svc/Autenticar', N'INTRA\sis-dev', N'JHA0czdpMTAj')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (7, N'API.UTAD.AutenticarUtilizadorSimul', N'https://api.utad.pt/ms/IUTADAuthSimul/UTADAuth.svc/Autenticar', N'INTRA\sis-dev', N'JHA0czdpMTAj')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (10, N'API.UTAD.GetEstudantePerfisSimul', N'https://api.utad.pt/IRCUAlunoSimul/RCUAlunos.svc/RCU_Aluno_Perfil_S?numero=', N'wsWorldit', N'JHdzV29ybGRpdDIwMTgj')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (11, N'API.UTAD.GetUtilizadorCargosSimul', N'https://api.utad.pt/IRCUSimul/RCUSimul.svc/RCU_Utilizador_Cargos_S?iupi=', N'wsWorldit', N'JHdzV29ybGRpdDIwMTgj')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (12, N'API.UTAD.GetUtilizadorPerfisSimul', N'https://api.utad.pt/IRCUSimul/RCUSimul.svc/RCU_Utilizador_Perfil_S?iupi=', N'wsWorldit', N'JHdzV29ybGRpdDIwMTgj')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (16, N'QuoVadis.PortalBase.GetFooter', N'https://quovadis.utad.pt/IPortalBase/GetFooter?id=1', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (17, N'QuoVadis.Aplicacoes.GetAplicacoesPortalBase', N'https://quovadis.utad.pt/IAplicacoes/GetAplicacoesPortalBase/{0}/{1}/{2}/{3}/{4}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (22, N'QuoVadis.Autenticar', N'https://quovadis.utad.pt/MS/IUTADAuth', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (23, N'QuoVadis.RCU.GetEstudantePerfis', N'https://quovadis.utad.pt/MS/Estudante_Perfis_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (26, N'QuoVadis.RCU.GetEstudanteFoto', N'https://quovadis.utad.pt/MS/Estudante_Fotos_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (27, N'QuoVadis.RCU.GetUtilizadorIUPI', N'https://quovadis.utad.pt/MS/Utilizador_IUPI_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (28, N'QuoVadis.RCU.GetUtilizador', N'https://quovadis.utad.pt/MS/Utilizadores_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (29, N'QuoVadis.RCU.GetEstudanteNomeAndIUPI', N'https://quovadis.utad.pt/MS/Estudante_NomeAndIUPI_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (31, N'QuoVadis.Cargos.GetTiposCargos', N'https://quovadis.utad.pt/ICargos/GetTiposCargosByEstado?ativo=true', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (32, N'QuoVadis.RCU.GetTiposPerfil', N'https://quovadis.utad.pt/MS/Dic_Perfil_Tipo_RCU_LS/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (33, N'QuoVadis.RCU.CreateCredenciaisTemporarias', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_RCU_I', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (34, N'Auth.CredenciaisTemporarias.Dominio', N'@portalBase', N'', N'')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (35, N'QuoVadis.RCU.GetCredenciaisTemporarias', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (36, N'QuoVadis.RCU.GetCredenciaisTemporariasPorEmail', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_PorEmail_RCU_S/{0}/', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (37, N'QuoVadis.RCU.UpdateCredenciaisTemporarias', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_RCU_U', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (38, N'Mail.DeveloperAddress', N'mvale@utad.pt', N'', N'')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (39, N'QuoVadis.RCU.GetUtilizadorNomeAndIUPI', N'https://quovadis.utad.pt/MS/Utilizador_NomeAndIUPI_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (40, N'QuoVadis.RCU.CreateCredenciaisTemporariasToken', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_Tokens_RCU_I', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (41, N'QuoVadis.RCU.UpdateCredenciaisTemporariasTokenEstadoUso', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_Tokens_EstadoUso_RCU_U/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (42, N'QuoVadis.RCU.GetCredenciaisTemporariasToken', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_Tokens_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (43, N'QuoVadis.RCU.GetCredenciaisTemporariasPorUsername', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_PorUsername_RCU_S/{0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (44, N'QuoVadis.RCU.UpdateCredenciaisTemporariasPassword', N'https://quovadis.utad.pt/MS/CredenciaisTemporarias_Password_RCU_U', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (45, N'QuoVadis.Aplicacoes.GetLinguagens', N'https://quovadis.utad.pt/IAplicacoes/GetLinguagens', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (46, N'QuoVadis.Cargos.GetPessoaCargos', N'https://quovadis.utad.pt/ICargos/GetCargosByPessoa?pessoa=', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (47, N'QuoVadis.RCU.GetGruposMenu', N'https://quovadis.utad.pt/MS/Dic_Grupos_Menu_RCU_LS', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (48, N'QuoVadis.Aplicacoes.GetAplicacaoPortalBase', N'https://quovadis.utad.pt/IAplicacoes/GetAplicacaoPortalBase/{0}/{1}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (49, N'QuoVadis.IUTAD.SearchPessoas', N'https://quovadis.utad.pt/MS/IUTADPessoas/{0}/{1}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (50, N'QuoVadis.Mensagens.GetMensagensByVisibilidades', N'https://quovadis.utad.pt/IMensagens/GetMensagensByVisibilidades/{0}/{1}/{2}/{3}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (51, N'QuoVadis.Grupos.GetGrupo', N'https://quovadis.utad.pt/IGrupos/GetGrupo?id={0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (52, N'QuoVadis.Grupos.GetGruposByTipo', N'https://quovadis.utad.pt/IGrupos/GetGrupos?tipo={0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (53, N'QuoVadis.Grupos.GetGruposByUtilizador', N'https://quovadis.utad.pt/IGrupos/GetGruposByUtilizador?utilizadorIUPI={0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (54, N'QuoVadis.Grupos.GetGrupoUtilizadores', N'https://quovadis.utad.pt/IGrupos/GetGrupoUtilizadores?nome={0}', N'uWS', N'JGExVTRlMU41Iw==')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (55, N'Auth.CredenciaisTemporarias.DiasValidade', N'30', N'', N'')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (56, N'Azure.MultiThread', N'false', N'', N'')
INSERT [dbo].[nsi_t_Parametros] ([id], [chave], [valor], [valor1], [valor2]) VALUES (57, N'Azure.AlwaysDeallocate', N'true', N'', N'')
SET IDENTITY_INSERT [dbo].[nsi_t_Parametros] OFF
SET IDENTITY_INSERT [dbo].[nsi_t_Tipo] ON 

INSERT [dbo].[nsi_t_Tipo] ([id], [nome], [codigo]) VALUES (1, N'Grupo', N'G')
INSERT [dbo].[nsi_t_Tipo] ([id], [nome], [codigo]) VALUES (2, N'Cargo', N'C')
INSERT [dbo].[nsi_t_Tipo] ([id], [nome], [codigo]) VALUES (3, N'Perfil', N'P')
INSERT [dbo].[nsi_t_Tipo] ([id], [nome], [codigo]) VALUES (4, N'Grupo QuoVadis', N'GQV')
SET IDENTITY_INSERT [dbo].[nsi_t_Tipo] OFF
SET IDENTITY_INSERT [dbo].[nsi_t_User] ON 

INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (5, N'jborges', N'30bc8b13-1c3a-4942-8be1-2035ebb3fe8e', N'JORGE JOSÉ DOS SANTOS BORGES', CAST(N'2020-06-12T16:25:36.240' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (9, N'mvale', N'5e1cb914-42b3-494c-ba38-d27d78839c0f', N'MARCO ANTONIO GOMES DO VALE', CAST(N'2020-03-24T16:54:11.703' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (12, N'cmrodrigues', N'13343ee9-fd6a-4360-a938-58aa482e4fe1', N'CARLOS MIGUEL PEREIRA RODRIGUES', CAST(N'2019-12-18T15:10:14.197' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (28, N'yasmine', N'20393797-822b-4924-87ed-32a86482b39b', N'YASMINE ALEXANDRA NOBREGA DE SALES GOMES AMRAOUI', CAST(N'2019-10-22T17:14:37.887' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (31, N'al62163', N'6474e4be-72a7-4cbb-8490-74c59d735c11', N'PEDRO MIGUEL BENTO TEIXEIRA', CAST(N'2020-03-03T15:17:24.463' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (32, N'al62039', N'0c2b483c-d7c2-441a-88d9-c66cb19bac88', N'YASMINE ALEXANDRA NÓBREGA DE SALES GOMES AMRAOUI', CAST(N'2019-10-23T10:57:05.383' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (33, N'al27067', N'9e9ce638-338c-47e6-bb32-f2dcc7a1ba4f', N'JORGE JOSÉ DOS SANTOS BORGES', CAST(N'2020-01-14T12:34:29.767' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (34, N'al66423', N'5dcf10c9-9e7f-4155-847b-ec0e5b20ffbe', N'TONI LEMOS CHAVES', CAST(N'2019-11-25T11:11:21.853' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (35, N'al64200', N'511e92c2-f3a6-45dc-aca0-72015df5ec03', N'FERNANDO CÉSAR DA COSTA FERREIRA', CAST(N'2020-01-30T11:53:26.553' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (36, N'a3esjs', N'9a564755-dfe8-4011-b1ad-4c5b70af5df9', N'Jacinto Vidigal Silva', CAST(N'2019-12-05T11:55:13.637' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (37, N'carlosvaz', N'90f24858-e32a-469f-9a40-b3efdbbf3327', N'CARLOS MANUEL RODRIGUES SOARES VAZ', CAST(N'2019-12-09T15:30:16.730' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (38, N'al60661', N'5d4cdcf0-912b-446c-9080-a3945e8496ce', N'MARCO ANTÓNIO GOMES DO VALE', CAST(N'2020-01-31T16:22:36.823' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (39, N'jlmourao', N'03c742bb-bfe9-40bc-b2c3-6217b77a8090', N'JOSÉ LUIS TEIXEIRA DE ABREU DE MEDEIROS MOURÃO', CAST(N'2020-01-14T12:33:42.813' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (40, N'mariana.portugal', N'30cb9083-8083-499d-b51e-3790010939f5', N'MARIANA SILVA PORTUGAL VASCONCELOS FERREIRA RIO', CAST(N'2020-01-16T15:41:55.483' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (41, N'al64166', N'e9f27295-3b01-41f7-8caa-a769d2848f6d', N'DIOGO JORGE PEREIRA MESQUITA GUIMARÃES', CAST(N'2020-01-31T10:58:22.927' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (42, N'ruimest', N'96314886-9fc5-45da-b843-d848592df5f4', N'RUI BAIO MESTRE', CAST(N'2020-02-12T16:55:47.787' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (43, N'marcovale151@portalBase', N'5a3697fd-0d44-4f46-848e-44162aed0fd1', N'Marco António Gomes do Vale', CAST(N'2020-02-13T15:14:25.550' AS DateTime), 0)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (44, N'lfb', N'd62707f8-8031-4c6d-881e-02434c30aa7a', N'LUIS FILIPE LEITE BARBOSA', CAST(N'2020-02-20T15:47:54.773' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (45, N'cpereira', N'dc27a15b-0020-49fa-9b91-0ade9b0b0f51', N'CLÁUDIO JOSÉ SILVA PEREIRA', CAST(N'2020-06-15T14:47:39.060' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (46, N'carlosp', N'8288c965-2835-4a95-8e6b-9f0fbad08d05', N'CARLOS ALBERTO GOMES PAULO', CAST(N'2020-06-03T21:16:37.007' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (47, N'nelsonm', N'6552dfe8-df57-4ab5-adc3-53b59492afc8', N'NELSON ROGÉRIO SANTOS PINTO MONTEIRO', NULL, 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (48, N'arosa', N'36eadcff-b4fc-4301-a1b9-6f918d924641', N'ANA ROSA PEREIRA', CAST(N'2020-05-14T14:27:34.233' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (49, N'ricardom', N'df6d3618-e271-4c52-9392-7dddc1032989', N'RICARDO MIGUEL DA COSTA MARTINS', CAST(N'2020-05-21T17:35:12.290' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (50, N'jerf', N'7409eff7-226f-4a96-bf7c-d0c99d004496', N'JOSÉ EDUARDO ROSAS FRAGA', CAST(N'2020-05-28T12:49:32.763' AS DateTime), 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (51, N'vledo', N'37b77372-b6ea-4167-b8e3-7cb6e485379c', N'VICTOR MANUEL DE ALMEIDA LEDO RODRIGUES', NULL, 1)
INSERT [dbo].[nsi_t_User] ([id], [username], [iupi], [nome], [lastLoginDate], [isFromUTAD]) VALUES (52, N'acosta', N'531a4090-a60a-4064-b49e-bf06ab648e4a', N'ANTÓNIO MANUEL ARAÚJO DA SILVA RIO COSTA', CAST(N'2020-05-26T15:09:04.203' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[nsi_t_User] OFF
SET IDENTITY_INSERT [dbo].[nsi_t_UserGrupo] ON 

INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (90, 37, 6)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (91, 36, 6)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (92, 5, 6)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (93, 9, 6)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (94, 34, 6)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (100, 46, 27)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (101, 5, 27)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (102, 47, 27)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (105, 45, 27)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (106, 45, 6)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (108, 45, 28)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (109, 48, 28)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (110, 49, 28)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (111, 50, 28)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (112, 51, 28)
INSERT [dbo].[nsi_t_UserGrupo] ([id], [idUser], [idGrupo]) VALUES (113, 52, 28)
SET IDENTITY_INSERT [dbo].[nsi_t_UserGrupo] OFF

ALTER TABLE [dbo].[nsi_t_User] ADD  CONSTRAINT [DF__nsi_t_Use__lastL__778AC167]  DEFAULT (getdate()) FOR [lastLoginDate]
GO
ALTER TABLE [dbo].[nsi_t_User] ADD  CONSTRAINT [DF__nsi_t_Use__isFro__787EE5A0]  DEFAULT ((1)) FOR [isFromUTAD]
GO
ALTER TABLE [dbo].[nsi_t_Grupo]  WITH CHECK ADD  CONSTRAINT [FK__nsi_t_Gru__idTip__3B75D760] FOREIGN KEY([idTipo])
REFERENCES [dbo].[nsi_t_Tipo] ([id])
GO
ALTER TABLE [dbo].[nsi_t_Grupo] CHECK CONSTRAINT [FK__nsi_t_Gru__idTip__3B75D760]
GO
ALTER TABLE [dbo].[nsi_t_GrupoAutorizacao]  WITH CHECK ADD  CONSTRAINT [FK__nsi_t_Gru__idAut__44FF419A] FOREIGN KEY([idAutorizacao])
REFERENCES [dbo].[nsi_t_Autorizacao] ([id])
GO
ALTER TABLE [dbo].[nsi_t_GrupoAutorizacao] CHECK CONSTRAINT [FK__nsi_t_Gru__idAut__44FF419A]
GO
ALTER TABLE [dbo].[nsi_t_GrupoAutorizacao]  WITH CHECK ADD  CONSTRAINT [FK__nsi_t_Gru__idGru__440B1D61] FOREIGN KEY([idGrupo])
REFERENCES [dbo].[nsi_t_Grupo] ([id])
GO
ALTER TABLE [dbo].[nsi_t_GrupoAutorizacao] CHECK CONSTRAINT [FK__nsi_t_Gru__idGru__440B1D61]
GO
ALTER TABLE [dbo].[nsi_t_GrupoMensagem]  WITH CHECK ADD FOREIGN KEY([idGrupo])
REFERENCES [dbo].[nsi_t_Grupo] ([id])
GO
ALTER TABLE [dbo].[nsi_t_GrupoMensagem]  WITH CHECK ADD FOREIGN KEY([idMensagem])
REFERENCES [dbo].[nsi_t_Mensagens] ([id])
GO
ALTER TABLE [dbo].[nsi_t_UserGrupo]  WITH CHECK ADD  CONSTRAINT [FK__nsi_t_Use__idGru__3F466844] FOREIGN KEY([idGrupo])
REFERENCES [dbo].[nsi_t_Grupo] ([id])
GO
ALTER TABLE [dbo].[nsi_t_UserGrupo] CHECK CONSTRAINT [FK__nsi_t_Use__idGru__3F466844]
GO
ALTER TABLE [dbo].[nsi_t_UserGrupo]  WITH CHECK ADD  CONSTRAINT [FK__nsi_t_Use__idUse__3E52440B] FOREIGN KEY([idUser])
REFERENCES [dbo].[nsi_t_User] ([id])
GO
ALTER TABLE [dbo].[nsi_t_UserGrupo] CHECK CONSTRAINT [FK__nsi_t_Use__idUse__3E52440B]
GO
ALTER TABLE [dbo].[streaming_t_Log]  WITH CHECK ADD  CONSTRAINT [FK_streaming_t_Log_nsi_t_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[nsi_t_User] ([id])
GO
ALTER TABLE [dbo].[streaming_t_Log] CHECK CONSTRAINT [FK_streaming_t_Log_nsi_t_User]
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Autorizacao_D]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Autorizacao_D] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_Autorizacao
	WHERE id=@_id

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Autorizacao_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Autorizacao_I] @_itemName NVARCHAR(100), @_pagina NVARCHAR(100)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_Autorizacao (itemName, pagina)
	VALUES (@_itemName, @_pagina)

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Autorizacao_LS]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Autorizacao_LS]
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, itemName, pagina
	FROM nsi_t_Autorizacao
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Autorizacao_S]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Autorizacao_S] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT itemName, pagina
	FROM nsi_t_Autorizacao
	WHERE id=@_id
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Autorizacao_S_ByGrupo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Autorizacao_S_ByGrupo] @_idGrupo INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT a.id, a.itemName, a.pagina
	FROM nsi_t_Autorizacao AS a
	LEFT JOIN nsi_t_GrupoAutorizacao AS ga ON ga.idAutorizacao=a.id
	WHERE ga.idGrupo=@_idGrupo
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Autorizacao_U]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Autorizacao_U] @_id INTEGER, @_itemName NVARCHAR(100), @_pagina NVARCHAR(100)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE nsi_t_Autorizacao
	SET itemName=@_itemName, pagina=@_pagina
	WHERE id=@_id

	SELECT ISNULL(SCOPE_IDENTITY(), @_id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_D]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_D]
	@id int,
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
	SET IsDeleted = 1, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
	WHERE Id = @id

	select IsDeleted as Result from [dbo].[nsi_t_Azure_VirtualMachines] where Id = @id
	
	--[dbo].[nsi_stp_Azure_VirtualMachines_D] 7, 'cpereira'
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_IU]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_IU]
	@id int = null,
	@vmId nvarchar(max),
	@vmName nvarchar(max),
	@alias nvarchar(max),
	@vmAzureUrl nvarchar(max),
	@configFilePath nvarchar(max),
	@pluginUrl nvarchar(max),
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @id IS NOT NULL
	BEGIN
		UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
		SET VmId = @vmId, VmName = @vmName, Alias = @alias, ConfigFilePath = @configFilePath, VmAzureUrl = @vmAzureUrl, PluginUrl = @pluginUrl, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
		WHERE Id = @id
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[nsi_t_Azure_VirtualMachines] (VmId, VmName, Alias, ConfigFilePath, VmAzureUrl, PluginUrl, IsActive, IsDeleted, CreatedDate, CreatedBy)
		VALUES (@vmId, @vmName, @alias, @configFilePath, @vmAzureUrl, @pluginUrl, 0, 0, GETDATE(), @userPerformingAction)

		set @id = SCOPE_IDENTITY()
	END

	SELECT Id, VmId, VmName, Alias, ConfigFilePath, PluginUrl, VmAzureUrl, IsActive, IsDeleted, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy
	FROM [dbo].[nsi_t_Azure_VirtualMachines]
	WHERE Id = @id

GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_LS]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_LS]
	@includeDeleted bit = 0
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Id, VmId, VmName, Alias, VmAzureUrl, ConfigFilePath, PluginUrl, IsActive, IsDeleted, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy
	FROM [dbo].[nsi_t_Azure_VirtualMachines]
	where IsDeleted = 0 OR IsDeleted = @includeDeleted
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_LS_Active]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_LS_Active]
	@includeDeleted bit = 0
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Id, VmId, VmName, Alias, VmAzureUrl, ConfigFilePath, PluginUrl, IsActive, IsDeleted, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy
	FROM [dbo].[nsi_t_Azure_VirtualMachines]
	where (IsDeleted = 0 OR IsDeleted = @includeDeleted)
	AND IsActive = 1
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_LS_Inactive]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_LS_Inactive]
	@includeDeleted bit = 0
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Id, VmId, VmName, Alias, VmAzureUrl, ConfigFilePath, PluginUrl, IsActive, IsDeleted, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy
	FROM [dbo].[nsi_t_Azure_VirtualMachines]
	where (IsDeleted = 0 OR IsDeleted = @includeDeleted)
	AND IsActive = 0
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_R]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_R]
	@id int,
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
	SET IsDeleted = 0, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
	WHERE Id = @id

	select (case when IsDeleted = 0 then 1 else 0 end) as Result from [dbo].[nsi_t_Azure_VirtualMachines] where Id = @id

	--[dbo].[nsi_stp_Azure_VirtualMachines_R] 7, 'cpereira'
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_S]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_S]
	@id int,
	@includeDeleted bit = 0
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Id, VmId, VmName, Alias, VmAzureUrl, ConfigFilePath, PluginUrl, IsActive, IsDeleted, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy
	FROM [dbo].[nsi_t_Azure_VirtualMachines]
	where (IsDeleted = 0 OR IsDeleted = @includeDeleted)
	AND Id = @id

	--[dbo].[nsi_stp_Azure_VirtualMachines_S] 10, 1
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_SetActive]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_SetActive]
	@id int,
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
	SET IsActive = 1, ModifiedBy = @userPerformingAction
	WHERE Id = @id

	select IsActive as Result from [dbo].[nsi_t_Azure_VirtualMachines] where Id = @id
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Azure_VirtualMachines_SetInactive]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Azure_VirtualMachines_SetInactive]
	@id int,
	@userPerformingAction nvarchar(max)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [dbo].[nsi_t_Azure_VirtualMachines]
	SET IsActive = 0, ModifiedDate = GETDATE(), ModifiedBy = @userPerformingAction
	WHERE Id = @id

	select (case when IsActive = 0 then 1 else 0 end) as Result from [dbo].[nsi_t_Azure_VirtualMachines] where Id = @id
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_D]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_D] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_Grupo
	WHERE id=@_id

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_I] @_nome NVARCHAR(100), @_idTipo INTEGER, @_descricao NVARCHAR(200)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_Grupo (nome, idTipo, descricao)
	VALUES (@_nome, @_idTipo, @_descricao)

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_LS]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_LS]
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT g.id, g.nome, g.idTipo, t.nome, g.descricao
	FROM nsi_t_Grupo AS g
	LEFT JOIN nsi_t_Tipo AS t ON g.idTipo=t.id
	ORDER BY g.nome
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_S]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_S] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT nome, idTipo, descricao
	FROM nsi_t_Grupo
	WHERE id=@_id
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_S_ByIUPI]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_S_ByIUPI] @_iupi UNIQUEIDENTIFIER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ug.idGrupo, g.nome, g.descricao
	FROM nsi_t_Grupo AS g
	LEFT JOIN nsi_t_UserGrupo AS ug ON ug.idGrupo=g.id
	LEFT JOIN nsi_t_User AS u ON ug.idUser=u.id
	WHERE u.iupi=@_iupi
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_S_ByNomeAndTipo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_S_ByNomeAndTipo] @_nome NVARCHAR(100), @_idTipo INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, descricao
	FROM nsi_t_Grupo
	WHERE nome=@_nome AND idTipo=@_idTipo
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_S_ByTipo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_S_ByTipo] @_idTipo INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, nome, descricao
	FROM nsi_t_Grupo
	WHERE idTipo=@_idTipo
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_S_ByUser]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_S_ByUser] @_idUser INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ug.idGrupo, g.nome, g.descricao
	FROM nsi_t_Grupo AS g
	LEFT JOIN nsi_t_UserGrupo AS ug ON ug.idGrupo=g.id
	WHERE ug.idUser=@_idUser
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Grupo_U]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Grupo_U] @_id INTEGER, @_nome NVARCHAR(100), @_idTipo INTEGER, @_descricao NVARCHAR(200)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE nsi_t_Grupo
	SET nome=@_nome, idTipo=@_idTipo, descricao=@_descricao
	WHERE id=@_id

	SELECT ISNULL(SCOPE_IDENTITY(), @_id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_GrupoAutorizacao_D_ByAutorizacao]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_GrupoAutorizacao_D_ByAutorizacao] @_idAutorizacao INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_GrupoAutorizacao
	WHERE idAutorizacao=@_idAutorizacao

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_GrupoAutorizacao_D_ByGrupo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_GrupoAutorizacao_D_ByGrupo] @_idGrupo INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_GrupoAutorizacao
	WHERE idGrupo=@_idGrupo

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_GrupoAutorizacao_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_GrupoAutorizacao_I] @_idGrupo INTEGER, @_idAutorizacao INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_GrupoAutorizacao (idGrupo, idAutorizacao)
	VALUES (@_idGrupo, @_idAutorizacao)

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_GrupoAutorizacao_S_ByAutorizacao]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_GrupoAutorizacao_S_ByAutorizacao] @_idAutorizacao INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, idGrupo
	FROM nsi_t_GrupoAutorizacao
	WHERE idAutorizacao=@_idAutorizacao
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_GrupoMensagem_D_ByMensagem]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_GrupoMensagem_D_ByMensagem] @_idMensagem INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_GrupoMensagem
	WHERE idMensagem=@_idMensagem

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_GrupoMensagem_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_GrupoMensagem_I] @_idGrupo INTEGER, @_idMensagem INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_GrupoMensagem (idGrupo, idMensagem)
	VALUES (@_idGrupo, @_idMensagem)

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_GrupoMensagem_S_ByMensagem]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_GrupoMensagem_S_ByMensagem] @_idMensagem INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, idGrupo
	FROM nsi_t_GrupoMensagem
	WHERE idMensagem=@_idMensagem
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Log_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Log_I] @_type NVARCHAR(MAX), @_message NVARCHAR(MAX), @_stackTrace NVARCHAR(MAX), @_iupiUtilizador UNIQUEIDENTIFIER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_Log (date, type, message, stackTrace, iupiUtilizador)
	VALUES (GETDATE(), @_type, @_message, @_stackTrace, @_iupiUtilizador)

	SELECT [id], [date], [type], [message], [stackTrace], [iupiUtilizador]
	FROM nsi_t_Log
	WHERE id = SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Mensagens_D]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Mensagens_D] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_Mensagens
	WHERE id=@_id

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Mensagens_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Mensagens_I] @_titulo NVARCHAR(255), @_texto NVARCHAR(MAX), @_dataPublicacaoInicio DATETIME = NULL, @_dataPublicacaoFim DATETIME = NULL, @_visibilidade INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_Mensagens (titulo, texto, dataPublicacaoInicio, dataPublicacaoFim, visibilidade)
	VALUES (@_titulo, @_texto, @_dataPublicacaoInicio, @_dataPublicacaoFim, @_visibilidade)

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Mensagens_LS]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Mensagens_LS]
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, titulo, texto, dataPublicacaoInicio, dataPublicacaoFim, visibilidade
	FROM nsi_t_Mensagens
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Mensagens_S]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Mensagens_S] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT titulo, texto, dataPublicacaoInicio, dataPublicacaoFim, visibilidade
	FROM nsi_t_Mensagens
	WHERE id=@_id
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Mensagens_S_ByVisibilidade]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Mensagens_S_ByVisibilidade] @_visibilidade INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, titulo, texto, dataPublicacaoInicio, dataPublicacaoFim
	FROM nsi_t_Mensagens
	WHERE visibilidade>=@_visibilidade AND (dataPublicacaoInicio IS NULL OR dataPublicacaoInicio<=GETDATE()) AND (dataPublicacaoFim IS NULL OR dataPublicacaoFim>=GETDATE())
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Mensagens_U]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Mensagens_U] @_id INTEGER, @_titulo NVARCHAR(255), @_texto NVARCHAR(MAX), @_dataPublicacaoInicio DATETIME = NULL, @_dataPublicacaoFim DATETIME = NULL, @_visibilidade INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE nsi_t_Mensagens
	SET titulo=@_titulo, texto=@_texto, dataPublicacaoInicio=@_dataPublicacaoInicio, dataPublicacaoFim=@_dataPublicacaoFim, visibilidade=@_visibilidade
	WHERE id=@_id

	SELECT ISNULL(SCOPE_IDENTITY(), @_id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Parametros_D]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Parametros_D] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_Parametros
	WHERE id=@_id

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Parametros_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Parametros_I] @_chave NVARCHAR(100), @_valor NVARCHAR(100), @_valor1 NVARCHAR(50), @_valor2 NVARCHAR(50)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_Parametros (chave, valor, valor1, valor2)
	VALUES (@_chave, @_valor, @_valor1, @_valor2)

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Parametros_LS]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Parametros_LS]
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, chave, valor, valor1, valor2
	FROM nsi_t_Parametros
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Parametros_S_ByChave]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Parametros_S_ByChave] @_chave NVARCHAR(100)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, valor, valor1, valor2
	FROM nsi_t_Parametros
	WHERE chave=@_chave
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_Parametros_U]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_Parametros_U] @_id INTEGER, @_chave NVARCHAR(100), @_valor NVARCHAR(100), @_valor1 NVARCHAR(50), @_valor2 NVARCHAR(50)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE nsi_t_Parametros
	SET chave=@_chave, valor=@_valor, valor1=@_valor1, valor2=@_valor2
	WHERE id=@_id

	SELECT ISNULL(SCOPE_IDENTITY(), @_id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_User_D]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_User_D] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_User
	WHERE id=@_id

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_User_IU]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_User_IU] @_username NVARCHAR(50), @_iupi UNIQUEIDENTIFIER, @_nome NVARCHAR(255), @_lastLoginDate DATETIME = NULL, @_isFromUTAD BIT
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	IF EXISTS(SELECT id FROM nsi_t_User WHERE iupi=@_iupi)
	BEGIN
		SET @id = (SELECT id FROM nsi_t_User WHERE iupi=@_iupi)

		UPDATE nsi_t_User
		SET username=@_username, nome=@_nome, lastLoginDate=@_lastLoginDate, isFromUTAD=@_isFromUTAD
		WHERE iupi=@_iupi
	END
	ELSE
	BEGIN
		INSERT INTO nsi_t_User (username, iupi, nome, lastLoginDate, isFromUTAD)
		VALUES (@_username, @_iupi, @_nome, @_lastLoginDate, @_isFromUTAD)
	END

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_User_LS]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_User_LS]
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, username, iupi, nome, lastLoginDate, isFromUTAD
	FROM nsi_t_User
	ORDER BY nome
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_User_S]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_User_S] @_id INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT username, iupi, nome, lastLoginDate, isFromUTAD
	FROM nsi_t_User
	WHERE id=@_id
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_UserGrupo_D_ByGrupo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_UserGrupo_D_ByGrupo] @_idGrupo INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_UserGrupo
	WHERE idGrupo=@_idGrupo

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_UserGrupo_D_ByUser]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_UserGrupo_D_ByUser] @_idUser INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM nsi_t_UserGrupo
	WHERE idUser=@_idUser

	SELECT @@ROWCOUNT
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_UserGrupo_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_UserGrupo_I] @_idUser INTEGER, @_idGrupo INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @id INTEGER

	INSERT INTO nsi_t_UserGrupo (idUser, idGrupo)
	VALUES (@_idUser, @_idGrupo)

	SELECT ISNULL(SCOPE_IDENTITY(), @id)
GO
/****** Object:  StoredProcedure [dbo].[nsi_stp_UserGrupo_S_ByGrupo]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nsi_stp_UserGrupo_S_ByGrupo] @_idGrupo INTEGER
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT id, idUser
	FROM nsi_t_UserGrupo
	WHERE idGrupo=@_idGrupo
GO
/****** Object:  StoredProcedure [dbo].[streaming_stp_Log_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[streaming_stp_Log_I]
	@idUser int,
	@message nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    insert into [streaming_t_Log] ([IdUser], [Date], [Message])
	values (@idUser, GETDATE(), @message)
END
GO
/****** Object:  StoredProcedure [dbo].[streaming_stp_Log_LS]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--[dbo].[streaming_stp_Log_LS]
CREATE PROCEDURE [dbo].[streaming_stp_Log_LS]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT l.[Id], l.[IdUser], usr.Username, usr.Nome as FullName, usr.IUPI, l.[Date], l.[Message], l.[IdVirtualMachine], vm.VmName, l.[Action]
	FROM [dbo].[streaming_t_Log] l
	LEFT JOIN [dbo].[nsi_t_Azure_VirtualMachines] vm on vm.Id = l.IdVirtualMachine
	LEFT JOIN [dbo].[nsi_t_User] usr on usr.id = l.IdUser
	order by l.Date desc
	END
GO
/****** Object:  StoredProcedure [dbo].[streaming_stp_Log_LS_ByDate]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[streaming_stp_Log_LS_ByDate] 
	@minDate DATETIME = null,
	@maxDate DATETIME = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT l.[Id], l.[IdUser], usr.Username, usr.Nome as FullName, usr.IUPI, l.[Date], l.[Message], l.[IdVirtualMachine], vm.VmName, l.[Action]
	FROM [dbo].[streaming_t_Log] l
	LEFT JOIN [dbo].[nsi_t_Azure_VirtualMachines] vm on vm.Id = l.IdVirtualMachine
	LEFT JOIN [dbo].[nsi_t_User] usr on usr.id = l.IdUser
	WHERE (@minDate is null OR [Date] >= @minDate) AND (@maxDate is null OR [Date] <= @maxDate)
	order by l.Date desc
	END
GO
/****** Object:  StoredProcedure [dbo].[streaming_stp_Log_LS_ByVM]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[streaming_stp_Log_LS_ByVM]
	@idVirtualMachine int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT l.[Id], l.[IdUser], usr.Username, usr.Nome as FullName, usr.IUPI, l.[Date], l.[Message], l.[IdVirtualMachine], vm.VmName, l.[Action]
	FROM [dbo].[streaming_t_Log] l
	LEFT JOIN [dbo].[nsi_t_Azure_VirtualMachines] vm on vm.Id = l.IdVirtualMachine
	LEFT JOIN [dbo].[nsi_t_User] usr on usr.id = l.IdUser
	WHERE [IdVirtualMachine] = @idVirtualMachine
	order by l.Date desc
	END
GO
/****** Object:  StoredProcedure [dbo].[streaming_stp_Log_WithVM_I]    Script Date: 15/06/2020 14:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[streaming_stp_Log_WithVM_I]
	@idUser int,
	@message nvarchar(max),
	@idVirtualMachine int,
	@action nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    insert into [streaming_t_Log] ([IdUser], [Date], [Message], [IdVirtualMachine], [Action])
	values (@idUser, GETDATE(), @message, @idVirtualMachine, @action)
END
GO
USE [master]
GO
ALTER DATABASE [streaming] SET  READ_WRITE 
GO
